% Unit tests for The Hydrostatics.m
clc
clearvars
addpath(genpath('../src'))
TestFolderPath = './tests_data/';
tol = 1e-9;

%% Test for GetHydrostatics
%
load(strcat(TestFolderPath,'InputForGetHydrostaticsTests.mat'));

UpdateTheTestResults = 0; % must be false after the update

if UpdateTheTestResults
    [ExpectedCjk , ExpectedVOL, ExpectedXb, ExpectedZb] = GetHydrostatics(S, Zg); %#ok<UNRCH>
    save(strcat(TestFolderPath,'ExpectedHydrostatics.mat'), 'ExpectedCjk', 'ExpectedVOL', 'ExpectedXb', 'ExpectedZb');
end
load(strcat(TestFolderPath,'ExpectedHydrostatics.mat'));
[ActualCjk, ActualVOL, ActualXb, ActualZb]= GetHydrostatics(S, Zg);

Test1 = all(abs(ActualCjk - ExpectedCjk)<tol); assert(all(Test1(:)==1));
Test2 = all(abs(ActualVOL - ExpectedVOL)<tol); assert(all(Test2(:)==1));
Test3 = all(abs(ActualXb  - ExpectedXb)<tol);  assert(all(Test3(:)==1));
Test4 = all(abs(ActualZb  - ExpectedZb)<tol);  assert(all(Test4(:)==1));