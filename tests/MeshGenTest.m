% Unit tests for the panel generator
clc
clc
clearvars
addpath(genpath('../src'))
TestFolderPath = './tests_data/';
tol = 1e-9;
load(strcat(TestFolderPath,'yForTest.mat'));
load(strcat(TestFolderPath,'zForTest.mat'));
PanelRes = 20;

%% Test GetPanelWidth
ExpectedPanelWidth    = 0.893505305140076;
ExpectedBodyLineWidth = 16.97660079766144;

[PanelWidth, BodyLineWidth] = GetPanelWidth(yForTest, zForTest, PanelRes);

assert(abs(PanelWidth-ExpectedPanelWidth)<tol);
assert(abs(BodyLineWidth-ExpectedBodyLineWidth)<tol);

%% Test GetFromEngine

[PanelWidth, BodyLineWidth] = GetPanelWidth(yForTest, zForTest, PanelRes);

UpdateTheTestResults = 0 ; % must be false after the update

if UpdateTheTestResults
    [ActualY,ActualZ] = GetFromEngine(yForTest, zForTest, PanelWidth); %#ok<UNRCH>
    MeshedLine = [ActualY', ActualZ'];
    save(strcat(TestFolderPath,'MeshedLine.mat'), 'MeshedLine');
end

load(strcat(TestFolderPath,'MeshedLine.mat'));
ExpectedY = MeshedLine(:,1);
ExpectedZ = MeshedLine(:,2);
[ActualY,ActualZ] = GetFromEngine(yForTest, zForTest, PanelWidth);

Test1 = all(abs(ActualY'-ExpectedY)<tol); assert(all(Test1(:)==1));
Test2 = all(abs(ActualZ'-ExpectedZ)<tol); assert(all(Test2(:)==1));

%% Test GenerateMesh

V(:,1) = yForTest';
V(:,2) = zForTest';

UpdateTheTestResults = 0 ; % must be false after the update

if UpdateTheTestResults
    MeshedLineUniform = GenerateMesh(V, PanelRes, 1e-8, 0); %#ok<UNRCH>
    save(strcat(TestFolderPath,'MeshedLineUniform.mat'), 'MeshedLineUniform');
end

load(strcat(TestFolderPath,'MeshedLineUniform.mat'));
ExpectedY = MeshedLineUniform(:,1);
ExpectedZ = MeshedLineUniform(:,2);
Vmeshed = GenerateMesh(V, PanelRes, 1e-8, 0);
ActualY = Vmeshed(:,1);
ActualZ = Vmeshed(:,2);

Test1 = all(abs(ActualY-ExpectedY)<tol); assert(all(Test1(:)==1));
Test2 = all(abs(ActualZ-ExpectedZ)<tol); assert(all(Test2(:)==1));