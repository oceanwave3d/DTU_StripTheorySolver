% Unit test for the IntegraEquationlSolver.m
clc
clearvars
addpath(genpath('../src'))
TestFolderPath = './tests_data/';
tol = 1e-9;

%% Test for IntegraEquationlSolver

n = 10;
x = linspace(-1,1,n);
order = 4;
Cmid  = FDCoefficients(order/2, order/2);
Cleft = zeros(order+1, order+1, order+1);
Cright= zeros(order+1, order+1, order+1);
for index=1:order+1
    Cleft (:,:,index) = FDCoefficients(index-1, order-index+1);
    Cright(:,:,index) = FDCoefficients(order-index+1, index-1);
end
c.m = Cmid;
c.l = Cleft;
c.r = Cright;
K = 0.25;
A.parm.K = K;
W.parm.K = K;
S.parm.K = K;
W.f = @(x,parm) sin(parm.K*x);
A.f = @(x,parm) cos(parm.K*x);
S.f = @(x,parm) sin(parm.K*x);

UpdateTheTestResults = 0; % must be false after the update

if UpdateTheTestResults
    ExpectedQIntegral = IntegraEquationlSolver(x,W,A,S,c); %#ok<UNRCH>
    save(strcat(TestFolderPath,'ExpectedQIntegral.mat'), 'ExpectedQIntegral');
end
load(strcat(TestFolderPath,'ExpectedQIntegral.mat'));
ActualQIntegral = IntegraEquationlSolver(x,W,A,S,c);

Test = abs(ExpectedQIntegral - ActualQIntegral)<tol;
assert(all(Test()==1));
