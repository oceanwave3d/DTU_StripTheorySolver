% Unit tests for StripTheorySolver.m
clc
clearvars
addpath(genpath('../src'))
TestFolderPath = './tests_data/';
tol = 1e-9;

%% Test for DTU_StripTheorySolver

KochinType='2D';

% 1. Test 3D results for head-seas condition

UpdateTheTestResults_HeadSeas = 0; % must be false after the update

load(strcat(TestFolderPath,'InputTestDataForSolver.mat'));

if UpdateTheTestResults_HeadSeas
    [SolExpectedHead, SExpectedHead] = DTU_StripTheorySolver(S, U, beta, omD, g, ulen, zcg, RGyr,KochinType,'all'); %#ok<UNRCH>
    save(strcat(TestFolderPath,'OutputTestDataForSolver.mat'), 'SolExpectedHead', 'SExpectedHead','-append');
end

[SolActualHead, SActualHead] = DTU_StripTheorySolver(S, U, beta, omD, g, ulen, zcg, RGyr, KochinType , 'all');

load(strcat(TestFolderPath,'OutputTestDataForSolver.mat'));

for i = 1:length(U)
    Test1 = all(abs(SolActualHead(i).Fjk-SolExpectedHead(i).Fjk)<tol); assert(all(Test1(:)==1));
    Test2 = all(abs(SolActualHead(i).XjD-SolExpectedHead(i).XjD)<tol); assert(all(Test2(:)==1));
    Test3 = all(abs(SolActualHead(i).xi-SolExpectedHead(i).xi)<tol); assert(all(Test3(:)==1));
    Test4 = all(abs(SolActualHead(i).RwMaruo-SolExpectedHead(i).RwMaruo)<tol); assert(all(Test4(:)==1));
    Test5 = all(abs(SolActualHead(i).RwSalvesen-SolExpectedHead(i).RwSalvesen)<tol); assert(all(Test5(:)==1));
end

% 2. Test 2D sectional results for head-seas condition

for t=1:length(SActualHead)
    if SActualHead(t).Nv~=0
        Test1 = all(abs(SActualHead(t).Hm-SExpectedHead(t).Hm)<tol); assert(all(Test1(:)==1));
        Test2 = all(abs(SActualHead(t).E-SExpectedHead(t).E)<tol); assert(all(Test2(:)==1));
        Test3 = all(abs(SActualHead(t).phi-SExpectedHead(t).phi)<tol); assert(all(Test3(:)==1));
        Test4 = all(abs(SActualHead(t).f3-SExpectedHead(t).f3)<tol); assert(all(Test4(:)==1));
        Test5 = all(abs(SActualHead(t).h3-SExpectedHead(t).h3)<tol); assert(all(Test5(:)==1));
        Test6 = all(abs(SActualHead(t).hs-SExpectedHead(t).hs)<tol); assert(all(Test6(:)==1));
        Test7 = all(abs(SActualHead(t).ScatPhiMapped-SExpectedHead(t).ScatPhiMapped)<tol); assert(all(Test7(:)==1));
        Test8 = all(abs(SActualHead(t).ScatBCMapped-SExpectedHead(t).ScatBCMapped)<tol); assert(all(Test8(:)==1));
        Test9 = all(abs(SActualHead(t).dphi_heave_dx-SExpectedHead(t).dphi_heave_dx)<tol); assert(all(Test9(:)==1));
        Test10 = all(abs(SActualHead(t).dphi_heave_dz-SExpectedHead(t).dphi_heave_dz)<tol); assert(all(Test10(:)==1));
        Test11 = all(abs(SActualHead(t).dphi_scatr_dy-SExpectedHead(t).dphi_scatr_dy)<tol); assert(all(Test11(:)==1));
        Test12 = all(abs(SActualHead(t).dn1dx-SExpectedHead(t).dn1dx)<tol); assert(all(Test12(:)==1));
        Test13 = all(abs(SActualHead(t).dn2dx-SExpectedHead(t).dn2dx)<tol); assert(all(Test13(:)==1));
    end
end

% 3. Test 3D results for following-seas condition

beta = 0;
U(2) = 10; % Increase the speed in order to get some negative frequencies.

UpdateTheTestResults_FollSeas = 0; % must be false after the update

if UpdateTheTestResults_FollSeas
    [SolExpectedFoll, SExpectedFoll] = DTU_StripTheorySolver(S, U, beta, omD, g, ulen, zcg, RGyr,KochinType,'all'); %#ok<UNRCH>
    save(strcat(TestFolderPath,'OutputTestDataForSolver.mat'), 'SolExpectedFoll', 'SExpectedFoll','-append');
end

[SolActual, SActualFoll] = DTU_StripTheorySolver(S, U, beta, omD, g, ulen, zcg, RGyr, KochinType , 'all');

for i = 1:length(U)
    Test1 = all(abs(SolActual(i).Fjk-SolExpectedFoll(i).Fjk)<tol); assert(all(Test1(:)==1));
    Test2 = all(abs(SolActual(i).XjD-SolExpectedFoll(i).XjD)<tol); assert(all(Test2(:)==1));
    Test3 = all(abs(SolActual(i).xi-SolExpectedFoll(i).xi)<tol); assert(all(Test3(:)==1));
    Test4 = all(abs(SolActual(i).RwMaruo-SolExpectedFoll(i).RwMaruo)<tol); assert(all(Test4(:)==1));
    Test5 = all(abs(SolActual(i).RwSalvesen-SolExpectedFoll(i).RwSalvesen)<tol); assert(all(Test5(:)==1));
end

% 4. Test 2D sectional results for following-seas condition

for t=1:length(SActualFoll)
    if SActualFoll(t).Nv~=0
        Test1 = all(abs(SActualFoll(t).Hm-SExpectedFoll(t).Hm)<tol); assert(all(Test1(:)==1));
        Test2 = all(abs(SActualFoll(t).E-SExpectedFoll(t).E)<tol); assert(all(Test2(:)==1));
        Test3 = all(abs(SActualFoll(t).phi-SExpectedFoll(t).phi)<tol); assert(all(Test3(:)==1));
        Test4 = all(abs(SActualFoll(t).f3-SExpectedFoll(t).f3)<tol); assert(all(Test4(:)==1));
        Test5 = all(abs(SActualFoll(t).h3-SExpectedFoll(t).h3)<tol); assert(all(Test5(:)==1));
        Test6 = all(abs(SActualFoll(t).hs-SExpectedFoll(t).hs)<tol); assert(all(Test6(:)==1));
        Test7 = all(abs(SActualFoll(t).ScatPhiMapped-SExpectedFoll(t).ScatPhiMapped)<tol); assert(all(Test7(:)==1));
        Test8 = all(abs(SActualFoll(t).ScatBCMapped-SExpectedFoll(t).ScatBCMapped)<tol); assert(all(Test8(:)==1));
        Test9 = all(abs(SActualFoll(t).dphi_heave_dx-SExpectedFoll(t).dphi_heave_dx)<tol); assert(all(Test9(:)==1));
        Test10 = all(abs(SActualFoll(t).dphi_heave_dz-SExpectedFoll(t).dphi_heave_dz)<tol); assert(all(Test10(:)==1));
        Test11 = all(abs(SActualFoll(t).dphi_scatr_dy-SExpectedFoll(t).dphi_scatr_dy)<tol); assert(all(Test11(:)==1));
    end
end