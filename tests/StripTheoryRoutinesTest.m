% Unit tests for Green function routines
clc
clearvars
addpath(genpath('../src'))
TestFolderPath = './tests_data/';
tol = 1e-9;

%% Test for Solve_BEM_2D_WaveG

UpdateTheTestResults = 0 ; % must be false after the update

TestXCoor  = -0.46552;
xsym = 0;
irr = 1;
beta=pi;
KochinType='2D';
CatFlag='no';

load(strcat(TestFolderPath,'x0FS.mat'));
load(strcat(TestFolderPath,'x1FS.mat'));
load(strcat(TestFolderPath, 'ds.mat'));
load(strcat(TestFolderPath,'dphidn.mat'));
load(strcat(TestFolderPath,'xvForTests.mat'));
load(strcat(TestFolderPath,'xcForTests.mat'));
load(strcat(TestFolderPath,'nuForTests.mat'));

if UpdateTheTestResults
    [fjkExpected, EExpected, HpExpected, HmExpected, phiExpected] = Solve_BEM_2D_WaveG(nuForTests,TestXCoor, xv, xc, dphidn, ds, xsym, irr, x0FS, x1FS, beta, CatFlag); %#ok<UNRCH>
    save(strcat(TestFolderPath,'fjkExpected.mat'), 'fjkExpected');
    save(strcat(TestFolderPath,'EExpected.mat'),   'EExpected');
    save(strcat(TestFolderPath,'HpExpected.mat'),  'HpExpected');
    save(strcat(TestFolderPath,'HmExpected.mat'),  'HmExpected');
    save(strcat(TestFolderPath,'phiExpected.mat'), 'phiExpected');
end

load(strcat(TestFolderPath,'fjkExpected.mat'));
load(strcat(TestFolderPath,'EExpected.mat'));
load(strcat(TestFolderPath,'HpExpected.mat'));
load(strcat(TestFolderPath,'HmExpected.mat'));
load(strcat(TestFolderPath,'phiExpected.mat'));

[fjkActual, EActual, HpActual, HmActual, phiActual] = Solve_BEM_2D_WaveG(nuForTests,TestXCoor, xv, xc, dphidn, ds, xsym, irr, x0FS, x1FS, beta, CatFlag);

Test1 = all(abs(fjkActual-fjkExpected)<tol); assert(all(Test1(:)==1));
Test2 = all(abs(EActual-EExpected)<tol);     assert(all(Test2(:)==1));
Test3 = all(abs(HpActual-HpExpected)<tol);   assert(all(Test3(:)==1));
Test4 = all(abs(HmActual-HmExpected)<tol);   assert(all(Test4(:)==1));
Test5 = all(abs(phiActual-phiExpected)<tol); assert(all(Test5(:)==1));

%% Test for Integral_LogR_LineSegment_Kashiwagi

UpdateTheTestResults = 0 ; % must be false after the update

load(strcat(TestFolderPath, 'xvForTests.mat'));
N  = length(xv)-1;
xc = xv(1:N,:) + (xv(2:N+1,:)-xv(1:N,:))/2;

if UpdateTheTestResults
    [LnExpected, TnExpected, LnpExpected, TnpExpected] = Integral_LogR_LineSegment_Kashiwagi(xc,xv,0); %#ok<UNRCH>
    save(strcat(TestFolderPath,'LnExpected.mat'), 'LnExpected');
    save(strcat(TestFolderPath,'TnExpected.mat'), 'TnExpected');
    save(strcat(TestFolderPath,'LnpExpected.mat'), 'LnpExpected');
    save(strcat(TestFolderPath, 'TnpExpected.mat'), 'TnpExpected');
end

load(strcat(TestFolderPath,'LnExpected.mat'));
load(strcat(TestFolderPath,'TnExpected.mat'));
load(strcat(TestFolderPath,'LnpExpected.mat'));
load(strcat(TestFolderPath,'TnpExpected.mat'));

[LnActual,TnActual, LnpActual, TnpActual] = Integral_LogR_LineSegment_Kashiwagi(xc,xv,0);

Test1 = all(abs(LnActual-LnExpected)<tol);   assert(all(Test1(:)==1));
Test2 = all(abs(TnActual-TnExpected)<tol);   assert(all(Test2(:)==1));
Test3 = all(abs(LnpActual-LnpExpected)<tol); assert(all(Test3(:)==1));
Test4 = all(abs(TnpActual-TnpExpected)<tol); assert(all(Test4(:)==1));

%% Test For Integral_G_Wave_LineSegment_Kashiwagi

UpdateTheTestResults = 0 ; % must be false after the update
WaveNumber = 0.0049;

load(strcat(TestFolderPath,'xvForTests.mat'));
N  = length(xv)-1;
xc = xv(1:N,:) + (xv(2:N+1,:)-xv(1:N,:))/2;

if UpdateTheTestResults
    [GrExpected, GnrExpected] = Integral_G_Wave_LineSegment_Kashiwagi(WaveNumber,xc,[-xv(N+1:-1:1,1),xv(N+1:-1:1,2)]); %#ok<UNRCH>
    save(strcat(TestFolderPath,'GrExpected.mat'), 'GrExpected');
    save(strcat(TestFolderPath,'GnrExpected.mat'), 'GnrExpected');
end

load(strcat(TestFolderPath,'GrExpected'));
load(strcat(TestFolderPath,'GnrExpected'));

[GrActual, GnrActual] = Integral_G_Wave_LineSegment_Kashiwagi(WaveNumber,xc,[-xv(N+1:-1:1,1),xv(N+1:-1:1,2)]);

Test1 = all(abs(GrActual-GrExpected)<tol);   assert(all(Test1(:)==1));
Test2 = all(abs(GnrActual-GnrExpected)<tol); assert(all(Test2(:)==1));
