% Unit tests for KochinFunction_m.m
clc
clearvars
addpath(genpath('../src'))
TestFolderPath = './tests_data/';
tol = 1e-9;

%% Test For KochinFunction_m

load(strcat(TestFolderPath,'InputForKochinFunctionTests.mat'));

UpdateTheTestResults = 0; % must be false after the update

if UpdateTheTestResults
    [ExpectedHp, ExpectedHm] = KochinFunction_m(K, mvec, xj, phi, phin,1); %#ok<UNRCH>
    save(strcat(TestFolderPath,'ExpectedKochinFunctionValues.mat'), 'ExpectedHp', 'ExpectedHm');
end
load(strcat(TestFolderPath,'ExpectedKochinFunctionValues.mat'));
[ActualHp, ActualHm]  = KochinFunction_m(K, mvec, xj, phi, phin,1);

Test1 = abs(ActualHp - ExpectedHp)<tol; assert(Test1(:)==1);
Test2 = abs(ActualHm - ExpectedHm)<tol; assert(Test2(:)==1);