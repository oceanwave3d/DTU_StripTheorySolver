% Unit tests for GetIntertiaMatrix.m
clc
clearvars
addpath(genpath('../src'))
TestFolderPath = './tests_data/';
tol = 1e-9;

%% Test for GetIntertiaMatrix
%
load(strcat(TestFolderPath,'InputForGetIntertiaMatrixTests.mat'));

UpdateTheTestResults = 0; % must be false after the update

if UpdateTheTestResults
    ExpectedMjk = GetInertiaMatrix(Vol, Xg, Zg, RGyr); %#ok<UNRCH>
    save(strcat(TestFolderPath,'ExpectedIntertiaMatrix.mat'), 'ExpectedMjk');
end
load(strcat(TestFolderPath,'ExpectedIntertiaMatrix.mat'));
ActualMjk = GetInertiaMatrix(Vol, Xg, Zg, RGyr);

Test1 = all(abs(ActualMjk - ExpectedMjk)<tol); assert(all(Test1(:)==1));