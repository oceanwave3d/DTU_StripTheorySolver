% Unit tests for ARGerritsmaBeukelman.m
clc
clearvars
addpath(genpath('../src'))
TestFolderPath = './tests_data/';
tol = 1e-9;

%% Test for ARGerritsmaBeukelman

load(strcat(TestFolderPath,'InputForARGBTests.mat'));

UpdateTheTestResults = 0; % must be false after the update

ActualRwGB = zeros(nf,1);

if UpdateTheTestResults
    ExpectedRwGB = zeros(nf,1); %#ok<UNRCH>
    for i=1:nf
        ExpectedRwGB(i) = ARGerritsmaBeukelman(Fn, xi(i,:), om0(i), om(i), S, i, beta, GBCoeff);
    end
    save(strcat(TestFolderPath,'ExpectedRwGB.mat'), 'ExpectedRwGB');
end
load(strcat(TestFolderPath,'ExpectedRwGB.mat'));
for i=1:nf
    ActualRwGB(i) = ARGerritsmaBeukelman(Fn, xi(i,:), om0(i), om(i), S, i, beta, GBCoeff);
end

Test = abs(ActualRwGB-ExpectedRwGB)<tol;
assert(all(Test()==1))