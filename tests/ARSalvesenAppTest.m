% Unit tests for ARSalvesenApp.m
clc
clearvars
addpath(genpath('../src'))
TestFolderPath = './tests_data/';
tol = 1e-9;

%% Test for ARSalvesenApp

load(strcat(TestFolderPath,'InputForARSalvesenAppTests.mat'));

UpdateTheTestResults = 0; % must be false after the update

ActualRwSalvesenApp = zeros(nf,1);

if UpdateTheTestResults
    ExpectedRwSalvesenApp = zeros(nf,1); %#ok<UNRCH>
    for i=1:nf
        ExpectedRwSalvesenApp(i) = ARSalvesenApp(Fn, xi(i,:), om0(i), om(i), S, i, beta);
    end
    save(strcat(TestFolderPath,'ExpectedRwSalvesenApp.mat'), 'ExpectedRwSalvesenApp');
end
load(strcat(TestFolderPath,'ExpectedRwSalvesenApp.mat'));
for i=1:nf
    ActualRwSalvesenApp(i) = ARSalvesenApp(Fn, xi(i,:), om0(i), om(i), S, i, beta);
end

Test1 = abs(ActualRwSalvesenApp-ExpectedRwSalvesenApp)<tol;
assert(all(Test1(:)==1));