% Unit tests for FDCoefficients.m
clc
clearvars
addpath(genpath('../src'))
TestFolderPath = './tests_data/';
tol = 1e-9;

%% Test for FDCoefficients

UpdateTheTestResults = 0; % must be false after the update

if UpdateTheTestResults
    ExpectedC1 = FDCoefficients(0, 4); %#ok<UNRCH>
    ExpectedC2 = FDCoefficients(2, 2);
    ExpectedC3 = FDCoefficients(1, 3);
    ExpectedC4 = FDCoefficients(3, 1);
    ExpectedC5 = FDCoefficients(4, 0);

    save(strcat(TestFolderPath,'ExpectedFDCoeffs.mat'), 'ExpectedC1', 'ExpectedC2', 'ExpectedC3', 'ExpectedC4', 'ExpectedC5' );
end
load(strcat(TestFolderPath,'ExpectedFDCoeffs.mat'));

ActualC1 = FDCoefficients(0, 4);
ActualC2 = FDCoefficients(2, 2);
ActualC3 = FDCoefficients(1, 3);
ActualC4 = FDCoefficients(3, 1);
ActualC5 = FDCoefficients(4, 0);

Test1 = all(abs(ActualC1 - ExpectedC1)<tol); assert(all(Test1(:,:)==1));
Test2 = all(abs(ActualC2 - ExpectedC2)<tol); assert(all(Test2(:,:)==1));
Test3 = all(abs(ActualC3 - ExpectedC3)<tol); assert(all(Test3(:,:)==1));
Test4 = all(abs(ActualC4 - ExpectedC4)<tol); assert(all(Test4(:,:)==1));
Test5 = all(abs(ActualC5 - ExpectedC5)<tol); assert(all(Test5(:,:)==1));
