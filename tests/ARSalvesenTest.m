% Unit tests for ARSalvesen.m
clc
clearvars
addpath(genpath('../src'))
TestFolderPath = './tests_data/';
tol = 1e-9;

%% Test for ARSalvesen

load(strcat(TestFolderPath,'InputForARSalvesenTests.mat'));
S(2).CatFlag='no';

UpdateTheTestResults = 0; % must be false after the update

ActualRwSalvesen = zeros(nf,1);

if UpdateTheTestResults
    ExpectedRwSalvesen = zeros(nf,1); %#ok<UNRCH>
    for i=1:nf
        ExpectedRwSalvesen(i) = ARSalvesen(Fn, xi(i,:), om0(i), om(i), S, i, beta, KochinType);
    end
    save(strcat(TestFolderPath,'ExpectedRwSalvesen.mat'), 'ExpectedRwSalvesen');
end
load(strcat(TestFolderPath,'ExpectedRwSalvesen.mat'));
for i=1:nf
    ActualRwSalvesen(i) = ARSalvesen(Fn, xi(i,:), om0(i), om(i), S, i, beta, KochinType);
end

Test1 = abs(ActualRwSalvesen-ExpectedRwSalvesen)<tol;
assert(all(Test1()==1));