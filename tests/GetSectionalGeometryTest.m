% Unit Tests for GetSectionalGeometry.m
clc
clearvars
addpath(genpath('../src'))
TestFolderPath = './tests_data/';
tol = 1e-9;

%% Test For GetSectionalGeometry

nst = 45;

load(strcat(TestFolderPath,'SForGetSectionTests.mat'));
load(strcat(TestFolderPath,'LForGetSectionTests.mat'));
S(2).CatFlag='no';

UpdateTheTestResults = 0; % must be false after the update

if UpdateTheTestResults
    SForGetSectionTests=Compute3DNormals(SForGetSectionTests); %#ok<UNRCH>
    for t=1:nst
        ExpectedSecProps(t) = GetSectionalGeometry(SForGetSectionTests(t), L, 0);
    end
    save(strcat(TestFolderPath,'ExpectedSecProps.mat'),'ExpectedSecProps');
end
load(strcat(TestFolderPath,'ExpectedSecProps.mat'));
SForGetSectionTests=Compute3DNormals(SForGetSectionTests);

for t=1:nst
    ActualSecProps = GetSectionalGeometry(SForGetSectionTests(t), L, 0);
    TestNv     = ActualSecProps.Nv     - ExpectedSecProps(t).Nv     <tol; assert(TestNv==1);
    Testxv     = ActualSecProps.xv     - ExpectedSecProps(t).xv     <tol; assert(all(Testxv(:)==1));
    Testb      = ActualSecProps.b      - ExpectedSecProps(t).b      <tol; assert(Testb==1);
    Testds     = ActualSecProps.ds     - ExpectedSecProps(t).ds     <tol; assert(all(Testds(:)==1));
    Testnv     = ActualSecProps.nv     - ExpectedSecProps(t).nv     <tol; assert(all(Testnv(:)==1));
    Testxc     = ActualSecProps.xc     - ExpectedSecProps(t).xc     <tol; assert(all(Testxc(:)==1));
    TestBeam   = ActualSecProps.beam   - ExpectedSecProps(t).beam   <tol; assert(TestBeam(:)==1);
    TestArea   = ActualSecProps.area   - ExpectedSecProps(t).area   <tol; assert(TestArea(:)==1);
    TestDraft  = ActualSecProps.draft  - ExpectedSecProps(t).draft  <tol; assert(TestDraft(:)==1);
    TestAreaND = ActualSecProps.areaND - ExpectedSecProps(t).areaND <tol; assert(TestAreaND(:)==1);
    TestxCoor  = ActualSecProps.xCoor  - ExpectedSecProps(t).xCoor  <tol; assert(TestxCoor(:)==1);
    Testn3D    = ActualSecProps.n3D    - ExpectedSecProps(t).n3D    <tol; assert(all(Testn3D(:)==1));
end