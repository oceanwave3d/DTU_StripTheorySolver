% Unit tests for FDCoefficientsNonUniform.m
clc
clearvars
addpath(genpath('../src'))
TestFolderPath = './tests_data/';
tol = 1e-9;

%% Test for FDCoefficientsNonUniform
%
load(strcat(TestFolderPath,'InputForFDNonUniTests.mat'));

UpdateTheTestResults = 0; % must be false after the update

if UpdateTheTestResults
    [cExp, aExp, bExp, dExp] = FDCoefficientsNonUniform(xForGB, 2, 2); %#ok<UNRCH>
    save(strcat(TestFolderPath,'ExpectedFDNonUniCoeffs.mat'), 'cExp', 'aExp', 'bExp', 'dExp');
end
load(strcat(TestFolderPath,'ExpectedFDNonUniCoeffs.mat'));
[cAct, aAct, bAct, dAct] = FDCoefficientsNonUniform(xForGB, 2, 2);

Test1 = all(abs(cAct - cExp)<tol); assert(all(Test1(:,:)==1));
Test2 = all(abs(aAct - aExp)<tol); assert(all(Test2(:)==1));
Test3 = all(abs(bAct - bExp)<tol); assert(all(Test3(:)==1));
Test4 = all(abs(dAct - dExp)<tol); assert(all(Test4(:)==1));