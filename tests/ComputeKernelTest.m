% Unit tests for ComputeKernel.m
clc
clearvars
addpath(genpath('../src'))
TestFolderPath = './tests_data/';
tol = 1e-9;

%% Test for ComputeKernel

% tau < 1/4
load(strcat(TestFolderPath,'InputForKernelTestL.mat'));

parms.Fr = Fr;
parms.tau = tau;
parms.k1 = k1;
parms.k2 = k2;
parms.k3 = k3;
parms.k4 = k4;

UpdateTheTestResults = 0; % must be false after the update

if UpdateTheTestResults
    ExpectedWL = ComputeKernel(x, parms); %#ok<UNRCH>
    save(strcat(TestFolderPath,'ExpectedWL.mat'), 'ExpectedWL');
end
load(strcat(TestFolderPath,'ExpectedWL.mat'));
ActualWL = ComputeKernel(x, parms);

TestL = abs(ExpectedWL - ActualWL)<tol; assert(TestL(:)==1);

% tau > 1/4
load(strcat(TestFolderPath,'InputForKernelTestG.mat'));

parms.Fr = Fr;
parms.tau = tau;
parms.k1 = k1;
parms.k2 = k2;
parms.k3 = 0;
parms.k4 = 0;

UpdateTheTestResults = 0; % must be false after the update

if UpdateTheTestResults
    ExpectedWG = ComputeKernel(x, parms); %#ok<UNRCH>
    save(strcat(TestFolderPath,'ExpectedWG.mat'), 'ExpectedWG');
end
load(strcat(TestFolderPath,'ExpectedWG.mat'));
ActualWG = ComputeKernel(x, parms);

TestG = abs(ExpectedWG - ActualWG)<tol; assert(TestG(:)==1);