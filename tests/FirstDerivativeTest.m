% Unit tests for FirstDerivative.m
clc
clearvars
addpath(genpath('../src'))
TestFolderPath = './tests_data/';
tol = 1e-9;

%% Test for FirstDerivative
%
load(strcat(TestFolderPath,'InputDataForFirstDerivativeTests.mat'));

UpdateTheTestResults = 0; % must be false after the update

if UpdateTheTestResults
    ExpectedDADX = FirstDerivative(xst, a33, GBCoeff); %#ok<UNRCH>
    save(strcat(TestFolderPath,'ExpectedAddedMassDerivative.mat'), 'ExpectedDADX');
end
load(strcat(TestFolderPath,'ExpectedAddedMassDerivative.mat'));
ActualDADX = GetAddedMassDerivative(xst, a33, GBCoeff);

Test1 = all(abs(ExpectedDADX - ActualDADX)<tol); assert(Test1(:)==1);