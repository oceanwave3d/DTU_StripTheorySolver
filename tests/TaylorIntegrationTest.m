% Unit tests for TaylorIntegration.m
clc
clearvars
addpath(genpath('../src'))
TestFolderPath = './tests_data/';
tol = 1e-9;

%% Test for TaylorIntegration
%
load(strcat(TestFolderPath,'InputDataForTaylorIntegrationTests.mat'));

UpdateTheTestResults = 0; % must be false after the update

if UpdateTheTestResults
    Expected = TaylorIntegration (N2, integrand, DL, C, Cleft, Cright); %#ok<UNRCH>
    save(strcat(TestFolderPath,'ExpectedIntegralValue.mat'), 'Expected');
end
load(strcat(TestFolderPath,'ExpectedIntegralValue.mat'));
Actual = TaylorIntegration (N2, integrand, DL, C, Cleft, Cright);

Test1 = abs(Expected - Actual)<tol; assert(Test1==1);