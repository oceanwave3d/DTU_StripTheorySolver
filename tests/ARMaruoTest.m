% Unit tests for ARMaruo.m
clc
clearvars
addpath(genpath('../src'))
TestFolderPath = './tests_data/';
tol = 1e-9;

%% Test for ARMaruo

load(strcat(TestFolderPath,'InputForARMaruoTests.mat'));
S(2).CatFlag='no';

UpdateTheTestResults = 0; % must be false after the update

ActualRwMaruo   = zeros(nf,1);
ActualRwMaruoZS = zeros(nf,1);

if UpdateTheTestResults
    ExpectedRwMaruo   = zeros(nf,1); %#ok<UNRCH>
    ExpectedRwMaruoZS = zeros(nf,1);
    for i=1:nf
        ExpectedRwMaruo  (i) = ARMaruo(Fn, xi(i,:), om0(i),  om(i), S, i, beta, MaruoCoeff, KochinType);
        ExpectedRwMaruoZS(i) = ARMaruo(0 , xi(i,:), om0(i), om0(i), S, i, beta, MaruoCoeff, KochinType);
    end
    save(strcat(TestFolderPath,'ExpectedRwMaruo.mat'), 'ExpectedRwMaruo', 'ExpectedRwMaruoZS');
end
load(strcat(TestFolderPath,'ExpectedRwMaruo.mat'));
for i=1:nf
    ActualRwMaruo  (i) = ARMaruo(Fn, xi(i,:), om0(i), om(i) , S, i, beta, MaruoCoeff, KochinType);
    ActualRwMaruoZS(i) = ARMaruo(0 , xi(i,:), om0(i), om0(i), S, i, beta, MaruoCoeff, KochinType);
end

Test1 = abs(ActualRwMaruo-ExpectedRwMaruo)<tol;
assert(all(Test1(:)==1));
Test2 = abs(ActualRwMaruoZS-ExpectedRwMaruoZS)<tol;
assert(all(Test2(:)==1));