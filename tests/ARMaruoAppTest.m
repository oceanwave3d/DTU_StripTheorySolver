% Unit tests for the ARMaruoApp.m
clc
clearvars
addpath(genpath('../src'))
TestFolderPath = './tests_data/';
tol = 1e-9;

%% Test for ARMaruoApp

load(strcat(TestFolderPath,'InputForARMaruoAppTests.mat'));
xst = 0;
UpdateTheTestResults = 0; % must be false after the update

ActualRwMaruoApp   = zeros(nf,1);
ActualRwMaruoAppZS = zeros(nf,1);

if UpdateTheTestResults
    ExpectedRwMaruoApp   = zeros(nf,1); %#ok<UNRCH>
    ExpectedRwMaruoAppZS = zeros(nf,1);
    for i=1:nf
        ExpectedRwMaruoApp  (i) = ARMaruoApp(Fn, xi(i,:), om0(i),  om(i), S, i, beta, MaruoCoeff);
        ExpectedRwMaruoAppZS(i) = ARMaruoApp(0 , xi(i,:), om0(i), om0(i), S, i, beta, MaruoCoeff);
    end
    save(strcat(TestFolderPath,'ExpectedRwMaruoApp.mat'), 'ExpectedRwMaruoApp', 'ExpectedRwMaruoAppZS');
end
load(strcat(TestFolderPath,'ExpectedRwMaruoApp.mat'));
for i=1:nf
    ActualRwMaruoApp  (i) = ARMaruoApp(Fn, xi(i,:), om0(i), om(i) , S, i, beta, MaruoCoeff);
    ActualRwMaruoAppZS(i) = ARMaruoApp(0 , xi(i,:), om0(i), om0(i), S, i, beta, MaruoCoeff);
end

Test1 = abs(ActualRwMaruoApp-ExpectedRwMaruoApp)<tol;
assert(all(Test1()==1));
Test2 = abs(ActualRwMaruoAppZS-ExpectedRwMaruoAppZS)<tol;
assert(all(Test2()==1));