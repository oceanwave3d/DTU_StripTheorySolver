% Unit tests for ComputeSurfaceDerivatives.m
clc
clearvars
addpath(genpath('../src'))
TestFolderPath = './tests_data/';
tol = 1e-9;

%% Test for ComputeSurfaceDerivatives

nst = 30;
load(strcat(TestFolderPath,'InputForComputeSurfaceDerivatives.mat'));

UpdateTheTestResults = 0; % must be false after the update

if UpdateTheTestResults
    ExpectedXDerivatives = ComputeSurfaceDerivatives(InputForComputeSurfaceDerivatives); %#ok<UNRCH> 
    save(strcat(TestFolderPath,'ExpectedXDerivatives.mat'), 'ExpectedXDerivatives');
end
load(strcat(TestFolderPath,'ExpectedXDerivatives.mat'));
ActualXDerivatives = ComputeSurfaceDerivatives(InputForComputeSurfaceDerivatives);

for i=1:nst
    TestDn1Dx = all(abs(ActualXDerivatives(i).dn1dx - ExpectedXDerivatives(i).dn1dx)<tol); assert(TestDn1Dx==1);
    TestDn2Dx = all(abs(ActualXDerivatives(i).dn2dx - ExpectedXDerivatives(i).dn2dx)<tol); assert(TestDn2Dx==1);
    TestDphi2Dx = all(abs(ActualXDerivatives(i).dphi_heave_dx - ExpectedXDerivatives(i).dphi_heave_dx)<tol); assert(all(TestDphi2Dx(:)==1));
    TestDphisDx = all(abs(ActualXDerivatives(i).dphi_scatr_dx - ExpectedXDerivatives(i).dphi_scatr_dx)<tol); assert(all(TestDphisDx(:)==1));
end
