%
% This function implements Maruo's method for calculation of mean second-order wave
% drift force (added resistance) based on the Kochin Functions. For further details
% on Maruo's method see:
% H. Maruo, “Wave resistance of a ship in regular head seas,” Bulletin of the Faculty
% of Engineering, Yokohama National University, vol. vol. 9, 1960.
%
% Written by: Mostafa Amini-Afshar
%

function [Rw, Kochin1, Kochin2, Kochin3] = ARMaruo(U, rao, omegao, omegae, S, findex, beta, MaruoCoeff, KochinType)

C      = MaruoCoeff.C;
Cleft  = MaruoCoeff.Cleft;
Cright = MaruoCoeff.Cright;

xst   = vertcat(S.xCoor);
g     = 1;
rho   = 1;
K     = omegao^2/g;
N1    = 150;
N2    = 60;
N3    = 300;
nst   = length(xst);
pm = 1; if sin(beta)<0, pm = -1; end

Kochin1 = struct('Heave',{},'Pitch',{},'Scatr',{},'mvec', {});
Kochin2 = struct('Heave',{},'Pitch',{},'Scatr',{},'mvec', {});
Kochin3 = struct('Heave',{},'Pitch',{},'Scatr',{},'mvec', {});

if U~=0 % Added resistance (Forward-Speed Cases)
    kNyq      = 2*GetNyquistWaveNumber(xst);
    tau       = U * omegae/g;
    kappaZero = g/U^2;
    kBar1     = -kappaZero/2 * (1 + 2 * tau + sqrt(1 + 4 * tau));
    kBar2     = -kappaZero/2 * (1 + 2 * tau - sqrt(1 + 4 * tau));

    if tau < 0.25
        kBar3 =  kappaZero/2 * (1 - 2 * tau - sqrt(1 - 4 * tau));
        kBar4 =  kappaZero/2 * (1 - 2 * tau + sqrt(1 - 4 * tau));
    else
        kBar3 = kappaZero * tau;
        kBar4 = kappaZero * tau;
    end
else % Wave Drift Froce (Zero-Speed Cases)
    kBar1 =  0;
    kBar2 = -K;
    kBar3 =  K;
    kBar4 =  0;
    kNyq  =  0;
end

Rw1  =  0;
Rw2  =  0;
Rw3  =  0;

finteg = 1; % 1 If The 1st Integral Should Be Included
sinteg = 1; % 1 If The 2nd Integral Should Be Included
tinteg = 1; % 1 If The 3rd Integral Should Be Included

if kBar1 >-kNyq && finteg && U~=0
    DL = (kBar1+kNyq)/(N1-1);
    [integrand, Kochin1] = GetIntegrand(-kNyq, kBar1, N1);
    Rw1 = TaylorIntegration(N1, integrand, DL, C, Cleft, Cright);

end

if kBar3 > kBar2 && sinteg
    DL = (kBar3-kBar2)/(N2-1);
    [integrand, Kochin2] = GetIntegrand(kBar2, kBar3, N2);
    Rw2 = TaylorIntegration(N2, integrand, DL, C, Cleft, Cright);
end

if kNyq > kBar4 && tinteg && U~=0
    DL = (kNyq-kBar4)/(N3-1);
    [integrand, Kochin3] = GetIntegrand(kBar4, kNyq, N3);
    Rw3 = TaylorIntegration(N3, integrand, DL, C, Cleft, Cright);
end

Rw = rho/(4*pi) * (-Rw1 + Rw2 + Rw3);

% ---

    function [integrand, Kochin] = GetIntegrand(kLow, kUp, N)

        integrand  = zeros(N,1);
        StripHeave = zeros(nst,1);
        StripScatr = zeros(nst,1);
        HTotal     = zeros(N,1);
        HHeave     = zeros(N,1);
        HPitch     = zeros(N,1);
        HScatr     = zeros(N,1);
        mvec       = linspace(kLow, kUp, N);
        CatFlag=S(2).CatFlag;

        for p=1:N
            m = mvec(p);
            kappaBar = 1/g*(omegae + m*U)^2;

            for t=1:nst
                if S(t).Nv == 0
                    StripHeave(t) = 0;
                    StripScatr(t) = 0;
                else
                    switch CatFlag
                        case 'yes'
                            Nv=size(S(t).xv,1);
                            Np=Nv-2;
                            switch KochinType
                                case '2D'
                                    [f1, ~] = KochinFunction_m(omegae^2/g, 0, S(t).xv(1:Np/2+1,:), ...
                                        S(t).HeavePhiMapped(1:Np/2,findex), S(t).nv(1:Np/2,2),pm);
                                    [f2, ~] = KochinFunction_m(omegae^2/g, 0, S(t).xv(Np/2+2:Np+2,:),...
                                        S(t).HeavePhiMapped(Np/2+1:Np,findex), S(t).nv(Np/2+1:Np,2),pm);
                                    StripHeave(t)=f1+f2;
                                    [f1, ~] = KochinFunction_m(omegao^2/g, 0, S(t).xv(1:Np/2+1,:),...
                                        S(t).ScatPhiMapped(1:Np/2,findex), S(t).ScatBCMapped(1:Np/2,findex),pm);
                                    [f2, ~] = KochinFunction_m(omegao^2/g, 0, S(t).xv(Np/2+2:Np+2,:), ...
                                        S(t).ScatPhiMapped(Np/2+1:Np,findex), S(t).ScatBCMapped(Np/2+1:Np,findex),pm);
                                    StripScatr(t)=f1+f2;
                                case '3D'
                                    error('not implemented yet')
                            end
                        otherwise
                            switch KochinType
                                case '2D'
                                    [StripHeave(t), ~] = KochinFunction_m(omegae^2/g, 0, S(t).xv,...
                                        S(t).HeavePhiMapped(:,findex), S(t).nv(:,2),pm);
                                    [StripScatr(t), ~] = KochinFunction_m(omegao^2/g, 0, S(t).xv,...
                                        S(t).ScatPhiMapped(:,findex), S(t).ScatBCMapped(:,findex),pm);
                                case '3D'
                                    [StripHeave(t), ~] = KochinFunction_m_3D(omegae^2/g, 0, S(t).xv,...
                                        S(t).phi(:,findex,2), S(t).n3D(:,3),S(t).n3D);
                                    [StripScatr(t), ~] = KochinFunction_m_3D(omegao^2/g, 0, S(t).xv,...
                                        S(t).ScatPhiMapped(:,findex), S(t).ScatBCMapped(:,findex),S(t).n3D);
                            end
                    end
                end
            end

            for t=1:nst-1
                x0 = xst(t);
                x1 = xst(t+1);
                % Heave
                ah = (StripHeave(t)-StripHeave(t+1))/(x0-x1);
                bh = StripHeave(t+1)-ah*x1;
                % Pitch
                y1 = StripHeave(t+1)*(-x1+U/(1i*omegae));
                y0 = StripHeave(t)  *(-x0+U/(1i*omegae));
                ap = (y0-y1)/(x0-x1);
                bp = y1-ap*x1;
                % Scattering
                as = (StripScatr(t)-StripScatr(t+1))/(x0-x1);
                bs = StripScatr(t+1)-as*x1;

                HHeave(p) = HHeave(p) + 1/m^2*( (exp(1i*m*x1)*(ah-1i*ah*m*x1-1i*bh*m))-((exp(1i*m*x0)*(ah-1i*ah*m*x0-1i*bh*m))) );
                HPitch(p) = HPitch(p) + 1/m^2*( (exp(1i*m*x1)*(ap-1i*ap*m*x1-1i*bp*m))-((exp(1i*m*x0)*(ap-1i*ap*m*x0-1i*bp*m))) );
                HScatr(p) = HScatr(p) + 1/m^2*( (exp(1i*m*x1)*(as-1i*as*m*x1-1i*bs*m))-((exp(1i*m*x0)*(as-1i*as*m*x0-1i*bs*m))) );
            end

            HHeave(p) = HHeave(p) * 1i * omegae * rao(3);
            HPitch(p) = HPitch(p) * 1i * omegae * rao(5);
            HScatr(p) = HScatr(p) * 1/(1i*omegao);

            HTotal(p) = HHeave(p) + HPitch(p) + HScatr(p);

            integrand(p) = kappaBar * (m - K*cos(beta))/sqrt(kappaBar^2 - m^2) * (abs(HTotal(p)))^2;
        end

        Kochin.Heave = HHeave;
        Kochin.Pitch = HPitch;
        Kochin.Scatr = HScatr;
        Kochin.mvec  = mvec';
    end
end
