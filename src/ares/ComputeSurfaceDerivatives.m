function [S] = ComputeSurfaceDerivatives(S)

% This script tests an evaluation of the x-derivative of a function defined
% on a set of structured points along the Wigley hull geometry. Following
% Bingham & Maniar IWWWFB (1996) Eqs. (8) & (9), we apply arbitrary order
% finite differences to evaluate all of the required surface derivatives,
% which are then combined with the normal derivative to find the
% x-derivative.
%
% Written by H.B. Bingham, November 2020.

%%
Sd  = S(vertcat(S.Nv)==0); % The dummy stations
S   = S(vertcat(S.Nv)>0);  % Remove the dummy stations
nst = length(S);
nfq = length(S(2).fjk);
%
% The parameter u runs from 0 to 1 from stern to bow, and the parameter v
% runs from 0 to 1 from free surface to the keel along the body surface.
% Thus the d/du and d/dv operators are approximated by FD matrices on a
% uniform grid.
%
npans = length(S(2).xc);

S = ComputePatchDerivatives(S, 1:npans/2);
S = ComputePatchDerivatives(S, npans/2+1:npans);

    function [S] = ComputePatchDerivatives(S, range)
        
        Nc=length(S(2).xc(range));  % The number of panel centroids.
        alpha=2;  % The half-width of the FD stencils.
        %
        % The u,v coordinates
        %
        u=1/(nst-1)*[0:nst-1]';
        v=1/Nc*(1/2+[0:Nc-1]');
        %
        % The derivative matrices ordered to follow v-first then u.
        %
        Du=BuildDx2DOneSided(nst,Nc,u,alpha,1);
        Dv=BuildDy2DOneSided(nst,Nc,v,alpha,1);
        %
        % Extract the coordinates following v first then u.
        %
        N=nst*Nc;
        x=zeros(N,3);  % Initialize the coordinates
        for i=1:nst
            x((i-1)*Nc+1:i*Nc,1)=S(i).xst;
            x((i-1)*Nc+1:i*Nc,2)=S(i).xc(range,1);
            x((i-1)*Nc+1:i*Nc,3)=S(i).xc(range,2);
            %
            n1(:,i) = S(i).nv(range,1); % 2D normnals used for surge
            n2(:,i) = S(i).nv(range,2); % 2D normnals used for heave
        end
        
        n1 = reshape(n1, N,1);
        n2 = reshape(n2, N,1);
        
        %
        % Take all of the coordinate derivatives and build the operators in Eqs.
        % (8) & (9) of Bingham & Maniar (1996).
        %
        xu(:,1)=Du*x(:,1); xu(:,2)=Du*x(:,2); xu(:,3)=Du*x(:,3);
        xv(:,1)=Dv*x(:,1); xv(:,2)=Dv*x(:,2); xv(:,3)=Dv*x(:,3);
        for i=1:N
            E(i,1)=xu(i,:)*xu(i,:)';
            F(i,1)=xu(i,:)*xv(i,:)';
            G(i,1)=xv(i,:)*xv(i,:)';
            n(i,1)=xv(i,2)*xu(i,3)-xv(i,3)*xu(i,2);
            n(i,2)=xv(i,3)*xu(i,1)-xv(i,1)*xu(i,3);
            n(i,3)=xv(i,1)*xu(i,2)-xv(i,2)*xu(i,1);
        end
        
        H=sqrt(E.*G-F.^2);
        for i=1:3
            n(:,i)=n(:,i)./H;
        end
        
        dn1dx = reshape(GetDerivative(n1, 0*n1), Nc, nst); % For surge mode
        dn2dx = reshape(GetDerivative(n2, 0*n2), Nc, nst); % For heave mode
        
        phi_heave   = zeros(Nc,nst);
        phi_scatr   = zeros(Nc,nst);
        phi_scatr_n = zeros(Nc,nst);
        
        for f=1:nfq
            
            for t=1:nst
                phi_heave(:,t)   = S(t).HeavePhiMapped(range,f);
                phi_scatr(:,t)   = S(t).ScatPhiMapped(range,f);
                phi_scatr_n(:,t) = S(t).ScatBCMapped(range,f);
            end
            
            phi3  = reshape(phi_heave,   N,1); % Heave potential
            phis  = reshape(phi_scatr,   N,1); % Scattering potential
            phisn = reshape(phi_scatr_n, N,1); % Scattering BC.
            
            [real_dphi2dx, real_dphi2dy, real_dphi2dz] = GetDerivative(real(phi3), n2);
            real_dphi2dx = reshape(real_dphi2dx, Nc,nst);
            real_dphi2dy = reshape(real_dphi2dy, Nc,nst);
            real_dphi2dz = reshape(real_dphi2dz, Nc,nst);
            
            [imag_dphi2dx, imag_dphi2dy, imag_dphi2dz] = GetDerivative(imag(phi3), n2);
            imag_dphi2dx = reshape(imag_dphi2dx, Nc,nst);
            imag_dphi2dy = reshape(imag_dphi2dy, Nc,nst);
            imag_dphi2dz = reshape(imag_dphi2dz, Nc,nst);
            
            dphi2dx = real_dphi2dx + 1i*imag_dphi2dx;
            dphi2dy = real_dphi2dy + 1i*imag_dphi2dy;
            dphi2dz = real_dphi2dz + 1i*imag_dphi2dz;
            
            [real_dphisdx, real_dphisdy, real_dphisdz] = GetDerivative(real(phis), real(phisn));
            real_dphisdx = reshape(real_dphisdx, Nc,nst);
            real_dphisdy = reshape(real_dphisdy, Nc,nst);
            real_dphisdz = reshape(real_dphisdz, Nc,nst);
            [imag_dphisdx, imag_dphisdy, imag_dphisdz] = GetDerivative(imag(phis), imag(phisn));
            imag_dphisdx = reshape(imag_dphisdx, Nc,nst);
            imag_dphisdy = reshape(imag_dphisdy, Nc,nst);
            imag_dphisdz = reshape(imag_dphisdz, Nc,nst);
            
            dphisdx = real_dphisdx + 1i*imag_dphisdx;
            dphisdy = real_dphisdy + 1i*imag_dphisdy;
            dphisdz = real_dphisdz + 1i*imag_dphisdz;
            
            for t=1:nst
                S(t).dphi_heave_dx(range,f) = dphi2dx(:,t);
                S(t).dphi_heave_dy(range,f) = dphi2dy(:,t);
                S(t).dphi_heave_dz(range,f) = dphi2dz(:,t);
                S(t).dphi_scatr_dx(range,f) = dphisdx(:,t);
                S(t).dphi_scatr_dy(range,f) = dphisdy(:,t);
                S(t).dphi_scatr_dz(range,f) = dphisdz(:,t);
                if f==1
                    S(t).dn1dx(range,1) = dn1dx(:,t);
                    S(t).dn2dx(range,1) = dn2dx(:,t);
                end
            end
        end
        
        function[phix, phiy, phiz] = GetDerivative(phi, phin)
            %
            % Take the surface derivatives of phi.
            %
            phiu=Du*phi; phiv=Dv*phi;
            %
            % The x-component of Eq. (8) gives the x-derivative.
            %
            phix=1./H.^2.*(xu(:,1).*(G.*phiu-F.*phiv) + xv(:,1).*(E.*phiv-F.*phiu)) + phin.*n(:,1);
            phiy=1./H.^2.*(xu(:,2).*(G.*phiu-F.*phiv) + xv(:,2).*(E.*phiv-F.*phiu)) + phin.*n(:,2);
            phiz=1./H.^2.*(xu(:,3).*(G.*phiu-F.*phiv) + xv(:,3).*(E.*phiv-F.*phiu)) + phin.*n(:,3);
        end
    end

for m=1:length(Sd)
    Sd(m).dn1dx=[];
    Sd(m).dn2dx=[];
    Sd(m).dphi_heave_dx=[];
    Sd(m).dphi_heave_dy=[];
    Sd(m).dphi_heave_dz=[];
    Sd(m).dphi_scatr_dx=[];
    Sd(m).dphi_scatr_dy=[];
    Sd(m).dphi_scatr_dz=[];
end

if length(Sd)==2
    S = [Sd(1), S, Sd(2)];
elseif length(Sd)==1 && Sd().xst<0
    S = [Sd(1), S];
elseif length(Sd)==1
    S = [S, Sd(1)];
end

end

