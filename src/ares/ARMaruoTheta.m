%
% This function implements Maruo's method for calculation of mean second-order wave
% drift force (added resistance) based on the Kochin Functions. For further details
% on Maruo's method see:
% H. Maruo, “Wave resistance of a ship in regular head seas,” Bulletin of the Faculty
% of Engineering, Yokohama National University, vol. vol. 9, 1960.
%
% Written by: Mostafa Amini-Afshar
%

function [Rw, Kochin1, Kochin2, Kochin3, Kochin4] = ARMaruoTheta(U, rao, omegao, omegae, S, findex, beta)

Kochin1 = struct('Heave',{},'Pitch',{},'Scatr',{},'tvec', {});
Kochin2 = struct('Heave',{},'Pitch',{},'Scatr',{},'tvec', {});
Kochin3 = struct('Heave',{},'Pitch',{},'Scatr',{},'tvec', {});
Kochin4 = struct('Heave',{},'Pitch',{},'Scatr',{},'tvec', {});

S     = S(vertcat(S.Nv)>0); % Remove the dummy stations
xst   = vertcat(S.xCoor);
g     = 1;
rho   = 1;
K     = omegao^2/g;
N1    = 100;
N2    = 200;
N3    = 100;
N4    = 100;
nst   = length(xst);

tau    = U * omegae/g;
kappa0 = g/U^2;
theta0 = 0; if tau > 0.25, theta0 = acos(1/(4*tau)); end

Rw1 = 0;
Rw2 = 0;
Rw3 = 0;
Rw4 = 0;

integ1 = 0; % 1 If The 1st Integral Should Be Included
integ2 = 0; % 1 If The 2nd Integral Should Be Included
integ3 = 0; % 1 If The 3rd Integral Should Be Included
integ4 = 1; % 1 If The 4th Integral Should Be Included

if integ1 && U~=0
    [integrand, Kochin1] = GetIntegrand(-pi/2, -theta0, N1, 1);
    Rw1 = trapz(Kochin2.tvec(2:end-1), integrand(2:end-1));
end
if integ2 && U~=0
    [integrand, Kochin2] = GetIntegrand(theta0, pi/2, N2, 1);
    Rw2 = trapz(Kochin2.tvec(2:end-1), integrand(2:end-1));
end
if integ3 && U~=0
    [integrand, Kochin3] = GetIntegrand(pi/2, 3*pi/2, N3, 1);
    Rw3= trapz(Kochin3.tvec(2:end-1), integrand(2:end-1));
end
if integ4==1
    [integrand, Kochin4] = GetIntegrand(theta0, 2*pi-theta0, N4, -1);
    Rw4 = trapz(Kochin4.tvec(2:end-1), integrand(2:end-1));
end

Rw = rho/(8*pi) * (Rw1 + Rw2 - Rw3 + Rw4);

% ---

    function [integrand, Kochin] = GetIntegrand(tLow, tUp, N, pm)

        integrand  = zeros(N,1);
        StripHeave = zeros(nst,1);
        StripScatr = zeros(nst,1);
        HTotal     = zeros(N,1);
        HHeave     = zeros(N,1);
        HPitch     = zeros(N,1);
        HScatr     = zeros(N,1);
        tvec       = linspace(tLow, tUp, N);

        for p=1:N

            theta  = tvec(p);
            cosT   = cos(theta);
            sinT   = sin(theta);
            if U==0
                kappaj = K;
            else
                kappaj = kappa0*((1+pm*sqrt(1-4*tau*cosT))/(2*cosT))^2;
            end
            m = kappaj*cosT;
            for t=1:nst
                StripHeave(t) = KochinFunction_theta(kappaj, sinT, S(t).xv, S(t).phi(:,findex,2), S(t).nv(:,2));
                StripScatr(t) = KochinFunction_theta(kappaj, sinT, S(t).xv, S(t).ScatPhiMapped(:,findex), S(t).ScatBCMapped(:,findex));
            end

            for t=1:nst-1
                x0 = xst(t);
                x1 = xst(t+1);
                % Heave
                ah = (StripHeave(t)-StripHeave(t+1))/(x0-x1);
                bh = StripHeave(t+1)-ah*x1;
                % Pitch
                y1 = StripHeave(t+1)*(-x1+U/(1i*omegae));
                y0 = StripHeave(t)  *(-x0+U/(1i*omegae));
                ap = (y0-y1)/(x0-x1);
                bp = y1-ap*x1;
                % Scattering
                as = (StripScatr(t)-StripScatr(t+1))/(x0-x1);
                bs = StripScatr(t+1)-as*x1;

                HHeave(p) = HHeave(p) + 1/m^2*( (exp(1i*m*x1)*(ah-1i*ah*m*x1-1i*bh*m))-((exp(1i*m*x0)*(ah-1i*ah*m*x0-1i*bh*m))) );
                HPitch(p) = HPitch(p) + 1/m^2*( (exp(1i*m*x1)*(ap-1i*ap*m*x1-1i*bp*m))-((exp(1i*m*x0)*(ap-1i*ap*m*x0-1i*bp*m))) );
                HScatr(p) = HScatr(p) + 1/m^2*( (exp(1i*m*x1)*(as-1i*as*m*x1-1i*bs*m))-((exp(1i*m*x0)*(as-1i*as*m*x0-1i*bs*m))) );
            end

            HHeave(p) = HHeave(p) * 1i * omegae * rao(3);
            HPitch(p) = HPitch(p) * 1i * omegae * rao(5);
            HScatr(p) = HScatr(p) * 1/(1i*omegao);

            HTotal(p) = HHeave(p) + HPitch(p) + HScatr(p);

            integrand(p) = kappaj * (kappaj*cosT - K*cos(beta))/sqrt(1-4*tau*cosT) * (abs(HTotal(p)))^2;
        end

        Kochin.Heave = HHeave;
        Kochin.Pitch = HPitch;
        Kochin.Scatr = HScatr;
        Kochin.tvec  = tvec';
    end
end