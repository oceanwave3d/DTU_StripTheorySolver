
% This function calculates the added resistance according to the
% Gerritsma and Beukelman method and using the ship motion data based
% on STF theory.
%
% For the implementation details see:
%
% (J. Gerritsma and W. Beukelman) ANALYSIS OF THE MODIFIED STRIP THEORY FOR THE CALCULATION OF SHIP MOTIONS
% AND WAVE BENDING MOMENTS, International Shipbuilding Progress, vol. 19, no. 217, 1972
%
% Written by : Mostafa Amini-Afshar

function [Rw] = ARGerritsmaBeukelman(U, rao, omegao, omegae, S, findex, beta, GBCoeff)

if abs(beta-pi)>eps && U~=0
    warning('ARGerritsmaBeukelman.m : Gerritsma-Beukelman method is valid only for head seas.!');
end

g    = 1;
S    = S(vertcat(S.Nv)>0); % Remove the dummy stations
xst  = vertcat(S.xCoor);
K    = omegao^2/g;
nst  = length(xst);
Vza  = zeros(nst,1);
a33  = zeros(nst,1);
b33  = zeros(nst,1);

for t=1:nst
    
    % Calculate the dynamic correction to Froude-Kriloff hypothesis for
    % this section
    xb     = xst(t);
    Nv     = S(t).Nv;
    yb     = S(t).xv(1:Nv,1);
    zb     = S(t).xv(1:Nv,2);
    zb     = zb(end:-1:1);
    yb     = yb(end:-1:1);
    b      = S(t).beam;
    a33(t) =  real(squeeze(S(t).fjk(findex, 2, 2)));
    b33(t) = -imag(squeeze(S(t).fjk(findex, 2, 2))) * omegae;
    
    integrands = yb.*exp(-K*zb);
    temp       = trapz(-zb, integrands);
    dcf        = 1-K/(0.5*b)*temp;
    
    % Calculate the relative vertical velocity for this section
    
    Vza(t) = abs(1i*omegae*rao(3) - xb*1i*omegae*rao(5) + U*rao(5) - 1i*omegae*exp(1i*K*xb)*dcf);
end

% Calculate dm/dx for this frequency at the location of this station

[dmdx] = FirstDerivative(xst, a33, GBCoeff);

% Calculate the added resistance for this frequency

dR = (b33 - U * dmdx).*Vza.^2;
Rw = K/(2*omegae)*trapz(xst,dR);

end