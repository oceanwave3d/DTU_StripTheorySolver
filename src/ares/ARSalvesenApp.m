%
% This script calculates the added resistance using
% the method proposed by Salvesen(1974 & 1978) based on the
% ship motion results coming from STF strip theory.
% R = -i/2*k*cos(beta)*[zeta3(FI3+FD3)+zeta5(FI5+FD5)]+R7
% For further details see:
%
% N. Salvesen, “Second-order steady state forces and moments on surface ships in oblique regular
% waves,” in International Symposium on Dynamics of Marine Vehicles and Structures in Waves,
% Paper, vol. 22, 1974.
%
% N. Salvesen, “Added resistance of ships in waves,” Journal of Hydronautics, vol. 12, no. 1, pp. 24–34, 1978.
%
% Written by: Mostafa Amini-Afshar
%

function [Rw] = ARSalvesenApp(U, rao, omegao, omegae, S, findex, beta)

g    = 1;
rho  = 1;
S    = S(vertcat(S.Nv)>0); % Remove the dummy stations
xst  = vertcat(S.xCoor);
nst  = length(xst);
K    = omegao^2/g;

SFI_3 = zeros(nst,1);
SFI_5 = zeros(nst,1);
SFD_3 = zeros(nst,1);
SFD_5 = zeros(nst,1);
SFR7   = zeros(nst,1);

for t=1:nst % stations
    
    x   = xst(t);
    d   = S(t).draft;
    b   = S(t).beam;
    s   = S(t).area;
    a33 =  real(squeeze(S(t).fjk(findex, 2, 2)));
    a22 =  real(squeeze(S(t).fjk(findex, 1, 1)));
    b33 = -imag(squeeze(S(t).fjk(findex, 2, 2)))*omegae;
    b22 = -imag(squeeze(S(t).fjk(findex, 1, 1)))*omegae;
    
    if abs(beta-pi)<eps
        sing = b/2;
    else
        sing = sin(K*0.5*b*sin(beta))/(K*sin(beta));
    end
   
    SFI_3(t) =  2*rho*g*    exp(1i*K*x*cos(beta)) * exp(-K*d*s)*sing;
    SFI_5(t) = -2*rho*g*x*  exp(1i*K*x*cos(beta)) * exp(-K*d*s)*sing;
    SFD_3(t) = -2*omegao/b* exp(1i*K*x*cos(beta)) * exp(-K*d*s)*sing*(omegae*a33-1i*b33);
    
    SFD_5(t) = -(x+1i*U/omegae)*SFD_3(t);
    SFR7(t)   = -1/2*omegao^2/omegae*K*cos(beta) * exp(-2*K*d*s)*(b33 + b22*sin(beta)^2);
end

% Integrate along the vessel

FI3 = trapz(xst,SFI_3);
FI5 = trapz(xst,SFI_5);
FD3 = trapz(xst,SFD_3);
FD5 = trapz(xst,SFD_5);
R7  = trapz(xst,SFR7);

% Added resistance for this frequency

Rw = real(-1i/2*K*cos(beta)*(rao(3)*(FI3+FD3) + rao(5)*(FI5+FD5))) + R7;

end
