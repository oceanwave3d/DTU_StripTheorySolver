function [ddxm] = FirstDerivative(x, f, GBCoeff)

a = GBCoeff.a;
b = GBCoeff.b;
c = GBCoeff.c;
d = GBCoeff.d;
m = 2;
for i=1:numel(x)
    ddxm(i,:)=(1/d)^(m-1)*c(m,:,i)*f(i-a(i):i+b(i),:);
end

end