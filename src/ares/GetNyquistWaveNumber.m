function [ kNyq ] = GetNyquistWaveNumber(x)

kNyq = pi/mean(diff(x));

end

