%
% This function implements Maruo's method for calculation of mean second-order wave
% drift force (added resistance) based on the "strip-therory approximations" of the
% Kochin Functions. For further details on Maruo's method see:
% H. Maruo, “Wave resistance of a ship in regular head seas,” Bulletin of the Faculty
% of Engineering, Yokohama National University, vol. vol. 9, 1960.
%
% Written by: Mostafa Amini-Afshar
%

function [Rw, Kochin1, Kochin2, Kochin3] = ARMaruoApp(U, rao, omegao, omegae, S, findex, beta, MaruoCoeff)

C      = MaruoCoeff.C;
Cleft  = MaruoCoeff.Cleft;
Cright = MaruoCoeff.Cright;

S    = S(vertcat(S.Nv)>0); % Remove the dummy stations
xst  = vertcat(S.xCoor);
g    = 1;
rho  = 1;
K    = omegao^2/g;
N1   = 200;
N2   = 70;
N3   = 200;
nst  = length(xst);
Rw1  =  0;
Rw2  =  0;
Rw3  =  0;

finteg = 1; % 1 If The 1th Integral Should Be Included
sinteg = 1; % 1 If The 2nd Integral Should Be Included
tinteg = 1; % 1 If The 3rd Integral Should Be Included

Kochin1 = struct('Heave',{},'Pitch',{},'Scatr',{},'mvec', {});
Kochin2 = struct('Heave',{},'Pitch',{},'Scatr',{},'mvec', {});
Kochin3 = struct('Heave',{},'Pitch',{},'Scatr',{},'mvec', {});

formulationType = 'Loukakis';

if U~=0 % Added resistance (Forward-Speed Cases)
    kNyq      = 2*GetNyquistWaveNumber(xst);
    tau       = U * omegae/g;
    kappaZero = g/U^2;
    kBar1     = -kappaZero/2 * (1 + 2 * tau + sqrt(1 + 4 * tau));
    kBar2     = -kappaZero/2 * (1 + 2 * tau - sqrt(1 + 4 * tau));
    
    if tau < 0.25
        kBar3 =  kappaZero/2 * (1 - 2 * tau - sqrt(1 - 4 * tau));
        kBar4 =  kappaZero/2 * (1 - 2 * tau + sqrt(1 - 4 * tau));
    else
        kBar3 = kappaZero * tau;
        kBar4 = kappaZero * tau;
    end
else % Wave Drift Froce (Zero-Speed Cases)
    kBar1 =  0;
    kBar2 = -K;
    kBar3 =  K;
    kBar4 =  0;
    kNyq  =  0;
end

if kBar1 >-kNyq && finteg && U~=0
    DL = (kBar1+kNyq)/(N1-1);
    [integrand, Kochin1] = GetIntegrand(-kNyq, kBar1, N1);
    Rw1 = TaylorIntegration(N1, integrand, DL, C, Cleft, Cright);
end

if kBar3 > kBar2 && sinteg
    DL = (kBar3-kBar2)/(N2-1);
    [integrand, Kochin2] = GetIntegrand(kBar2, kBar3, N2);
    Rw2 = TaylorIntegration(N2, integrand, DL, C, Cleft, Cright);
end

if kNyq > kBar4 && tinteg && U~=0
    DL = (kNyq-kBar4)/(N3-1);
    [integrand, Kochin3] = GetIntegrand(kBar4, kNyq, N3);
    Rw3 = TaylorIntegration(N3, integrand, DL, C, Cleft, Cright);
end

Rw = rho/(4*pi) * (-Rw1 + Rw2 + Rw3);

% ---

    function [integrand, Kochin] = GetIntegrand(kLow, kUp, N)
        
        if kUp<kLow , error('Lower limit is greater than upper limit.'); end
        
        H              = zeros(N,1);
        integrand      = zeros(N,1);
        HHeave         = zeros(N,1);
        HPitch         = zeros(N,1);
        HScatr         = zeros(N,1);
        heave_PhiB_ddn = zeros(nst,1);
        heave_ddn_PhiB = zeros(nst,1);
        pitch_PhiB_ddn = zeros(nst,1);
        pitch_ddn_PhiB = zeros(nst,1);
        scatr_PhiS_ddn = zeros(nst,1);
        scatr_ddn_PhiS = zeros(nst,1);
        HStrip         = zeros(nst,1);
        StripHeaveH    = zeros(nst,1);
        StripPitchH    = zeros(nst,1);
        StripScatrH    = zeros(nst,1);
        
        mvec = linspace(kLow, kUp, N);
        
        for J = 1:N % LOOP OVER m
            
            m = mvec(J);
            
            kappaBar = (omegae+m*U)^2/g;
            
            for t = 1:nst % Loop over stations to get Kochin function HSTF
                
                a33 =  real(squeeze(S(t).fjk(findex, 2, 2)));
                a22 =  real(squeeze(S(t).fjk(findex, 1, 1)));
                b33 = -imag(squeeze(S(t).fjk(findex, 2, 2)))*omegae;
                b22 = -imag(squeeze(S(t).fjk(findex, 1, 1)))*omegae;
                s   = S(t).area;
                b   = S(t).beam;
                d   = S(t).draft;
                x   = xst(t);
                
                switch formulationType
                    
                    case 'Loukakis'
                        
                        % --------------------------------------------------
                        % HEAVE CONTRIBUTIONS [ PHIB DDN - DDN PHIB ] (...)
                        %  --------------------------------------------------
                        
                        heave_PhiB_ddn(t) = ...
                            2*1i*rao(3)*kappaBar/(rho*sqrt(kappaBar^2 - m^2)) * ...
                            1/b * exp( -kappaBar*s*d) * exp(1i*m*x) * ...
                            (omegae*a33 - 1i*b33) * sin(sqrt(kappaBar^2 - m^2)*b/2);
                        
                        heave_ddn_PhiB(t) = ...
                            2*1i*omegae*rao(3)/sqrt(kappaBar^2-m^2) * ...
                            exp(-kappaBar*s*d)*exp(1i*x*m) * sin(sqrt(kappaBar^2 - m^2)*b/2);
                        
                        % --------------------------------------------------
                        % PITCH CONTRIBUTIONS [ PHIB DDN - DDN PHIB ] (...)
                        % --------------------------------------------------
                        
                        pitch_PhiB_ddn(t) = ...
                            2*1i*rao(5)*kappaBar/(rho*sqrt(kappaBar^2 - m^2)) * ...
                            1/b*(-x + U/(1i * omegae)) * exp(-kappaBar*s*d) * exp(1i*x*m) * ...
                            (omegae*a33 - 1i*b33) * sin(sqrt(kappaBar^2 - m^2)*b/2);
                        
                        pitch_ddn_PhiB(t) = ...
                            2*rao(5)/(sqrt(kappaBar^2 - m^2)) * ...
                            (U - 1i*omegae*x) * exp(-kappaBar*s*d) * exp(1i*x*m) * sin(sqrt(kappaBar^2 - m^2)*b/2);
                        
                        % -------------------------------------------------------
                        % SCATTERING CONTRIBUTIONS [ PHIS DDN - DDN PHIS ] (...)
                        % -------------------------------------------------------
                        
                        khaty = sqrt(kappaBar^2 - m^2) - K * sin(beta);
                        khatx = m                      - K * cos(beta);
                        
                        if khaty == 0
                            P    = b;
                            ksin = b/2;
                        else
                            P    = 2/khaty * sin(khaty*b/2);
                            ksin = 1/khaty * sin(khaty*b/2);
                        end
                        
                        scatr_PhiS_ddn(t) = -2*1i*omegao*kappaBar/(rho*omegae) * ...
                            1/b*(omegae*a33 - 1i*b33) * exp(-(K + kappaBar)*s*d) * exp(1i*khatx*x) * ksin ...
                            -1i*kappaBar*sin(beta)*sqrt(kappaBar^2 - m^2)/(rho*omegae) * ...
                            1/d*(omegae*a22 - 1i*b22) * exp(-(K + kappaBar)*s*d) * exp(1i*khatx*x) * ksin;
                        
                        scatr_ddn_PhiS(t) = -1i*omegao* ...
                            exp(1i*khatx *x) * exp(-(K + kappaBar)*s*d) * ...
                            (P - khaty*s*b*d*sin(beta) * cos(s*khaty*b/2));
                        
                    case 'STF'
                        
                        % --------------------------------------------------
                        % HEAVE CONTRIBUTIONS [ PHIB DDN - DDN PHIB ] (...)
                        %  --------------------------------------------------
                        
                        heave_PhiB_ddn(t) = ...
                            1i*kappaBar*rao(3)/rho * exp(-kappaBar*s*d) * exp(1i*m*x) * ...
                            (omegae*a33 - 1i*b33) * cos(s*b/2*sqrt(kappaBar^2 - m^2));
                        
                        heave_ddn_PhiB(t)= ...
                            1i*omegae*rao(3)*b*exp(-kappaBar*s*d) * exp(1i*x*m) * cos(s*b/2*sqrt(kappaBar^2 - m^2));
                        
                        % --------------------------------------------------
                        % PITCH CONTRIBUTIONS [ PHIB DDN - DDN PHIB ] (...)
                        % --------------------------------------------------
                        
                        pitch_PhiB_ddn(t) = ...
                            1i*kappaBar*rao(5)/rho * ...
                            (-x + U/(1i*omegae)) * (omegae*a33 -1i*b33) * exp(-kappaBar*s*d) * exp(1i*x*m) * ...
                            cos(s*b/2*sqrt(kappaBar^2 - m^2));
                        
                        
                        pitch_ddn_PhiB(t) = rao(5) * (U - 1i*omegae*x) * exp(-kappaBar*s*d) * exp(1i*x*m) * ...
                            cos(s*b/2*sqrt(kappaBar^2 - m^2)) * b;
                        
                        % -------------------------------------------------------
                        % SCATTERING CONTRIBUTIONS [ PHIS DDN - DDN PHIS ] (...)
                        % -------------------------------------------------------
                        
                        khaty = sqrt( kappaBar^2 - m^2) - K * sin(beta);
                        khatx = m                       - K * cos(beta);
                        
                        scatr_PhiS_ddn(t) = -1i*kappaBar*omegao/(rho*omegae) * ...
                            (omegae*a33 -1i*b33) * exp(-(K + kappaBar)*s*d) * exp(1i*khatx * x) * cos(s*(b/2*khaty)) - ...
                            1i*omegao*sin(beta) * sqrt(kappaBar^2 - m^2)/(rho * omegae) * (omegae*a22 - 1i*b22) * ...
                            exp(-(K + kappaBar)*s*d) * exp(1i*khatx * x) * cos(s*(b/2*khaty));
                        
                        scatr_ddn_PhiS(t) = -1i*omegao * ...
                            exp(1i*khatx * x) * exp(-(K + kappaBar)*s*d) * ...
                            (b*cos(s*khaty*b/2) - khaty*s*b*d* sin(beta) * cos(s*khaty*b/2));
                        
                    otherwise
                        error('Unexpected type for the formulation!.')
                        
                end
                
                % ------------------------------------------------------
                % THE TOTAL CONTRIBUTIONS AT THIS ANGLE FOR THIS SECTION
                % ------------------------------------------------------
                
                HStrip(t) = ...
                    heave_PhiB_ddn (t) - heave_ddn_PhiB (t) + ...
                    pitch_PhiB_ddn (t) - pitch_ddn_PhiB (t) + ...
                    scatr_PhiS_ddn (t) - scatr_ddn_PhiS (t);
                
                StripHeaveH(t) = heave_PhiB_ddn (t) - heave_ddn_PhiB (t);
                StripPitchH(t) = pitch_PhiB_ddn (t) - pitch_ddn_PhiB (t);
                StripScatrH(t) = scatr_PhiS_ddn (t) - scatr_ddn_PhiS (t);
                
            end % END STATION   LOOP
            
            % --------------------------------------------------------
            % THE TOTAL CONTRIBUTIONS AT THIS ANGLE FROM ALL SECTIONS
            % --------------------------------------------------------
            
            HHeave(J)    = trapz(xst, StripHeaveH);
            HPitch(J)    = trapz(xst, StripPitchH);
            HScatr(J)    = trapz(xst, StripScatrH);            
            H(J)         = trapz(xst, HStrip);
            integrand(J) = kappaBar * (m - K*cos(beta))/sqrt(kappaBar^2 - m^2) * (abs(H(J)))^2;
        end
        
        Kochin.Heave = HHeave;
        Kochin.Pitch = HPitch;
        Kochin.Scatr = HScatr;
        Kochin.mvec  = mvec';
    end
end
