%
% This function implements Salvesen's method for calculation of mean second-order wave
% drift force (added resistance) using the Kochin Functions. Threfore the implementation
% is based on the direct calcualtion of the potentials, and no long-wave approximation
% is used. In addition the weak-scatter part of his equation is included by this
% implementation.
%
% For the implementation based on Slavesen's long-wave approximation, i.e the original
% method presented in his papers, use ARSalvesenApp.m script.
%
% Written by: Mostafa Amini-Afshar
%

function [Rw] = ARSalvesen(U, rao, omegao, omegae, S, findex, beta, KochinType)

xst   = vertcat(S.xCoor);
g     = 1;
rho   = 1;
A     = 1;
K     = omegao^2/g;
nst   = length(xst);
cosB  = cos(beta);
sinB  = sin(beta);
pm = 1; if sinB<0, pm = -1; end
m = K*cosB;
fx3 = 0;
fx5 = 0;
fxs = 0;
WS   = 1; % 1 if the weak-scatter should be removed
RwWS = 0; % Related to the weak-scatter part

StripHeave = zeros(nst,1);
StripScatr = zeros(nst,1);
WSStrip    = zeros(nst,1); % Related to the weak-scatter part

CatFlag=S(2).CatFlag;

for t=1:nst
    x  = xst(t);
    if S(t).Nv == 0
        StripHeave(t) = 0;
        StripScatr(t) = 0;
        WSStrip   (t) = 0;
    else
        y  = S(t).xc(:,1);
        z  = S(t).xc(:,2);
        n2 = S(t).nv(:,1);
        n3 = S(t).nv(:,2);
        switch KochinType
            case '2D'
                switch CatFlag
                    case 'yes'
                        Nv=size(S(t).xv,1);
                        Np=Nv-2;
                        [f1,~] = KochinFunction_m(K, m, S(t).xv(1:Np/2+1,:), ...
                            S(t).HeavePhiMapped(1:Np/2,findex), S(t).nv(1:Np/2,2),pm);
                        [f2,~] = KochinFunction_m(K, m, S(t).xv(Np/2+2:Np+2,:)...
                            , S(t).HeavePhiMapped(Np/2+1:Np,findex), S(t).nv(Np/2+1:Np,2),pm);
                        StripHeave(t)=f1+f2;
                        [f1,~] = KochinFunction_m(K, m, S(t).xv(1:Np/2+1,:),...
                            S(t).ScatPhiMapped(1:Np/2,findex), S(t).ScatBCMapped(1:Np/2,findex),pm);
                        [f2,~] = KochinFunction_m(K, m, S(t).xv(Np/2+2:Np+2,:),...
                            S(t).ScatPhiMapped(Np/2+1:Np,findex), S(t).ScatBCMapped(Np/2+1:Np,findex),pm);
                        StripScatr(t)=f1+f2;
                    otherwise
                        [StripHeave(t),~] = KochinFunction_m(K, m, S(t).xv,...
                            S(t).HeavePhiMapped(:,findex), S(t).nv(:,2),pm);
                        [StripScatr(t),~] = KochinFunction_m(K, m, S(t).xv,...
                            S(t).ScatPhiMapped(:,findex), S(t).ScatBCMapped(:,findex),pm);

                end
                % ------------------------
                % The Weak-Scatterer Part
                % ------------------------
                Phi0    = g*A/(1i*omegao)*exp(-K*z).*exp(-1i*K*(x*cosB+y*sinB));
                Phi3    = 1i*omegae*S(t).HeavePhiMapped(:,findex);
                dPhi3Dx = 1i*omegae*S(t).dphi_heave_dx(:,findex);
                dPhi3Dy = 1i*omegae*S(t).dphi_heave_dy(:,findex);
                dPhi3Dz = 1i*omegae*S(t).dphi_heave_dz(:,findex);
                Phis    = 1/(1i*omegao)*S(t).ScatPhiMapped(:,findex);
                dPhisDx = 1/(1i*omegao)*S(t).dphi_scatr_dx(:,findex);
                dPhisDy = 1/(1i*omegao)*S(t).dphi_scatr_dy(:,findex);
                dPhisDz = 1/(1i*omegao)*S(t).dphi_scatr_dz(:,findex);
                %
                W = Phi3    * (rao(3)+(U/(1i*omegae)-x)*rao(5))             + Phis;
                Q = dPhi3Dx * (rao(3)+(U/(1i*omegae)-x)*rao(5))-rao(5)*Phi3 + dPhisDx;
                N = 1i*omegae*rao(3)*n3 + rao(5)*(U*n3-1i*omegae*x*n3) + K*Phi0.*(n3+1i*n2*sinB);
                M = S(t).dn2dx*(1i*omegae*rao(3)+U*rao(5)-1i*omegae*x*rao(5))- ...
                    1i*omegae*rao(5)*n3 -1i*K^2*cosB*Phi0.*(n3+1i*n2*sinB)+...
                    K*Phi0.*(S(t).dn2dx + 1i*S(t).dn1dx*sinB);
                Wy = dPhi3Dy * (rao(3)+(U/(1i*omegae)-x)*rao(5)) + dPhisDy;
                Wz = dPhi3Dz * (rao(3)+(U/(1i*omegae)-x)*rao(5)) + dPhisDz;
                R = Wy.*S(t).dn1dx + Wz.*S(t).dn2dx;
                %
                WSStrip(t) = sum((W.*conj(M-R) - N.*conj(Q)).*S(t).ds);
            case '3D'
                StripHeave(t) = 0;
                StripScatr(t) = 0;
        end
    end
end

StripHeave = -StripHeave; % Due to sign change in KochinFunction_m
StripScatr = -StripScatr; % Due to sign change in KochinFunction_m

for t=1:nst-1
    x0 = xst(t);
    x1 = xst(t+1);
    % --------------------------
    % The original Salvesen part
    % --------------------------
    % Heave
    ah = (StripHeave(t)-StripHeave(t+1))/(x0-x1);
    bh = StripHeave(t+1)-ah*x1;
    % Pitch
    y1 = StripHeave(t+1)*(-x1+U/(1i*omegae));
    y0 = StripHeave(t)  *(-x0+U/(1i*omegae));
    ap = (y0-y1)/(x0-x1);
    bp = y1-ap*x1;
    % Scattering
    as = (StripScatr(t)-StripScatr(t+1))/(x0-x1);
    bs = StripScatr(t+1)-as*x1;
    %
    fx3 = fx3 + 1/m^2*( (exp(1i*m*x1)*(ah-1i*ah*m*x1-1i*bh*m))-((exp(1i*m*x0)*(ah-1i*ah*m*x0-1i*bh*m))) );
    fx5 = fx5 + 1/m^2*( (exp(1i*m*x1)*(ap-1i*ap*m*x1-1i*bp*m))-((exp(1i*m*x0)*(ap-1i*ap*m*x0-1i*bp*m))) );
    fxs = fxs + 1/m^2*( (exp(1i*m*x1)*(as-1i*as*m*x1-1i*bs*m))-((exp(1i*m*x0)*(as-1i*as*m*x0-1i*bs*m))) );
end

fx3 = fx3*1i*omegae*rao(3);
fx5 = fx5*1i*omegae*rao(5);
fxs = fxs*1/(1i*omegao);

if WS==1
    % ----------------------
    % The Weak-Scatter Part
    % ----------------------
    RwWS = -1/4*rho*real(trapz(xst,WSStrip));
end
Rw = rho*g*K*cosB/(2*omegao)*real(fx3 + fx5 + fxs) + RwWS;
end
