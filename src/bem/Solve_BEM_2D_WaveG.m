function [fjk,E,Hp,Hm,phi, ScatBC]=Solve_BEM_2D_WaveG(K,xst,xv,xc,dphidn,ds,...
    xsym,irr,x0FS,x1FS,beta,CatFlag)
%
% A function to solve the radiation and diffraction problems on a 2D panel
% geometry defined by the vertices xv(1:N+1,1:2) using a constant strength
% panel method. The radiation problems to solve are defined by the body
% boundary conditions input in dphidn(1:N,1:nmodes). K(1:nf)=kL are the
% non-dimensional, infinite-depth wave numbers to be solved at.
%
% xsym==0 or 1 defines whether or not y=0 plane symmetry is
% exploited, but this is only partially implemented, so not recommended.
%
% If irr==1, then irregular frequency removal is implemented by adding
% additional points on the interior free surface defined to lie within the
% two points x0FS and x1FS.
%
% The non-dimensional radiation solution is returned in
% fjk(1:nf,1:nmodes,1:nmodes), where fjk=ajk/(rho L^m) - i bjk/(rho omega L^m)
% with L the non-dimensional length-scale of the input geometry and m=2 for
% linear-linear modes (heave,sway), m=3 for linear-rotational modes (i.e.
% sway-roll) and m=4 for roll-roll. Omega here is the dimensional radian
% frequency.
% E(1:nf,1:nmodes) are the non-dimensional 2D exciting forces normalized by
% rho g L^n, n=2 for sway, heave and n=3 for roll.
% Hp(1:nf,1:nmodes) and Hm(1:nf,1:nmodes) are the non-dimensional Kochin
% functions H^+ and H^-.
%
% The solutions phi(1:Nv,1:nf,1:nmodes+2) are also returned holding the
% radiation potentials for the last index=1:nmodes, while index=nmodes+1
% holds the 2D diffraction potential and nmodes+2 holds the 3D scattering
% potential in head-seas at location x=xst and heading angle beta.
%
% This implementation follows the lecture note: "Wave-Body Interaction
% Theory (Theory of Ship Waves)" by M. Kashiwagi (2019), Ch. 6.
%
% Written by H. B. Bingham, Nov. 2019
%
% This version: March 17, 2020.
%
%%
%
% The number of frequencies to solve for and the number of panels on the
% body.
%
nf=length(K); nmodes=size(dphidn,2);
switch CatFlag
    case 'yes'
        %
        % For a catamaran, there is no panel between the hulls. This
        % assumes that symmetry is not being exploited!
        %
        N=length(xv)-2;
    otherwise
        N=length(xv)-1;
end
%
% The incident wave heading angle sines and cosines
%
cosB=cos(beta); sinB=sin(beta);
%
% Initialize the solution arrays
%
phi=zeros(N,nf,nmodes+2); Hp=zeros(nf,nmodes+1); Hm=Hp; E=zeros(nf,nmodes);
ScatBC = zeros(N, nf);
%
% The waterline beam of this section
%
Beam=x1FS-x0FS;
%
% Compute the Rankine term matrices
%
switch CatFlag
    case 'yes'
        [Ln1,Tn1,Lnp1,Tnp1]=Integral_LogR_LineSegment_Kashiwagi(xc,xv(1:N/2+1,:),0);
        [Ln2,Tn2,Lnp2,Tnp2]=Integral_LogR_LineSegment_Kashiwagi(xc,xv(N/2+2:N+2,:),0);
        Ln=[Ln1 Ln2]; Tn=[Tn1 Tn2]; Lnp=[Lnp1 Lnp2]; Tnp=[Tnp1 Tnp2];
    otherwise
        [Ln,Tn,Lnp,Tnp]=Integral_LogR_LineSegment_Kashiwagi(xc,xv,0);
end
G0=Ln-Lnp; Gn0=Tn-Tnp;
switch xsym
    case 1
        %
        % To exploit symmetry, compute the coefficients for the missing
        % panels by following them back from the centerline to the bow
        % (i.e. keeping the direction the same), then fliplr to put the
        % influences where they belong in the matrices.
        %
        [Lnr,Tnr,Lnrp,Tnrp]=Integral_LogR_LineSegment_Kashiwagi(xc,...
            [-xv(N+1:-1:1,1),xv(N+1:-1:1,2)],1);
        G0r=fliplr(Lnr)-fliplr(Lnrp); Gn0r=fliplr(Tnr)-fliplr(Tnrp);
        G0sym=G0+G0r; Gn0sym=Gn0+Gn0r;
        G0asym=G0-G0r; Gn0asym=Gn0-Gn0r;
end
%
% Loop over frequencies and compute the solution.
%
for iom=1:nf
    %
    % Build the wave Green function influence coefficient matrices for this
    % value of K and solve for phi using a mixed source/dipole
    % formulation.
    %
    switch CatFlag
        case 'yes'
            [G1,Gn1]=Integral_G_Wave_LineSegment_Kashiwagi(K(iom),xc,xv(1:N/2+1,:));
            [G2,Gn2]=Integral_G_Wave_LineSegment_Kashiwagi(K(iom),xc,xv(N/2+2:N+2,:));
            G=[G1 G2]; Gn=[Gn1 Gn2];
        otherwise
            [G,Gn]=Integral_G_Wave_LineSegment_Kashiwagi(K(iom),xc,xv);
    end
    switch xsym
        case 0
            A=Gn0+Gn; B=G0+G;
        case 1
            [Gr,Gnr]=Integral_G_Wave_LineSegment_Kashiwagi(K(iom),xc,...
                [-xv(N+1:-1:1,1),xv(N+1:-1:1,2)]);
            Gnsym=Gn+fliplr(Gnr); Gsym=G+fliplr(Gr);
            Gnasym=Gn-fliplr(Gnr); Gasym=G-fliplr(Gr);
            Asym=Gn0sym+Gnsym; Bsym=G0sym+Gsym;
            Aasym=Gn0asym+Gnasym; Basym=G0asym+Gasym;
    end
    %
    % Solve for phi.
    %
    switch irr
        case 0
            %
            % No irregular frequency removal.
            %
            for i=1:nmodes
                switch xsym
                    case 0
                        rhs=B*dphidn(:,i);
                        phi(:,iom,i)=A\rhs;
                    case 1
                        switch i
                            case {1,3}
                                rhs=Basym*dphidn(:,i);
                                phi(:,iom,i)=Aasym\rhs;
                            case 2
                                rhs=Bsym*dphidn(:,i);
                                phi(:,iom,i)=Asym\rhs;
                        end
                end
            end
            %
            % Solve the 2D diffraction problem
            %
            rhs=2*pi*exp(K(iom,1)*(1i*xc(:,1)-xc(:,2)));
            switch xsym
                case 0
                    phi(:,iom,nmodes+1)=A\rhs;
                case 1
                    phi(:,iom,nmodes+1)=Asym\rhs;
            end
            %
            % Solve the head-seas scattering problem at angle beta
            %
            rhs=B*(K(iom,1)*(dphidn(:,2)+1i*sinB*dphidn(:,1))...
                .*exp(K(iom,1)*(-1i*(xst*cosB+xc(:,1)*sinB)-xc(:,2))));
            phi(:,iom,nmodes+2)=A\rhs;
        case 1
            %
            % Add in M extra equations on the interior free-surface to
            % remove the irregular frequencies.
            %
            M=max(round(N/10),3);
            dx=Beam/(M+1); xcI(1:M,1)=x0FS+[1:M]*dx; xcI(1:M,2)=0;
            switch CatFlag
                case 'yes'
                    % Reflect the points for the second hull.
                    xcI(M+1:2*M,:)=-xcI(1:M,:); M=2*M;
                    [Ln1,Tn1,Lnp1,Tnp1]=Integral_LogR_LineSegment_Kashiwagi(xcI,xv(1:N/2+1,:),1);
                    [Ln2,Tn2,Lnp2,Tnp2]=Integral_LogR_LineSegment_Kashiwagi(xcI,xv(N/2+2:N+2,:),1);
                    Ln=[Ln1 Ln2]; Tn=[Tn1 Tn2]; Lnp=[Lnp1 Lnp2]; Tnp=[Tnp1 Tnp2];
                    [G1,Gn1]=Integral_G_Wave_LineSegment_Kashiwagi(K(iom),xcI,xv(1:N/2+1,:));
                    [G2,Gn2]=Integral_G_Wave_LineSegment_Kashiwagi(K(iom),xcI,xv(N/2+2:N+2,:));
                    G=[G1 G2]; Gn=[Gn1 Gn2];
                otherwise
                    [Ln,Tn,Lnp,Tnp]=Integral_LogR_LineSegment_Kashiwagi(xcI,xv,1);
                    [G,Gn]=Integral_G_Wave_LineSegment_Kashiwagi(K(iom),xcI,xv);
            end
            A=[A;Tn-Tnp+Gn]; B=[B;Ln-Lnp+G];
            %
            % Solve by least-squares
            %
            for i=1:nmodes
                rhs=B*dphidn(:,i);
                phi(:,iom,i)=(A.'*A)\(A.'*rhs);
            end
            %
            % Solve the 2D diffraction problem
            %
            rhs=2*pi*exp(K(iom,1)*(1i*xc(:,1)-xc(:,2)));
            rhs(N+1:N+M,1)=2*pi*exp(K(iom,1)*(1i*xcI(:,1)-xcI(:,2)));
            phi(:,iom,nmodes+1)=(A.'*A)\(A.'*rhs);
            %
            % Solve the head-seas scattering problem at angle beta
            %
            rhs=B*(K(iom,1)*(dphidn(:,2)+1i*sinB*dphidn(:,1))...
                .*exp(K(iom,1)*(-1i*(xst*cosB+xc(:,1)*sinB)-xc(:,2))));
            phi(:,iom,nmodes+2)=(A.'*A)\(A.'*rhs);

    end
    %
    % Compute the added mass and damping
    %
    for i=1:nmodes
        for j=1:nmodes
            fjk(iom,i,j)=-sum(phi(:,iom,j).*dphidn(:,i).*ds);
            switch xsym
                case 1
                    fjk(iom,i,j)=2*fjk(iom,i,j);
            end
        end
        %
        % Compute the 2D exciting forces for each free mode.
        %
        E(iom,i)=sum(phi(:,iom,nmodes+1).*dphidn(:,i).*ds);
        switch xsym
            case 1
                E(iom,i)=2*E(iom,i);
        end
        %
    end
    %
    % Compute the 2D Kochin functions
    %
    % Radiation modes
    %
    for i=1:nmodes
        switch CatFlag
            case 'yes'
                [Hp1,Hm1]=KochinFunction_m(K(iom),0,xv(1:N/2+1,:),...
                    phi(1:N/2,iom,i),dphidn(1:N/2,i),1);
                [Hp2,Hm2]=KochinFunction_m(K(iom),0,xv(N/2+2:N+2,:),...
                    phi(N/2+1:N,iom,i),dphidn(N/2+1:N,i),1);
                Hp(iom,i)=Hp1+Hp2; Hm(iom,i)=Hm1+Hm2;
            otherwise
                [Hp(iom,i),Hm(iom,i)]=KochinFunction_m(K(iom),0,xv,...
                    phi(:,iom,i),dphidn(:,i),1);
        end
    end
    %
    % Diffraction mode
    %
    switch CatFlag
        case 'yes'
            [Hp1,Hm1]=KochinFunction_m(K(iom),...
                0,xv(1:N/2+1,:),phi(1:N/2,iom,nmodes+1),0*dphidn(1:N/2,i),1);
            [Hp2,Hm2]=KochinFunction_m(K(iom),...
                0,xv(N/2+2:N+2,:),phi(N/2+1:N,iom,nmodes+1),0*dphidn(N/2+1:N,i),1);
            Hp(iom,nmodes+1)=Hp1+Hp2; Hm(iom,nmodes+1)=Hm1+Hm2;
        otherwise
            [Hp(iom,nmodes+1),Hm(iom,nmodes+1)]=KochinFunction_m(K(iom),...
                0,xv,phi(:,iom,nmodes+1),0*dphidn(:,i),1);
    end

    ScatBC(:, iom) = K(iom) * (dphidn(:,2)+1i*sinB*dphidn(:,1)).*exp(K(iom,1)*(-1i*(xst*cosB+xc(:,1)*sinB)-xc(:,2)));
end
end
