function [W] = ComputeKernel(x, params)

tau = params.tau;
Fr = params.Fr;
k1 = params.k1;
k2 = params.k2;
k3 = params.k3;
k4 = params.k4;

if(x==0)
    error("The Kernel is not defined at x=0!");
end

msgid ='MATLAB:integral:MaxIntervalCountReached';
warning('off', msgid)
reltol = 1e-3;
abstol = 1e-3;

if tau<1/4

    if x <0
        fun = @(m, x, tau, Fr) exp(-1i*m*x).*(1./sqrt(1-m.^2./((tau/Fr)^2+2*m*tau + m.^2*Fr^2).^2)-1);
        W11 = 1i*integral(@(m) fun(m,x, tau, Fr),-Inf,k1, 'AbsTol', abstol,'RelTol', reltol) - (exp(-1i*k1*x)-1)/x;
        fun = @(m, x, tau, Fr) exp(-1i*m*x).*(1./sqrt(1-m.^2./((tau/Fr)^2+2*m*tau + m.^2*Fr^2).^2));
        W12 = -1i*integral(@(m) fun(m,x, tau, Fr),k2,0, 'AbsTol', abstol,'RelTol', reltol);
    end

    fun = @(m, x, tau, Fr) exp(-1i*m*x).*(1./sqrt(1-m.^2./((tau/Fr)^2+2*m*tau + m.^2*Fr^2).^2));
    W23 = -1i/2*integral(@(m) fun(m,x, tau, Fr),0,k3, 'AbsTol', abstol,'RelTol', reltol);
    fun = @(m, x, tau, Fr) exp(-1i*m*x).*(1./sqrt(m.^2./((tau/Fr)^2+2*m*tau + m.^2*Fr^2).^2-1));
    W234 = 1/2*integral(@(m) fun(m,x, tau, Fr),k3,k4, 'AbsTol', abstol,'RelTol', reltol);
    fun = @(m, x, tau, Fr) exp(-1i*m*x).*(1./sqrt(1-m.^2./((tau/Fr)^2+2*m*tau + m.^2*Fr^2).^2)-1);
    W24 = -1i/2*integral(@(m) fun(m,x, tau, Fr),k4,Inf, 'AbsTol', abstol,'RelTol', reltol) - (exp(-1i*k4*x)-1)/(2*x);

    if x<0
        W = W11+W12+W24+W234+W23;
    else
        W = W24+W234+W23;
    end

elseif tau>1/4

    if x <0
        fun = @(m, x, tau, Fr) exp(-1i*m*x).*(1./sqrt(1-m.^2./((tau/Fr)^2+2*m*tau + m.^2*Fr^2).^2)-1);
        W11 = 1i*integral(@(m) fun(m,x, tau, Fr),-Inf,k1, 'AbsTol', abstol,'RelTol', reltol) - (exp(-1i*k1*x)-1)/x;
        fun = @(m, x, tau, Fr) exp(-1i*m*x).*(1./sqrt(1-m.^2./((tau/Fr)^2+2*m*tau + m.^2*Fr^2).^2));
        W12 = -1i*integral(@(m) fun(m,x, tau, Fr),k2,0, 'AbsTol', abstol,'RelTol', reltol);
    end

    fun = @(m, x, tau, Fr) exp(-1i*m*x).*(1./sqrt(1-m.^2./((tau/Fr)^2+2*m*tau + m.^2*Fr^2).^2)-1);
    W2 = -1i/2*integral(@(m) fun(m,x, tau, Fr),0,Inf, 'AbsTol', abstol,'RelTol', reltol);

    if x<0
        W = W11+W12+W2;
    else
        W = W2;
    end
else
    error("The Kernel is not defined for tau=1/4!");
end

end