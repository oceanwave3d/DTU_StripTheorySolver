function [q] = IntegraEquationlSolver(x,w,a,s,c)

% Taking care of the dummy sections
% nst = length(S(vertcat(S.Nv)>0));
% if S(1).Nv == 0, S(1).Cx = 0; end
% if S(end).Nv == 0, S(end).Cx = 0; end
% nseg = nst-1;

order = length(c.m)-1;
nst = length(x);
width = order+1;
nseg = nst-1;
mat = diag(ones(nst,1),0); % Matrix for the integral equation
rhs = zeros(nst,1);        % right-hand-side vector

for row=1:nst
    a.parm.row = row;
    A = a.f(x(row), a.parm);
    rhs(row) = s.f(x(row), s.parm);
    for p=1:nseg
        ds = x(p+1)-x(p);
        for i=1:width % THIS DEPENDS ON THE DERIVATIVES OP TO ORDER "Order"
            if p <= (order/2) %  POINTS AT THE LEFT BOUNDARY
                counter = 1;
                for j=1:order+1
                    w.parm.zeta = x(j);
                    W = w.f(x(row),w.parm);
                    mat(row,j) = mat(row,j) + ds*A*W*c.l(i,counter,p)/factorial(i);
                    counter = counter+1;
                end
            elseif  (nst-p) < order/2 % POINTS AT THE RIGHT BOUNDARY
                rem = nst-p;
                counter = 1;
                for j=p-(order-rem):nst
                    w.parm.zeta = x(j);
                    W = w.f(x(row),w.parm);
                    mat(row,j) = mat(row,j) + ds*A*W*c.r(i,counter,rem+1)/factorial(i);
                    counter = counter+1;
                end
            else % INTERNAL POINTS
                counter = 1;
                for j=(p-order/2):(p+order/2)
                    w.parm.zeta = x(j);
                    W = w.f(x(row),w.parm);
                    mat(row,j) = mat(row,j) + ds*A*W*c.m(i,counter)/factorial(i);
                    counter = counter+1;
                end
            end
        end
    end
end

% Solve the system
q = mat\rhs;

end