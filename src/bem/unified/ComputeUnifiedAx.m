function [A] = ComputeUnifiedAx(~, params)

S = params.S;
K = params.K;
row = params.row;
pm = params.pm;

sigma = KochinFunction_m(K,0,S(row).xv,S(row).phi(:,f,2),S(row).nv(:,2),pm);
A = 1i/(2*pi)*(sigma/conj(sigma)+1);

end