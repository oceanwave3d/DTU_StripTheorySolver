function [G,Gn]=Integral_G_Wave_LineSegment_Kashiwagi(K,xi,xj)
%
%  Compute the integrals of the wave part of the 2D Green function and its  
%  normal derivative with respect to panel j, over the line segment panels 
%  defined by the vertices xj(1:Nj+1,1:2) at the field points defined by 
%  the positions xi(1:Ni,1:2). K is the infinite water depth wavenumber. 
%  The function is only implemented for infinite water depth. 
%
% This implementation follows the Lecture note "Wave-Body Interaction
% Theory (Theory of Ship Waves)" by Prof. M. Kashiwagi, Osaka U. 
% (2019), Ch. 6. 
%
% All the outputs are matrices of size Ni X Nj 
%
% Outputs         Func.              Kashiwagi's notation
%------------------------------------------------------------------------
%      G       = Int(G)            = -2 hat{S}_n(x,y)
%      Gn      = Int(Gn)           = -2 [F_S(x-xi,y-eta)]_n^{n+1}
%
% Written by H.B. Bingham, November, 2019. 
%  
Ni=size(xi,1);       % The number of equations (field points)
Nj=size(xj,1)-1;     % The number of body panels (source points)
% Initialize
G=zeros(Ni,Nj); Gn=G;
%
% The j panel lengths and sines and cosines. 
%
dx=xj(2:Nj+1,1)-xj(1:Nj,1); dy=xj(2:Nj+1,2)-xj(1:Nj,2);
ds=sqrt(dx.^2+dy.^2);
CosDeltaj=dx./ds; 
SinDeltaj=dy./ds;
%
% Build the matrix column-by-column
%
for j=1:Nj
    x0=xi(:,1)-xj(j,1); y0=xi(:,2)+xj(j,2); 
    z0=complex(y0,abs(x0)); r0=abs(z0);
    x1=xi(:,1)-xj(j+1,1); y1=xi(:,2)+xj(j+1,2); 
    z1=complex(y1,abs(x1)); r1=abs(z1);
    %
    Expz0=exp(-K*z0); E0=expint(-K*z0); 
    Fc0=real(Expz0.*E0) -1i*pi*Expz0; Fs0=sign(x0).*(imag(Expz0.*E0) -pi*Expz0);
    
    Expz1=exp(-K*z1); E1=expint(-K*z1); 
    Fc1=real(Expz1.*E1) -1i*pi*Expz1; Fs1=sign(x1).*(imag(Expz1.*E1) -pi*Expz1);

    G(:,j)=2/K*( SinDeltaj(j,1).*( log(r1./r0) + (Fc1-Fc0) ) ...
        + CosDeltaj(j,1).*( (atan2(y1,x1)-atan2(y0,x0)) - (Fs1-Fs0) ) );

    Gn(:,j)=-2*(Fs1-Fs0);
    
    
end
