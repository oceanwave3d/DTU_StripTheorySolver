function [G,Gn,Gp,Gnp]=Integral_LogR_LineSegment_Kashiwagi(xi,xj,isym)
%
%  Compute the integrals of log(r), log(r1) and d/dn_j(log(r), log(r1)) 
%  over the line segment panels defined by the vertices xj(1:Nj+1,1:2) at 
%  the field points defined by the positions xi(1:Ni,1:2). 
%  Here r=sqrt((x-xi)^2+(y-eta)^2) and r1=sqrt((x-xi)^2+(y+eta)^2), where
%  xi=[x,y] and panel j has coordinates [xi,eta]. If isym==0, 
%  then the diagonal term of pi is put into the diagonal of the integral of 
%  the d/dn of log(r) term. 
%
% This implementation follows the Lecture note "Wave-Body Interaction
% Theory (Theory of Ship Waves)" by Prof. M. Kashiwagi, Osaka U. 
% (2019), Ch. 6. 
%
% All the outputs are matrices of size Ni X Nj 
%
% Outputs         Func.              Kashiwagi's notation
%------------------------------------------------------------------------
%          G   = Int(1/r)          = T_n(x,y)
%          Gn  = Int(d/dn_j(1/r))  = L_n(x,y)
%          Gp  = Int(1/r1)         = T_n(x,-y)
%          Gnp = Int(d/dn_j(1/r1)) = L_n(x,-y) 
%
% Written by H.B. Bingham, November 2019. 
%
Ni=size(xi,1);       % The number of equations (field points)
Nj=size(xj,1)-1;     % The number of body panels (source points)
% Initialize
G=zeros(Ni,Nj); Gn=G;
%
% The j panel lengths and sines and cosines. 
%
dx=xj(2:Nj+1,1)-xj(1:Nj,1); dy=xj(2:Nj+1,2)-xj(1:Nj,2);
ds=sqrt(dx.^2+dy.^2);
CosDeltaj=dx./ds; 
SinDeltaj=dy./ds;
%
% Build the matrices column-by-column
%
for j=1:Nj
    x0=xi(:,1)-xj(j,1); y0=xi(:,2)-xj(j,2); ym0=-xi(:,2)-xj(j,2); 
    r0=sqrt(x0.^2+y0.^2); rm0=sqrt(x0.^2+ym0.^2);
    x1=xi(:,1)-xj(j+1,1); y1=xi(:,2)-xj(j+1,2); ym1=-xi(:,2)-xj(j+1,2); 
    r1=sqrt(x1.^2+y1.^2); rm1=sqrt(x1.^2+ym1.^2); 
    sub0=x0*CosDeltaj(j,1)+y0*SinDeltaj(j,1); sub1=x1*CosDeltaj(j,1)+y1*SinDeltaj(j,1);
    Coef=x0*SinDeltaj(j,1)-y0*CosDeltaj(j,1); MagC=abs(Coef);
    ii=MagC<eps; MagC(ii)=eps; 
    subm0=x0*CosDeltaj(j,1)+ym0*SinDeltaj(j,1); subm1=x1*CosDeltaj(j,1)+ym1*SinDeltaj(j,1);
    Coefm=x0*SinDeltaj(j,1)-ym0*CosDeltaj(j,1); MagmC=abs(Coefm);
    ii=MagmC<eps; MagmC(ii)=eps; 

    ATanDiff=(atan(sub1./MagC) - atan(sub0./MagC));
    Gn(:,j)=Coef./MagC.*ATanDiff;
    if isym==0
        Gn(j,j)=pi;
    end
    ATanDiffm=(atan(subm1./MagmC) - atan(subm0./MagmC));
    Gnp(:,j)=Coefm./MagmC.*ATanDiffm;
    
    G(:,j)=-( sub1.*log(r1) - sub0.*log(r0) ) - MagC.*ATanDiff; 
    Gp(:,j)=-( subm1.*log(rm1) - subm0.*log(rm0) ) - MagmC.*ATanDiffm;

    
end
