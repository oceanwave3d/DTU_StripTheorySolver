function [SolOut, SOut] = DTU_StripTheorySolver(SUser, U, beta, om0User, g, L, Zg, RGyr, KochinType, ARMethod)
%% DTU_StripTheorySolver
%
% A function to compute the heave and pitch response of a ship in
% waves from heading angle beta, at a series of speeds and
% encounter-frequencies using the strip theory method of Salvesen, Tuck and
% Faltinsen (1970). Extension to the full 5 DOF response is in progress.
%
% Written by H.B. Bingham and Mostafa Amini-Afshar
%
% Dept. of Civil and Mechanical Engineering
% Technical University of Denmark
%
%% Input
%
% All input is dimensional in SI units.
%
% S(1:nst).xst    The longitudinal locations of the stations along the ship.
% S(1:nst).Nv     The number of vertices on the 1/2 section of each
%                 station. If Nv=0, the station is a zero-area dummy
%                 station representing the bow (or stern).
% S(:).x(1:Nv,2)  The y-z panel vertex coordinates of the station with z
%                 measured down from the free-surface and the points
%                 ordered to run from the waterline to the keel.
% S(:).CatFlag    =='yes', or 'no', to indicate whether this hull defines a
%                 catamaran or a monohull. If it does not exist, we assume
%                 a monohull. Note that xv should still only define the 1/2
%                 sections of one hull from its centerline. The reflections
%                 and transformation to +- Bcat/2 are made in
%                 GetSectionalGeometry.
% S(:).Bcat       The distance between the centerlines of the hulls when
%                 CatFlag='yes'.
% U(1:nU)         The forward-speed velocities to solve for in m/s.
% beta            The incident wave heading angle to solve for.
% om0User(1:nf)   The absolute frequencies to solve for in rad/s.
% L               A length-scale in m for non-dimensionalization.
% g               The gravitational acceleration in m/s^2.
% rho             The water density in kg/m^3.
% Zg              The vertical position of the center of gravity in m.
% RGyr            The 3 x 3 radii of gyration in roll, pitch and yaw [m].
% KochinType      Either 2D or 3D indicating whether we use the 2D or 3D
%                 normal vector to each section.
% ARMethod        The method for calculation of added resistance.
%                 4 options can be specified:
%                 ARMethod = "sal" for the Salvesen's method
%                 ARMethod = "mar" for the far-field Maruo's method
%                 ARMethod = "all" for both of above-mentioned methods
%                 ARMethod = "non" no added resistance is calculated
%
%% Output
%
% All output is non-dimensional with respect to rho, g and L.
%
% Sol(1:nU).Fjk(1:nf,6,6) Fjk=Ajk/(rho L^m) - i Bjk/(om rho L^m) where
%                         om=dimensional encounter frequency and m=3 for
%                         j&k<=3, m=4 for j<=3,k>3 (or vice-versa) and m=5
%                         for j&k>3.
% Sol(:).XjD(1:nf,6)      Excitation forces non-dimensionalized by rho g
%                         A L^n, n=2 for j<=3 and n=3 otherwise. A=incident
%                         wave amplitude.
% Sol(:).xi(1:nf,6)       RAOs non-dimensionalized by A for j<=3 and L/A
%                         otherwise.
% Sol(:).[Vol,Xb,Zb]      Non-dimensional body volume, center of buoyancy
%                         positions.
% Sol(:).[Mjk,Cjk]        Non-dimensional body inertia and hydrostatic
%                         matrices.
% Sol(:).Rw[Maruo,MaruoApp,Salvesen,GB]
%                         Added resistance using four different methods.
% S(:).n3D(:,2)           The 3D normal vector to each body panel.
%
%
%% Preliminary work

nU       = length(U);
FnVec    = U/sqrt(g*L);
om0User  = om0User*sqrt(L/g);
user_nf  = length(om0User);
nu0User  = om0User.^2;
cosB     = cos(beta);
xst      = vertcat(SUser.xst)/L;
dx       = diff(xst);
nst      = length(xst);
Zg       = Zg/L;
RGyr     = RGyr/L;
SUser    = Compute3DNormals(SUser);
HeadSeas = cosB<0 || abs(cosB)< eps;
pm = -1; if sin(beta)<0, pm = 1; end
%
% We assume a monohull if CatFlag does not exist
%
if isfield(SUser(2),'CatFlag')
    CatFlag=SUser(2).CatFlag;
else
    CatFlag='no'; for i=1:nst, SUser(i).CatFlag='no'; end
end

% -------------------------------------------------------------------------
% NOTE(maaf): For head-seas cases, Solve_BEM_2D_WaveG routine is called
%   using a uniform-spaced encounter frequency vector. The upepr bound of
%   this vector is the encounter frequency corresponding to the highest
%   Froude number in the user-defined speed vector. The lower bound of the
%   vector is equal to the minimum user-defined wave frequency.
%   For following-seas cases, the routine is called instead using a
%   uniform-spaced wave frequency vector. The higher bound in this vector
%   is equal to the maximun wave frequency in the user-defined frequency
%   vector. The lower bound of the vector is set to 1/4 of the minimum wave
%   frequency in the user-defined vector. All this, is carried out in order
%   to make sure the frequency domain which is interpolated from is wider
%   than the frequency domain which is interpolated to. For both cases this
%   uniform frequency space and its corresponding wave number are named
%   "omSol" and "nuSol".
% -------------------------------------------------------------------------

if HeadSeas
    nfs = user_nf;
    omSolMaxBased = om0User-nu0User.*max(FnVec)*cosB;
    omSolutions = {linspace(min(om0User),max(omSolMaxBased),user_nf)'};
else
    OmeVec = om0User-nu0User.*max(FnVec)*cosB; % Vector of the encounter frequencies.
    PosOme = OmeVec((OmeVec>0));  % Positive part of the vector
    NegOme = -OmeVec((OmeVec<0)); % Negative part of the vector (turned positives!)
    om0UserPos = om0User((OmeVec>0)); % om0 corresponding to positive part
    om0UserNeg = om0User((OmeVec<0)); % om0 corresponding to negative part
    nu0UserPos = om0UserPos.^2; % nu0 corresponding tp positive part
    nu0UserNeg = om0UserNeg.^2; % nu0 corresponding to negative part
    om_pos_space = linspace(0.25*min(om0UserPos), max(om0UserPos), length(PosOme))';
    omSolutions  = {om_pos_space};
    nfs = [length(PosOme)];
    if ~isempty(NegOme)
        om_neg_space = linspace(0.25*min(om0UserNeg), max(om0UserNeg),user_nf-length(PosOme))';
        omSolutions = {om_pos_space, om_neg_space};
        nfs=[nfs, user_nf-length(PosOme)];
    end
end

SOut = SUser; % 2D sectional solutions are added to SOut

SolOut = repmat(struct('Fjk',{}, 'xi',{}, 'XjDfk',{},'XjDs',{}, 'RwSalvesen',{} ,'RwMaruo',{}), 1, nU);

for fv=1:length(nfs)

    omSol = omSolutions{fv};
    nuSol = omSol.^2;
    S = SUser;
    nf = nfs(fv);
    range = 1+(fv-1)*nfs(1):nfs(1)+(fv-1)*nfs(fv);

    if fv==2
        om0UserPos = om0UserNeg;
        nu0UserPos = nu0UserNeg;
    end

    % -------------------------------------------------------------------------
    % Pre-allocate the varibales, and define finite-difference coefficients for
    % the Taylor integration on a uniform grid (Used for the Marou's method)
    % -------------------------------------------------------------------------

    RwMaruo = zeros(nf,1);
    RwSalvesen = zeros(nf,1);
    order = 4;
    Cmid  = FDCoefficients(order/2, order/2);
    Cleft = zeros(order+1, order+1, order+1);
    Cright= zeros(order+1, order+1, order+1);
    for index=1:order+1
        Cleft (:,:,index) = FDCoefficients(index-1, order-index+1);
        Cright(:,:,index) = FDCoefficients(order-index+1, index-1);
    end
    FdCoeff.C      = Cmid;
    FdCoeff.Cleft  = Cleft;
    FdCoeff.Cright = Cright;

    Sol = repmat(struct('Fjk',{}, 'xi',{}, 'XjDfk',{},'XjDs',{}, 'RwSalvesen',{} ,'RwMaruo',{}), 1, nU);

    %% Loop over the stations and solve on each station.

    xsym=0; % Do not exploit symmetry as this is not fully implemented.

    parfor i=1:nst
        % ---------------------------------------------------------------------
        % Check for a dummy station indicated by Nv=0.
        % ---------------------------------------------------------------------
        if S(i).Nv==0
            S(i).fjk=zeros(nf,2,2); S(i).Hp=zeros(nf,2,2);
            S(i).xCoor=S(i).xst/L;
            fprintf('%s %d %s %d %s \n', 'section' ,i, 'of', nst,' is a dummy section.');
            continue
        end
        % ---------------------------------------------------------------------
        % Get the sectional coefficients, & for a normal station, reflect the
        % panels about the y=0 plane to work with the whole body since we do
        % not yet exploit symmetry. The 3D normal vectors are also reflected
        % here.
        % ---------------------------------------------------------------------
        S(i).xCoor =[];S(i).xv=[];S(i).b=[];S(i).ds=[];
        S(i).nv=[];S(i).xc=[];S(i).beam=[];S(i).draft =[];
        S(i).area=[];S(i).areaND=[];
        [S(i)] = GetSectionalGeometry(S(i), L, 0);
        % ---------------------------------------------------------------------
        % Set irr=1 to remove the irregular frequencies and give the x-limits
        % of the interior free surface, as long as this section is
        % surface-piercing.
        % ---------------------------------------------------------------------
        if S(i).x(1,1)>eps
            switch S(i).CatFlag
                case 'yes'
                    irr=1; x0FS=S(i).Bcat/2-S(i).b; x1FS=S(i).Bcat/2+S(i).b;
                otherwise
                    irr=1; x0FS=-S(i).b; x1FS=S(i).b;
            end
        else
            irr=0; x0FS=0; x1FS=0;
        end
        % ---------------------------------------------------------------------
        % Solve the surge, heave and scattering problems on this section.
        % Here we switch the 2D boundary conditions to be either 2D or 3D based
        % on the chosen KochinType. (hbb)
        % ---------------------------------------------------------------------
        if strcmp(KochinType, '2D')
            dphidn=S(i).nv;
        else
            dphidn=S(i).n3D(:,2:3);
        end
        [fjk, E, Hp, Hm, phi, ScatBC] = Solve_BEM_2D_WaveG(nuSol, xst(i), S(i).xv, ...
            S(i).xc, dphidn, S(i).ds,xsym, irr, x0FS, x1FS, beta, S(i).CatFlag);
        %---------------------------------------------------------------------
        % Load the solutions for this section into the solution structure
        %---------------------------------------------------------------------
        S(i).fjk    = fjk;
        S(i).Hp     = Hp;
        S(i).Hm     = Hm;
        S(i).E      = E;
        S(i).phi    = phi;
        S(i).ScatBC = ScatBC;
        fprintf('%s %d %s %d %s \n', 'section' ,i, 'of', nst, 'solved');
    end
    %
    %% Compute the hydrostatic and mass matrices

    switch CatFlag
        case 'yes'
            [Cjk,Vol,Xb,Zb] = GetHydrostaticsCat(S, Zg);
        otherwise
            [Cjk,Vol,Xb,Zb] = GetHydrostatics(S, Zg);
    end
    Xg  = Xb; % Cg must lie on top of the Cb for static equilibrium.
    Mjk = GetInertiaMatrix(Vol, Xg, Zg, RGyr);

    %% Compute the sectional F-K and scattering Kochin functions on each station.
    %
    for i=1:nst

        % For dummy stations, set the functions to zero.
        if S(i).Nv==0
            S(i).f3=zeros(nf,1); S(i).h3=zeros(nf,1); S(i).hs=zeros(nf,1);
            continue
        end
        Nv=size(S(i).xv,1);
        switch S(i).CatFlag
            case 'yes'
                N=Nv-2;
            otherwise
                N=Nv-1;
        end
        % ---------------------------------------------------------------------
        % This function computes:
        % H(m)=\int(phin-phi d/dn)exp{-Kz+i\sqrt{K^2-m^2}y},
        % so to get iKy sin(beta) we pass m=K cos(beta).
        % ---------------------------------------------------------------------
        for iom=1:nf
            switch KochinType
                case '3D'
                    S(i).f3(iom,1) = KochinFunction_m_3D(nuSol(iom),nuSol(iom)*cosB, ...
                        S(i).xv, zeros(2*(S(i).Nv-1),1), S(i).n3D(:,3),S(i).n3D);
                    S(i).h3(iom,1) = KochinFunction_m_3D(nuSol(iom),nuSol(iom)*cosB, ...
                        S(i).xv, squeeze(S(i).phi(:,iom,2)),zeros(2*(S(i).Nv-1),1),S(i).n3D);
                case '2D'
                    switch CatFlag
                        case 'yes'
                            f1 = KochinFunction_m(nuSol(iom),nuSol(iom)*cosB, ...
                                S(i).xv(1:N/2+1,:), zeros(N/2,1), S(i).nv(1:N/2,2),pm);
                            f2 = KochinFunction_m(nuSol(iom),nuSol(iom)*cosB, ...
                                S(i).xv(N/2+2:N+2,:), zeros(N/2,1), S(i).nv(N/2+1:N,2),pm);
                            S(i).f3(iom,1)=f1+f2;
                            f1 = KochinFunction_m(nuSol(iom),nuSol(iom)*cosB, ...
                                S(i).xv(1:N/2+1,:), squeeze(S(i).phi(1:N/2,iom,2)),...
                                zeros(N/2,1),pm);
                            f2 = KochinFunction_m(nuSol(iom),nuSol(iom)*cosB, ...
                                S(i).xv(N/2+2:N+2,:), squeeze(S(i).phi(N/2+1:N,iom,2)),...
                                zeros(N/2,1),pm);
                            S(i).h3(iom,1)=f1+f2;
                        otherwise
                            S(i).f3(iom,1) = KochinFunction_m(nuSol(iom),nuSol(iom)*cosB, ...
                                S(i).xv, zeros(2*(S(i).Nv-1),1), S(i).nv(:,2),pm);
                            S(i).h3(iom,1) = KochinFunction_m(nuSol(iom),nuSol(iom)*cosB, ...
                                S(i).xv, squeeze(S(i).phi(:,iom,2)),zeros(2*(S(i).Nv-1),1),pm);
                    end
                    % Integrate the scattered heave force over this section and
                    % multiply by exp(-i nuSol*cos(beta)*x), i.e. the assumed
                    % variation in x- of the 3D scattered potential.
                    S(i).hs(iom,1) = trapz(squeeze(S(i).phi(:,iom,4)).*S(i).nv(:,2).*S(i).ds)...
                        *exp(1i*nuSol(iom)*xst(i)*cosB);
            end
        end
    end

    %% Solve for the 3D hydrodynamic coefficients and response RAOs

    for iU=1:nU

        Fn = FnVec(iU);

        if HeadSeas
            if (Fn==0 || abs(cosB)< eps)
                om0Sol = omSol;
            else
                om0Sol = 1/(2*cosB*Fn)*(1-sqrt(1-4*Fn*cosB*omSol));
            end
        end

        for t=1:nst
            if S(t).Nv==0
                S(t).ScatPhiMapped =[];
                S(t).ScatBCMapped  =[];
                continue
            end
            %
            if HeadSeas
                S(t).ScatPhiMapped(:,:) = (interp1(omSol, (S(t).phi(:,:,4)).', om0Sol,'linear','extrap')).';
                S(t).ScatBCMapped(:,:) = (interp1(omSol, (S(t).ScatBC(:,:)).', om0Sol, 'linear','extrap')).';
                S(t).HeavePhiMapped(:,:) = S(t).phi(:,:,2);
            else
                userOme = abs(om0UserPos-nu0UserPos*Fn*cosB);
                S(t).ScatPhiMapped(:,:) = (interp1(omSol,(S(t).phi(:,:,4)).', om0UserPos,'linear','extrap')).';
                S(t).ScatBCMapped (:,:) = (interp1(omSol,(S(t).ScatBC(:,:)).', om0UserPos, 'linear','extrap')).';
                S(t).HeavePhiMapped(:,:) = (interp1(omSol,S(t).phi(:,:,2).', userOme,'linear','extrap')).';
            end
        end
        %

        S = ComputeSurfaceDerivatives(S); % Required only for Salvesen's method

        %%

        % ---------------------------------------------------------------------
        % Integrate over the stations using the trapezoid rule to
        % get the phi^0 terms.
        % ---------------------------------------------------------------------

        Fjk=zeros(nf,6,6); XjD=zeros(nf,6); H3U=zeros(nf,1); XjDs=zeros(nf,6);
        XjDfk=zeros(nf,6);

        for i=1:nst-1
            Fjk(:,3,3)=Fjk(:,3,3) + (S(i+1).fjk(:,2,2)+S(i).fjk(:,2,2))/2 * dx(i);
            Fjk(:,3,5)=Fjk(:,3,5) - ...
                (xst(i+1)*S(i+1).fjk(:,2,2)+xst(i)*S(i).fjk(:,2,2))/2 * dx(i);
            Fjk(:,5,5)=Fjk(:,5,5) + ...
                (xst(i+1)^2*S(i+1).fjk(:,2,2)+xst(i)^2*S(i).fjk(:,2,2))/2 * dx(i);
            XjD(:,3)= XjD(:,3) + dx(i)* ...
                ((S(i+1).f3+S(i+1).h3).*exp(-1i*xst(i+1)*nuSol*cosB) ...
                +(S(i).f3+S(i).h3).*exp(-1i*xst(i)*nuSol*cosB))/2 ;
            XjDfk(:,3)= XjDfk(:,3) + dx(i)* ...
                ((S(i+1).f3).*exp(-1i*xst(i+1)*nuSol*cosB) ...
                +(S(i).f3).*exp(-1i*xst(i)*nuSol*cosB))/2 ;
            XjDs(:,3)= XjDs(:,3) + dx(i)* ...
                ((S(i+1).hs).*exp(-1i*xst(i+1)*nuSol*cosB) ...
                +(S(i).hs).*exp(-1i*xst(i)*nuSol*cosB))/2 ;
            XjD(:,5)= XjD(:,5) - dx(i)* ...
                (xst(i+1)*(S(i+1).f3+S(i+1).h3).*exp(-1i*xst(i+1)*nuSol*cosB) ...
                +xst(i)*(S(i).f3+S(i).h3).*exp(-1i*xst(i)*nuSol*cosB))/2 ;
            H3U=H3U+(S(i+1).h3.*exp(-1i*xst(i+1)*nuSol*cosB)...
                +S(i).h3.*exp(-1i*xst(i)*nuSol*cosB))/2 * dx(i);
        end

        % ---------------------------------------------------------------------
        % Add in the forward-speed terms. Station number 1 is assumed to be the
        % aft station here which may or may not be zero depending on whether or
        % not the ship has a transom-sterm.
        % ---------------------------------------------------------------------

        Fjk(:,5,5)=Fjk(:,5,5)+Fn^2./(omSol.^2).*Fjk(:,3,3)...
            +Fn./(1i*omSol).*xst(1)^2.*S(1).fjk(:,2,2)-Fn^2./omSol.^2.*xst(1).*S(1).fjk(:,2,2);
        Fjk(:,5,3)=Fjk(:,3,5)-Fn./(1i*omSol).*Fjk(:,3,3)...
            +Fn./(1i*omSol).*xst(1).*S(1).fjk(:,2,2);
        Fjk(:,3,5)=Fjk(:,3,5)+Fn./(1i*omSol).*Fjk(:,3,3)...
            +Fn./(1i*omSol).*xst(1).*S(1).fjk(:,2,2)-Fn^2./omSol.^2.*S(1).fjk(:,2,2);
        Fjk(:,3,3)=Fjk(:,3,3)+Fn./(1i*omSol).*S(1).fjk(:,2,2);
        XjD(:,3)=XjD(:,3)+Fn./(1i*omSol).*S(1).h3;
        XjD(:,5)=XjD(:,5)-Fn./(1i*omSol).*H3U-Fn./(1i*omSol).*xst(1).*S(1).h3;

        % ---------------------------------------------------------------------
        % Solve the 2 DOF heave-pitch problem
        % ---------------------------------------------------------------------

        xi=zeros(nf,6);
        ifix=[1,2,4,6];

        % Interpolate the exciting and the radiation forces

        Fjki  = Fjk;
        omRes = omSol;
        if HeadSeas
            XjDi   = interp1(omSol,XjD,om0Sol,'linear', 'extrap');
            XjDfki = interp1(omSol,XjDfk,om0Sol,'linear', 'extrap');
            XjDsi  = interp1(omSol,XjDs,om0Sol,'linear', 'extrap');
            ARom0  = om0Sol;
        else
            omeUser = abs(om0UserPos-nu0UserPos*Fn*cosB);
            Fjki   = interp1(omSol,Fjk,omeUser,'linear','extrap');
            XjDi   = interp1(omSol,XjD,om0UserPos,'linear','extrap');
            XjDfki = interp1(omSol,XjDfk,om0UserPos,'linear','extrap');
            XjDsi  = interp1(omSol,XjDs,om0UserPos,'linear','extrap');
            omRes  = omeUser;
            ARom0  = om0UserPos;
        end

        % Solve for the responses and compute the added resistance.

        for i=1:nf
            mat=(-omRes(i)^2*(Mjk+squeeze(Fjki(i,:,:)))+Cjk);
            mat(ifix,:)=[]; mat(:,ifix)=[];
            rhs=XjDi(i,[3,5]).';
            sol=mat\rhs;
            xi(i,[3,5])=sol.';
        end
        parfor i=1:nf
            if strcmp(ARMethod,'mar')
                [RwMaruo(i),~,~,~] = ARMaruo(Fn, xi(i,:), ARom0(i), omRes(i), S, i, beta, FdCoeff, KochinType);
                fprintf('%s %d %s %d %s %d %s %d \n', 'Added resistance computed (Maruo). Speed:' ,iU, 'of', nU,', frequency:', i ,'of', nf);
            elseif strcmp(ARMethod,'sal')
                RwSalvesen(i) = ARSalvesen(Fn, xi(i,:), ARom0(i), omRes(i), S, i, beta, KochinType);
                fprintf('%s %d %s %d %s %d %s %d \n', 'Added resistance computed (Salvesen). Speed:' ,iU, 'of', nU,', frequency:', i ,'of', nf);
            elseif strcmp(ARMethod,'all')
                [RwMaruo(i),~,~,~] = ARMaruo(Fn, xi(i,:), ARom0(i), omRes(i), S, i, beta, FdCoeff, KochinType);
                RwSalvesen(i) = ARSalvesen(Fn, xi(i,:), ARom0(i), omRes(i), S, i, beta, KochinType);
                fprintf('%s %d %s %d %s %d %s %d \n', 'Added resistance computed (All methods). Speed:' ,iU, 'of', nU,', frequency:', i ,'of', nf);
            end
        end

        %
        %% Pack this solution into the 3D output structure.

        % ---------------------------------------------------------------------
        % Interpolate the solutions back to the user-defined absolute
        % frequency domain (Only required for the head-seas cases)
        % ---------------------------------------------------------------------

        if HeadSeas
            omeUser = om0User-nu0User*Fn*cosB;
            Sol(iU).Fjk   = interp1(omSol,Fjki,omeUser,'linear');
            Sol(iU).XjD   = interp1(omSol,XjDi,omeUser,'linear');
            Sol(iU).XjDfk = interp1(omSol,XjDfki,omeUser,'linear');
            Sol(iU).XjDs  = interp1(omSol,XjDsi,omeUser,'linear');
            Sol(iU).xi    = interp1(omSol,xi,omeUser,'linear');

            if strcmp(ARMethod,'mar')
                Sol(iU).RwMaruo = interp1(omSol,RwMaruo,omeUser,'linear');
            elseif strcmp(ARMethod,'sal')
                Sol(iU).RwSalvesen = interp1(omSol,RwSalvesen,omeUser,'linear');
            elseif strcmp(ARMethod,'all')
                Sol(iU).RwMaruo = interp1(omSol,RwMaruo,omeUser,'linear');
                Sol(iU).RwSalvesen = interp1(omSol,RwSalvesen,omeUser,'linear');
            end
        else
            Sol(iU).Fjk   = Fjki;
            Sol(iU).XjD   = XjDi;
            Sol(iU).XjDfk = XjDfki;
            Sol(iU).XjDs  = XjDsi;
            Sol(iU).xi    = xi;

            if strcmp(ARMethod,'mar')
                Sol(iU).RwMaruo = RwMaruo;
            elseif strcmp(ARMethod,'sal')
                Sol(iU).RwSalvesen = RwSalvesen;
            elseif strcmp(ARMethod,'all')
                Sol(iU).RwMaruo = RwMaruo;
                Sol(iU).RwSalvesen = RwSalvesen;
            end
        end

        Sol(iU).Vol = Vol;
        Sol(iU).Xb  = Xb;
        Sol(iU).Zb  = Zb;
        Sol(iU).Cjk = Cjk;
        Sol(iU).Mjk = Mjk;
        %
    end

    % Arrange 2D solutions

    for t=1:nst
        if S(t).Nv~=0
            SOut(t).fjk(range,:,:) = S(t).fjk;
            SOut(t).Hp(range,:) = S(t).Hp;
            SOut(t).Hm(range,:) = S(t).Hm;
            SOut(t).E(range,:) = S(t).E;
            SOut(t).phi(:,range,:) = S(t).phi;
            SOut(t).f3(range,1) = S(t).f3;
            SOut(t).h3(range,1) = S(t).h3;
            SOut(t).hs(range,1) = S(t).hs;
            SOut(t).ScatPhiMapped(:,range) = S(t).ScatPhiMapped;
            SOut(t).ScatBCMapped(:,range) = S(t).ScatBCMapped;
            SOut(t).HeavePhiMapped(:,range) = S(t).HeavePhiMapped;
            SOut(t).dphi_heave_dx(:,range) = S(t).dphi_heave_dx;
            SOut(t).dphi_heave_dy(:,range) = S(t).dphi_heave_dy;
            SOut(t).dphi_heave_dz(:,range) = S(t).dphi_heave_dz;
            SOut(t).dphi_scatr_dx(:,range) = S(t).dphi_scatr_dx;
            SOut(t).dphi_scatr_dy(:,range) = S(t).dphi_scatr_dy;
            SOut(t).dphi_scatr_dz(:,range) = S(t).dphi_scatr_dz;
            SOut(t).dn1dx = S(t).dn1dx;
            SOut(t).dn2dx = S(t).dn2dx;
        end
    end

    % Arrange 3D solutions

    for iU=1:nU
        SolOut(iU).Fjk(range,:,:) = Sol(iU).Fjk;
        SolOut(iU).XjD(range,:) = Sol(iU).XjD;
        SolOut(iU).XjDfk(range,:) = Sol(iU).XjDfk;
        SolOut(iU).XjDs(range,:) = Sol(iU).XjDs;
        SolOut(iU).xi(range,:) = Sol(iU).xi;
        if ~isempty(Sol(iU).RwMaruo); SolOut(iU).RwMaruo(range,1) = Sol(iU).RwMaruo; end
        if ~isempty(Sol(iU).RwSalvesen); SolOut(iU).RwSalvesen(range,1) = Sol(iU).RwSalvesen; end
        SolOut(iU).Vol = Sol(iU).Vol;
        SolOut(iU).Xb = Sol(iU).Xb;
        SolOut(iU).Zb = Sol(iU).Zb;
        SolOut(iU).Cjk = Sol(iU).Cjk;
        SolOut(iU).Mjk = Sol(iU).Mjk;
    end
    clear Sol  S;
end
