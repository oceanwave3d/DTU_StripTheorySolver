function Dy=BuildDy2DOneSided(Nx,Ny,y,alpha,der)
%
% A function to build the sparse der^th y-derivative operator matrix on a 
% structured but non-uniform 2D grid.   
% One-sided schemes are used towards the boundaries.  The grid 
% is assumed to follow y first from bottom left to top right.  
%
%    der must of course be <= r-1; where r=2*alpha+1.
%
r=2*alpha+1; N=Nx*Ny;
if der > r-1, error('This stencil does not support that derivative'); end
index=0;
for i=1:Nx               % Loop over x-points.
    ip=i;
    for j=1:alpha              % One-sided schemes towards the bottom.
        c=FornbergInterpolate(y(j),y(1:r),r,der);
        im=(i-1)*Ny+j; 
        for jrel=1:r        % Stencil points for this grid point.
            jp=jrel;
            jm=(ip-1)*Ny+jp;
            index=index+1;
            irn(index)=im; icn(index)=jm;
            Mat(index)=c(1+der,jrel);
        end
    end
    for j=alpha+1:Ny-alpha      % Central points in y.
        c=FornbergInterpolate(y(j),y(j-alpha:j+alpha),r,der);
        im=(i-1)*Ny+j; 
        for jrel=1:r        % Stencil points for this grid point.
            jp=j-alpha-1+jrel;
            jm=(ip-1)*Ny+jp;
            index=index+1;
            irn(index)=im; icn(index)=jm;
            Mat(index)=c(1+der,jrel);
        end
    end
    for j=Ny-alpha+1:Ny         % One-sided points towards the top.  
        c=FornbergInterpolate(y(j),y(Ny-r+1:Ny),r,der);
        im=(i-1)*Ny+j; 
        for jrel=1:r        % Stencil points for this grid point.
            jp=Ny-r+jrel;
            jm=(ip-1)*Ny+jp;
            index=index+1;
            irn(index)=im; icn(index)=jm;
            Mat(index)=c(1+der,jrel);
        end
    end
end
Dy=spconvert([irn' icn' Mat']);  