
% This function returns the finite-difference coefficients on a uniform
%  spaced domain. There are "a" points to the left and "b" points to the
%  right. All coefficients are returned in the "C" matrix, where each nth
%  row contains the coefficinets for nth derivative (n = 0, ..., a+b).

function [ C ] = FDCoefficients(a, b)

stenc_size = a + b + 1;

M = zeros(stenc_size, stenc_size);

for i=1:stenc_size
    
    for j=1:stenc_size
        
        if i<=a
            
            M(i, j) = (-1)^(j-1) * (a-i+1)^(j-1)/factorial(j-1); % The left points
            
        elseif i==(a+1)
            
            p = 0;
            
            if j==1
                p=1;
            end
            
            M(i, j) = p;  % The point itself
    
        else
            
            M(i, j) = (i-a-1)^(j-1)/factorial(j-1); % The right points

        end
        
    end
    
end

C = inv(M);

end

