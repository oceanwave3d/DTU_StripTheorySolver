
function [ result ] = TaylorIntegration (Npt, f, dl, C, Cleft, Cright )

Width  = length(C); % STENCIL WIDTH

if Npt<=Width
    
    error(' TaylorIntegration.m : the number of points is larger that the stencil width');
end


Order  = Width - 1; % THE DESIRED ORDER
NumSeg = Npt - 1;   % NUMBER OF THE SEGMENTS
result = 0;

for s=1:NumSeg  % CALCULATE THE INTEGRAL FOR THE SEGMENT WITH INDEX "s"
    
    for i=1:Width % THIS DEPENDS ON THE DERIVATIVES OP TO ORDER "Order"
        
        if s <= (Order/2 + 1) %  POINTS AT THE LEFT BOUNDARY (Expansion around 1)
            
            counter = 1;
            
            for j=2:Order+2
                
                result  = result + f(j) * Cleft(i, counter, s) / factorial(i) * (-1)^(i+1);
                counter = counter + 1;
            end
            
        elseif  (Npt-s-1) < Order/2 % POINTS AT THE RIGHT BOUNDARY (Expansion around 0)
            
            rem     = Npt - s - 1;
            counter = 1;
            
            for j=s-(Order - rem):Npt-1
                
                result = result + f(j) * Cright(i, counter, rem + 1) / factorial(i);
                counter = counter + 1;
            end
            
        else % INTERNAL POINTS (Expansion around 0)
            
            counter = 1;
            
            for j=(s-Order/2):(s+Order/2)
                
                result = result + f(j) * C(i, counter) / factorial(i);
                counter = counter + 1;
                
            end
            
        end
         
    end
    
end

result = result * dl;

end

