function c=FornbergInterpolate(x0,x,n,m)
%
% A matlab implementation of the algorithm of 
%
%     B. Fornberg, 1998. "Calculation of weights in finite difference
%     formulas".  SIAM Rev. Vol. 40, No. 3, pp. 685-691.  
%
% for calculating arbitrary-order FD schemes on arbitrary 1-D grids for all
% applicable derivatives.  The interpolations are found at the point x0,
% using the function positions x(1:n), for derivatives 0:m, m<=n-1;   
%
% 
% Written by H.B. Bingham
%            Mech. Eng.
%            Technical U. of Denmark
%
% This version:  2009
%
%%
if m>n-1, error('Too many derivatives for this stencil, try again.'); end
%
c1=1; c4=x(1)-x0;
c=zeros(n,m+1);
c(1,1)=1;
for i=2:n
    mn=min(i,m+1); c2=1; c5=c4; c4=x(i)-x0;
    for j=1:i-1
        c3=x(i)-x(j);
        c2=c2*c3;
        if j==i-1
            for k=mn:-1:2
                c(k,i)=c1*((k-1)*c(k-1,i-1)-c5*c(k,i-1))/c2;
            end
            c(1,i)=-c1*c5*c(1,i-1)/c2;
        end
        for k=mn:-1:2
            c(k,j)=(c4*c(k,j)-(k-1)*c(k-1,j))/c3;
        end
        c(1,j)=c4*c(1,j)/c3;
    end
    c1=c2;
end
return