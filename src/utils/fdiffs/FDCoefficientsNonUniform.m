%
function [coeff, a, b, d] = FDCoefficientsNonUniform(x, A, B)

for s=1:numel(x)-1
    dx(s)=x(s+1)-x(s);
end
d=min(dx);

for p=1:numel(x)
    % if this is an internal point. Do not change A and B
    if (p>A && (p+B)<=numel(x))
        a(p)=A;
        b(p)=B;
    else
        % this is a boundary point. Change A anb B
        for I=1:numel(x)
            if (p>A-I && (p+B)+I<=numel(x))
                a(p)=A-I;
                b(p)=B+I;
                break
            end
        end
        for I=1:numel(x)
            if (p>A+I && (p+B)-I<=numel(x))
                a(p)=A+I;
                b(p)=B-I;
                break
            end
        end
    end
    % now the value of A and B for each point has been determined
    r=a(p)+b(p)+1;
    
    % populate matrix for "a"
    if a(p)~=0
        for h=1:a(p)
            for v=1:r
                C(h,v)=((x(p-a(p)+h-1)-x(p))/d)^(v-1)/factorial(v-1);
            end
        end
    end
    
    C(a(p)+1,:)=zeros(1,r);C(a(p)+1,1)=1;
    
    % populate matrix for "b"
    if b(p)~=0
        for h=1:b(p)
            for v=1:r
                C(a(p)+1+h,v)=((x(p+h)-x(p))/d)^(v-1)/factorial(v-1);
            end
        end
    end
    coeff(:,:,p)=inv(C);
end

end

