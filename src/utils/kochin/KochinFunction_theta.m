function [H] = KochinFunction_theta(kappaj, sinT, xj, phi, phin)
%
% Compute the Kochin function:
%
% H(m)= \int (phin - phi d/dn) exp{-Kz + i \sqrt{K^2-m^2} y} ds
%
% where K is non-dimensional (infinite depth) wavenumber and m is a vector
% of non-dimensional wavenumbers. The panels are defined by the vertices
% xj(1:N+1,1:2). The potentials and normal derivative of the potentials are
% given by phi(1:N,1) and dphidn(1:N,1). Hp is the Kochin function for y>0
% and Hm the one for y<0. The function can be used for either radiation,
% diffraction or scattering problems by setting phin=n_j,0, or -dphi_I/dn.
%
% This implementation follows the Lecture note "Wave-Body Interaction
% Theory (Theory of Ship Waves)" by Prof. M. Kashiwagi, Osaka U.
% (2019), Ch. 6.
%
% Written by H. B. Bingham, April, 2020
%
%%
N     = size(xj,1)-1;     % The number of body panels
dx    = xj(2:N+1,1)-xj(1:N,1);
dy    = xj(2:N+1,2)-xj(1:N,2);
Delta = atan2(dy,dx);
sinD  = sin(Delta);
cosD  = cos(Delta);

x1 = xj(2:N+1,:);
x0 = xj(1:N,:);

kbar  =  kappaj*sinT;
K1    = -kappaj*sinD + 1i*kbar*cosD;
K2    =  kappaj*cosD + 1i*kbar*sinD;

zeta1 = -kappaj*x1(:,2) + 1i*kbar*x1(:,1);
zeta0 = -kappaj*x0(:,2) + 1i*kbar*x0(:,1);

ExpP  = exp(zeta1) - exp(zeta0);
Fp    = 1 ./K1.*ExpP;
Gp    = K2./K1.*ExpP;

%
% Check for the singular case when both kbar=0 and dy=0, and put in the
% correct values. We assume here that K is never zero, in which case
% all other values are correctly evaluated as above.
%
if kbar <=10*eps
    idy0=find(abs(dy)<=10*eps);
    Fp(idy0)=-dx(idy0).*exp(-kappaj*xj(idy0,2));
    Gp(idy0)=dx(idy0)*kappaj.*exp(-kappaj*xj(idy0,2));
end

H = (phin.'*Fp - phi.'*Gp);

end