function [Vm] = GenerateMesh(V, PanelRes, tol, WithPlot)

% This script can be used for generation of equi-distnace panels along the
% strips. The inputs to the function are:
%
%  1. V (:,[1,2]) : The vector containing the set of the station points.
%  2. PanelRes    : The desired number of the panels.
%  3. tol         : The tolerance for the difference between panel widths
%  4. WithPlot    : The flag (0 or 1) to plot the results.
%
%  Note that the main mesh generation is performed in the funtion:
%  "GetFromEngine". This function is called as many times as required until
%  the required accuracy for the panel widths is achieved.
%


y = V(:,1);
z = V(:,2);

[PanelWidth, BodyLineLength] = GetPanelWidth(y, z, PanelRes);
LastPanelWidth = BodyLineLength;

py = y;
pz = z;

while abs(LastPanelWidth - PanelWidth) >= tol
    [Y, Z] = GetFromEngine(py, pz, PanelWidth);
    LastPanelWidth = sqrt((Y(end) -Y(end-1))^2 + (Z(end) -Z(end-1))^2 );
    [PanelWidth, ~] = GetPanelWidth(Y, Z, PanelRes);
    py = Y;
    pz = Z;
end

Vm(:,1) = Y;
Vm(:,2) = Z;

if WithPlot
    figure
    set(0,'defaulttextinterpreter','latex')
    set(0,'defaultAxesTickLabelInterpreter','latex');
    set(0,'defaultLegendInterpreter','latex');
    plot(Y,-Z, 'b-s', 'LineWidth', 1.2,'MarkerSize', 4);
    hold on
    plot(-y,-z, 'k-x', 'LineWidth',.5, 'MarkerSize', 4);
    axis equal
    legend('Re-meshed','Original', 'Location','NorthOutside');
    xlabel('y');
    ylabel('z');
    set(gca, 'FontSize', 12);
end
end

