%
% This function generates equal-distance panels on the line given 
% by y and z coordinates. It starts generating meshes at the waterline 
% and, the last remaining panel might not have the same width as the rest
% of panels. This is why it should be called as many time as required to
% reach the desired accuracy for a constant panel width. This function 
% works generally as follows:
%
% 1. start from (yStart, zStart) point, and calculate the distance to the 
%    next point in the line.
% 2. if the distance is larger than the PanelWidth, find a point on the 
%    line where the distance is equal to the PanelWidth. This is performed 
%    by a solution to the quadratic equation assembled using the line 
%    geometry between point i+1 and i.
% 3. update the start point
%
%
% Written by: Mostafa Amini-Afshar
% 
function [Y, Z] = GetFromEngine(y, z, PanelWidth)

maxit=10^3; %hbb added a maxit value to catch an infinite while loop.

Npoints = length(y);
yStart = y(1);
zStart = z(1);
Y = [y(1)];
Z = [z(1)];

for i=1:Npoints-1
    
    it=0;
    
    while sqrt((yStart-y(i+1))^2 + (zStart-z(i+1))^2) >= PanelWidth 
        
        dz = z(i+1)-z(i);
        dy = y(i+1)-y(i);
        theta = atan2(dz, dy);
        
        if dy==0
            yEnd = yStart;
            zEnd = zStart + PanelWidth;            
        elseif dz == 0
            yEnd = yStart - PanelWidth;
            zEnd = z(i+1);            
        else
            
            M = z(i)-y(i)*tan(theta);
            a = 1+tan(theta)*tan(theta);
            b = 2*M*tan(theta) - 2*yStart - 2*zStart*tan(theta);
            c = M^2 -2*M*zStart+yStart^2+zStart^2-PanelWidth^2;
            PolCoeff = [a b c];
            r = roots(PolCoeff);
            
            Miny = min(y(i+1), y(i));
            Maxy = max(y(i+1), y(i));
            Minz = min(z(i+1), z(i));
            Maxz = max(z(i+1), z(i));
            
            if (length(Y)>1) && (Miny<=Y(end)) && (Y(end)<=Maxy) && (Minz<=Z(end)) && (Z(end)<=Maxz)
                Miny = min(y(i+1), Y(end));
                Maxy = max(y(i+1), Y(end));
                Minz = min(z(i+1), Z(end));
                Maxz = max(z(i+1), Z(end));
            end
            
            yp1 = r(1);
            yp2 = r(2);
            zp1 = M + r(1)*tan(theta);
            zp2 = M + r(2)*tan(theta);
            
            if (Miny<=yp1) && (yp1<=Maxy) && (Minz<=zp1) && (zp1<=Maxz)
                yEnd = yp1;
                zEnd = zp1;
            else
                yEnd = yp2;
                zEnd = zp2;
            end
            
        end
        
        Y = [Y, yEnd];
        Z = [Z, zEnd];
        
        yStart = yEnd;
        zStart = zEnd;
        
        it=it+1;
        if it>=maxit, error(['GetFromEngine: no convergence after ',num2str(maxit),...
                ' iterations.']);  end
    end
    
end

Y = [Y, y(end)];
Z = [Z, z(end)];

end

