function [ PanelWidth, BodyLineLength ] = GetPanelWidth( y, z, PanelRes )

Npoints = length(y);

DeltaVector = zeros(Npoints-1,1);

for i = 1: Npoints-1
    DeltaVector(i) = sqrt( (y(i+1)-y(i))^2 + (z(i+1)-z(i))^2);
end

BodyLineLength = sum(DeltaVector);
PanelWidth     = BodyLineLength/(PanelRes - 1);
end

