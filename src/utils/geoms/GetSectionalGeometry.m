function [S] = GetSectionalGeometry(S, L, WithPlot)
%
% This routine non-dimensionalizes the geometry, reflects the section to
% the port side and for a cat moves the section to the half-beam and also
% reflects it to the port side. Sectional beam, draft, area and normal
% vectors are also computed and added to the structure along with the panel
% centroid coordinates. 
%
% Assume a monohull if CatFlag does not exist.
%
if isfield(S,'CatFlag') && isfield(S,'Bcat')
    CatFlag=S.CatFlag;
    S.Bcat=S.Bcat/L;
else
    CatFlag='no'; 
end

Y = S.x(:,1)/L;
Z = S.x(:,2)/L;

% Sectional properties required for approximate methods:
% 1. Salvesen 2. Gerritsma Beukelman 3. Maruo (Kochin Functions)

xCoor = S.xst/L;
beam  = 2 * max(Y);             % Non-dimensional beam
draft = abs( max(Z) - min(Z) ); % Non-dimensional draft
Y     = [Y; 0; Y(1)];
Z     = [Z; 0; 0];
areaND = 2 * polyarea(Y, Z);    % The non-dimensional sectional area wrt L
area  = areaND/(beam*draft);    % Sectional area coefficient wrt beam*draft.

% Sectional properties  required for BEM solution
Nv=S.Nv;
N = 2*(S.Nv-1);                                                   % Number of panels on this section after reflection
xv = [S.x;[-S.x(S.Nv-1:-1:1,1),S.x(S.Nv-1:-1:1,2)]];              % The panel vertices running from waterline to waterline.
xv = xv/L;                                                        % Non-dimensionalize the sectional coordinates
b  = max(xv(:,1));                                                % Max. 1/2 beam of this section
ds = sqrt((xv(2:N+1,1)-xv(1:N,1)).^2+(xv(2:N+1,2)-xv(1:N,2)).^2); % The panel lengths
xc = xv(1:N,:)+(xv(2:N+1,:)-xv(1:N,:))/2;                         % The panel centroids
tv = xv(2:N+1,:)-xv(1:N,:); mag=sqrt(tv(:,1).^2+tv(:,2).^2);      % The tangent vectors to each panel
tv(:,1)= tv(:,1)./mag; 
tv(:,2)= tv(:,2)./mag;
nv = [tv(:,2),-tv(:,1)];                                          % 2D Normal vectors pointing into the fluid
S.n3D(Nv:N,:)=S.n3D(Nv-1:-1:1,:); S.n3D(Nv:N,2)=-S.n3D(Nv:N,2);   % Reflect the 3D normal vectors for the reflection
%
% If this is a catamaran, move the hull to the half-beam and reflect it to
% create the other hull.
%
switch CatFlag
    case 'yes'
        Nv=2*Nv-1; 
        xv(:,1)=xv(:,1)+S.Bcat/2; xc(:,1)=xc(:,1)+S.Bcat/2;
        xv(Nv+1:2*Nv,1)=xv(1:Nv,1)-S.Bcat; xc(N+1:2*N,1)=xc(1:N,1)-S.Bcat;
        xv(Nv+1:2*Nv,2)=xv(1:Nv,2); xc(N+1:2*N,2)=xc(1:N,2);
        tv(N+1:2*N,:)=tv(1:N,:); nv(N+1:2*N,:)=nv(1:N,:);
        S.n3D(N+1:2*N,:)=S.n3D(1:N,:); ds(N+1:2*N,:)=ds(1:N,:);
        beam=2*beam; area=2*area; areaND=2*areaND;
end
%

S.xv = xv;
S.b  = b;
S.ds = ds; 
S.nv = nv; 
S.xc = xc;
S.beam  = beam;
S.draft = draft;
S.area  = area;
S.areaND  = areaND;
S.xCoor = xCoor;

if WithPlot
    figure
    plot(Y, Z, 'b-s', 'LineWidth', 1.5);
    axis equal
end

end