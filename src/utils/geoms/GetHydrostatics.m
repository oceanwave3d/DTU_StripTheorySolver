function [C,Vol,Xb,Zb] = GetHydrostatics(S, Zg)

% NOTE : This routine calculates the hydrostatic matrix
%        for a freely floating body with port-starboard 
%        symmetry, both w.r.t geometry and load distribution.
%        We assume that the XCG is at the same location as the XCB, to 
%        ensure static equilibrium. 
%        The input geometry is assumed to be non-dimensional with respect 
%        to a length L, and the hydrostatic matrix is then non-dimensional
%        with respect to L, rho and g. The body volume, and the x- and
%        z-coordinates of the CG are also returned. 

nst   = length(S);
Beams = zeros(nst,1);
xCoor = zeros(nst,1);
Sa = zeros(nst,1);
zcb = zeros(nst,1);
C     = zeros(6,6);

for i=1:nst
    if S(i).Nv==0
        Beams(i)=0; Sa(i)=0;  % Dummy stations have zero beam and area
        zcb(i)=0; 
    else
    Beams(i) = 2*max(S(i).xv(1,1)); % Waterline beam of this section
    Sa(i)=S(i).areaND;              % Sectional area
    % Vertical centroid of the area. 
    zcb(i)=0;
    for j=1:S(i).Nv-1
        zcb(i)=zcb(i)+S(i).nv(j,2)*S(i).xc(j,2)^2*S(i).ds(j);
    end
    zcb(i)=zcb(i)/S(i).areaND;
    end
    xCoor(i) = S(i).xCoor;
end

Vol=trapz(xCoor,Sa);
Xb=1/Vol*trapz(xCoor,xCoor.*Sa); 
Zb=-1/Vol*trapz(xCoor,zcb.*Sa);

C(3,3) =  trapz(xCoor, Beams);
C(4,4) =  trapz(xCoor, Beams.^3/12) + Vol*(Zb-Zg);
C(3,5) = -trapz(xCoor, xCoor.*Beams);
C(5,3) = C(3,5);
C(5,5) =  trapz(xCoor, xCoor.^2.*Beams) + Vol*(Zb-Zg);

end