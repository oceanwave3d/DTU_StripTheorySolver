function [C,Vol,Xb,Zb] = GetHydrostaticsCat(S, Zg)

% NOTE : This routine calculates the hydrostatic matrix
%        for a freely floating body with port-starboard 
%        symmetry, both w.r.t geometry and load distribution.
%        We assume that the XCG is at the same location as the XCB, to 
%        ensure static equilibrium. 
%        The input geometry is assumed to be non-dimensional with respect 
%        to a length L, and the hydrostatic matrix is then non-dimensional
%        with respect to L, rho and g. The body volume, and the x- and
%        z-coordinates of the CG are also returned. 
%        This version is for a catamaran hull and we assume that each hull
%        is also port-starboard symmetric, and that the two hulls are
%        identical. 

nst   = length(S);
Beams = zeros(nst,1);
xCoor = zeros(nst,1);
Sa = zeros(nst,1);
zcb = zeros(nst,1);
C     = zeros(6,6);

Bcat=S(2).Bcat;

for i=1:nst
    if S(i).Nv==0
        Beams(i)=0; Sa(i)=0;  % Dummy stations have zero beam and area
        zcb(i)=0; 
    else
    Beams(i) = 2*(S(i).xv(1,1)-Bcat/2); % Waterline beam of this single hull section
    Sa(i)=S(i).areaND/2;              % Sectional area of each single hull station
    % Vertical centroid of the area. 
    zcb(i)=0;
    for j=1:S(i).Nv-1
        zcb(i)=zcb(i)+S(i).nv(j,2)*S(i).xc(j,2)^2*S(i).ds(j);
    end
    zcb(i)=zcb(i)/Sa(i);
    end
    xCoor(i) = S(i).xCoor;
end

Vol=2*trapz(xCoor,Sa);
Xb=2/Vol*trapz(xCoor,xCoor.*Sa); 
Zb=-2/Vol*trapz(xCoor,zcb.*Sa);

C(3,3) =  2*trapz(xCoor, Beams);
C(4,4) =  trapz(xCoor, (Beams.^3+3*Beams*Bcat^2)/6) + Vol*(Zb-Zg);
C(3,5) = -2*trapz(xCoor, xCoor.*Beams);
C(5,3) = C(3,5);
C(5,5) =  2*trapz(xCoor, xCoor.^2.*Beams) + Vol*(Zb-Zg);

end