function S=Compute3DNormals(S)
%%
%
% This function reads in a set of station coordinates defining the hull and
% finds the 3D normal vector to each panel centroid. We require that all
% stations have the same number of panels in order to define tangent 
% along the hull fore-and-aft.
%
% At each body panel, we define a horizontal tangent vector t1 by central
% differences between the panel centroids of the stations ahead and behind 
% the current one. The vertical tangent vector along the section, t2, is
% defined by central differences between the panel vertices. The cross
% product of these two tangent vectors gives the normal vector. 
%
% 
% Written by H. B. Bingham, July, 2020
%

nst=length(S);

Nv=S(2).Nv; iderr=find(vertcat(S(3:nst-1).Nv)~=Nv);
if exist(iderr)
    error('Cannot compute the 3D normal vectors unless your stations all have the same number of panels.');
end
%
% Go through the stations and compute the 3D normal vectors
%
% The stern station
i=1;
if S(i).Nv~=0  % For a real stern station, compute the normals
    xv=[S(i+1).x;[-S(i+1).x(Nv-1:-1:1,1),S(i+1).x(Nv-1:-1:1,2)]];
    xc2=xv(1:Nv-1,:)+(xv(2:Nv,:)-xv(1:Nv-1,:))/2;
    xv=[S(i).x;[-S(i).x(Nv-1:-1:1,1),S(i).x(Nv-1:-1:1,2)]];
    xc1=xv(1:Nv-1,:)+(xv(2:Nv,:)-xv(1:Nv-1,:))/2;
    S(i).xc=xc1;                                       % Save the centroids
    tv1=[(S(i+1).xst-S(i).xst)*ones(Nv-1,1),xc2-xc1];  % Horizontal tangent vector
    tv2=[zeros(Nv-1,1),xv(2:Nv,:)-xv(1:Nv-1,:);];      % Vertical tangent vector
    nv=-[(tv1(:,2).*tv2(:,3)-tv1(:,3).*tv2(:,2)),(-tv1(:,1).*tv2(:,3)),tv1(:,1).*tv2(:,2)];
    mag=sqrt(nv(:,1).^2+nv(:,2).^2+nv(:,3).^2);
    nv=nv./mag;
    S(i).n3D=nv;
else
    % for a dummy station, set them to a straight line
    S(i).n3D=zeros(Nv-1,3); S(i).n3D(:,2)=1;
    xv=[S(i).x;[-S(i).x(Nv-1:-1:1,1),S(i).x(Nv-1:-1:1,2)]];
    xc1=xv(1:Nv-1,:)+(xv(2:Nv,:)-xv(1:Nv-1,:))/2;
    S(i).xc=xc1;                                       % Save the centroids
end
%
% The bow station should be a dummy station
%
i=nst; 
if S(i).Nv~=0  % Print a warning if the last station is not a dummy station
    warning('Your last station is not a bow line, which may lead to errors in the evaluation of integrated quantities.');
    % compute 2D normals if this is not a dummy station
    % The previous station's coordinates
    xv=[S(i-1).x;[-S(i-1).x(Nv-1:-1:1,1),S(i-1).x(Nv-1:-1:1,2)]];
    xc1=xv(1:Nv-1,:)+(xv(2:Nv,:)-xv(1:Nv-1,:))/2;
    % This station's coordinates
    xv=[S(i).x;[-S(i).x(Nv-1:-1:1,1),S(i).x(Nv-1:-1:1,2)]];
    xc0=xv(1:Nv-1,:)+(xv(2:Nv,:)-xv(1:Nv-1,:))/2;
    S(i).xc=xc0;                                       % Save the centroids
    tv1=[(S(i).xst-S(i-1).xst)*ones(Nv-1,1),xc2-xc1];  % Horizontal tangent vector
    tv2=[zeros(Nv-1,1),xv(2:Nv,:)-xv(1:Nv-1,:);];      % Vertical tangent vector
    nv=-[(tv1(:,2).*tv2(:,3)-tv1(:,3).*tv2(:,2)),(-tv1(:,1).*tv2(:,3)),tv1(:,1).*tv2(:,2)];
    mag=sqrt(nv(:,1).^2+nv(:,2).^2+nv(:,3).^2);
    nv=nv./mag;
    S(i).n3D=nv;
else
    S(i).n3D=zeros(Nv-1,3); S(i).n3D(:,2)=1;
    xv=[S(i).x;[-S(i).x(Nv-1:-1:1,1),S(i).x(Nv-1:-1:1,2)]];
    xc1=xv(1:Nv-1,:)+(xv(2:Nv,:)-xv(1:Nv-1,:))/2;
    S(i).xc=xc1;                                       % Save the centroids
end
%
% Run through the rest of the stations
%
for i=2:nst-1
    % The next station's coordinates
    xv=[S(i+1).x;[-S(i+1).x(Nv-1:-1:1,1),S(i+1).x(Nv-1:-1:1,2)]];
    xc2=xv(1:Nv-1,:)+(xv(2:Nv,:)-xv(1:Nv-1,:))/2;
    % The previous station's coordinates
    xv=[S(i-1).x;[-S(i-1).x(Nv-1:-1:1,1),S(i-1).x(Nv-1:-1:1,2)]];
    xc1=xv(1:Nv-1,:)+(xv(2:Nv,:)-xv(1:Nv-1,:))/2;
    % This station's coordinates
    xv=[S(i).x;[-S(i).x(Nv-1:-1:1,1),S(i).x(Nv-1:-1:1,2)]];
    xc0=xv(1:Nv-1,:)+(xv(2:Nv,:)-xv(1:Nv-1,:))/2;
    S(i).xc=xc0;                                       % Save the centroids
    tv1=[(S(i+1).xst-S(i-1).xst)*ones(Nv-1,1),xc2-xc1];  % Horizontal tangent vector
    tv2=[zeros(Nv-1,1),xv(2:Nv,:)-xv(1:Nv-1,:);];      % Vertical tangent vector
    nv=-[(tv1(:,2).*tv2(:,3)-tv1(:,3).*tv2(:,2)),(-tv1(:,1).*tv2(:,3)),tv1(:,1).*tv2(:,2)];
    mag=sqrt(nv(:,1).^2+nv(:,2).^2+nv(:,3).^2);
    nv=nv./mag;
    S(i).n3D=nv;
end
