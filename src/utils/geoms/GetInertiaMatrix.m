function Mjk = GetInertiaMatrix(Vol, Xg, Zg, RGyr)
%
% Build the non-dimensional inertia matrix for the input parameters of body
% volume, x- and z- positions of the CG and the 3 x 3 radius of gyration
% matrix. 
%

Mjk      = zeros(6);
Mjk(1,1) = Vol;
Mjk(2,2) = Mjk(1,1);
Mjk(3,3) = Mjk(1,1);
Mjk(3,5) = -Xg*Vol;
Mjk(5,3) = Mjk(3,5);
Mjk(1,5) = Vol*Zg;
Mjk(5,1) = Mjk(1,5);
Mjk(2,4) =-Vol*Zg;
Mjk(4,2) = Mjk(2,4);
Mjk(5,1) = Mjk(1,5);
Mjk(4:6,4:6) = Vol*RGyr.^2;
