function [beta,T,XMag,XPhase,XR,XI]=ReadWAMIT234file(fname,nHead,Order)
%
% A function to read a WAMIT .2, .3 or .4 file with exciting force/RAO data 
% and sort it into arrays ordered by frequency, mode and heading angle.  
% The returned arrays always have at least 6 modes, so that modes 1-6 
% correspond to the rigid-body modes.  Depending on Order, the arrays are
% returned with period first or mode first.  Heading angle always comes
% last.  
%
fin=fopen(fname);
%
for i=1:nHead, buffer = fgetl(fin); end;  % skip the header line(s)
tmp=fscanf(fin,'%f'); nd=length(tmp); 
fclose(fin);
beta=unique(tmp(2:7:end),'stable'); nbeta=length(beta); 
modes=unique(tmp(3:7:end),'stable'); nmds=length(modes); 
display('Found modes: ');
modes'
display(' in the file. Note that negative modes will be converted to positive.');
modes=abs(modes);
stride=nbeta*nmds*7;
nf=nd/stride; 
%
T=tmp(1:stride:end);
%
nmodes=max(6,max(modes));
%
switch Order
    case 'TM'
        XMag=zeros(nf,max(6,nmodes),nbeta); XPhase=XMag;
        XR=XMag; XI=XMag;
        %
        for ib=1:nbeta
            for im=1:nmds
                i0=(ib-1)*nmds*7+(im-1)*7+4;
                XMag(:,modes(im),ib)=tmp(i0:stride:end);
                XPhase(:,modes(im),ib)=tmp(i0+1:stride:end);
                XR(:,modes(im),ib)=tmp(i0+2:stride:end);
                XI(:,modes(im),ib)=tmp(i0+3:stride:end);
            end
        end
    case 'MT'
        XMag=zeros(max(6,nmodes),nf,nbeta); XPhase=XMag;
        XR=XMag; XI=XMag;
        %
        for ib=1:nbeta
            for im=1:nmds
                i0=(ib-1)*nmds*7+(im-1)*7+4;
                XMag(modes(im),:,ib)=tmp(i0:stride:end);
                XPhase(modes(im),:,ib)=tmp(i0+1:stride:end);
                XR(modes(im),:,ib)=tmp(i0+2:stride:end);
                XI(modes(im),:,ib)=tmp(i0+3:stride:end);
            end
        end
    otherwise
        error('Choose between "TMB" and "MTB" for the ordering.');
end