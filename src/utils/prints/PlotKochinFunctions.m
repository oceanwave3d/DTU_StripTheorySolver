function [] = PlotKochinFunctions(Sol, index)

if ~isempty(Sol(1).Kochin1{index})
    
    HHeave1    = Sol(1).Kochin1{index}.Heave;
    HPitch1    = Sol(1).Kochin1{index}.Pitch;
    HScatr1    = Sol(1).Kochin1{index}.Scatr;
    mvec1      = Sol(1).Kochin1{index}.mvec;
end

if ~isempty(Sol(1).Kochin2{index})
    
    HHeave2    = Sol(1).Kochin2{index}.Heave;
    HPitch2    = Sol(1).Kochin2{index}.Pitch;
    HScatr2    = Sol(1).Kochin2{index}.Scatr;
    mvec2      = Sol(1).Kochin2{index}.mvec;
end

if ~isempty(Sol(1).Kochin3{index})
    HHeave3    = Sol(1).Kochin3{index}.Heave;
    HPitch3    = Sol(1).Kochin3{index}.Pitch;
    HScatr3    = Sol(1).Kochin3{index}.Scatr;
    mvec3      = Sol(1).Kochin3{index}.mvec;
end

if exist('HHeave1', 'var')
    HTotal1 = abs(HHeave1 + HPitch1 + HScatr1);
end
if exist('HHeave2', 'var')
    HTotal2 = abs(HHeave2 + HPitch2 + HScatr2);
end
if exist('HHeave3', 'var')
    HTotal3 = abs(HHeave3 + HPitch3 + HScatr3);
end

figure(2)
subplot(4,1, 1)
if exist('HHeave1', 'var')
    plot(mvec1, abs(HHeave1), 'r.-', 'LineWidth', 1,'MarkerSize', 5);
end
hold on
if exist('HHeave2', 'var')
    plot(mvec2, abs(HHeave2), 'b.-', 'LineWidth', 1,'MarkerSize', 5);
end
hold on
if exist('HHeave3', 'var')
    plot(mvec3, abs(HHeave3), 'k.-', 'LineWidth', 1,'MarkerSize', 5);
end

subplot(4,1, 2)
if exist('HHeave1', 'var')
    plot(mvec1, abs(HPitch1), 'r.-', 'LineWidth', 1,'MarkerSize', 5);
end
hold on
if exist('HHeave2', 'var')
    plot(mvec2, abs(HPitch2), 'b.-', 'LineWidth', 1,'MarkerSize', 5);
end
hold on
if exist('HHeave3', 'var')
    plot(mvec3, abs(HPitch3), 'k.-', 'LineWidth', 1,'MarkerSize', 5);
end

subplot(4,1, 3)
if exist('HHeave1', 'var')
    plot(mvec1, abs(HScatr1), 'r.-', 'LineWidth', 1,'MarkerSize', 5);
end
hold on
if exist('HHeave2', 'var')
    plot(mvec2, abs(HScatr2), 'b.-', 'LineWidth', 1,'MarkerSize', 5);
end
hold on
if exist('HHeave3', 'var')
    plot(mvec3, abs(HScatr3), 'k.-', 'LineWidth', 1,'MarkerSize', 5);
end

subplot(4,1, 4)
if exist('HHeave1', 'var')
    plot(mvec1, abs(HTotal1), 'r.-', 'LineWidth', 1,'MarkerSize', 5);
end
hold on
if exist('HHeave2', 'var')
    plot(mvec2, abs(HTotal2), 'b.-', 'LineWidth', 1,'MarkerSize', 5);
end
hold on
if exist('HHeave3', 'var')
    plot(mvec3, abs(HTotal3), 'k.-', 'LineWidth', 1,'MarkerSize', 5);
end

end

