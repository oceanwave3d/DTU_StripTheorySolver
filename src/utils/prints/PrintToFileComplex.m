function [] = PrintToFileComplex(x, y, path)

Nx = size(x);
Ny = size(y);

if Nx(1)~=1
    x = x.';
end
if Ny(1)~=1
    y = y.';
end

out = [x; real(y); imag(y); abs(y)];
fid = fopen(path,'w');
fprintf(fid, '%15.8e %15.8e %15.8e %15.8e \n', out);

end