function [] = PrintToFile(x, y, path)

Nx = size(x);
Ny = size(y);

if Nx(1)~=1 && Nx(2)==1
    x = x.';
end
if Ny(1)~=1 && Ny(2)==1
    y = y.';
end

out = [x; y];
fid = fopen(path,'w');
fprintf(fid, '%15.8e %15.8e \n', out);

end