function [beta1,beta2,T,XMag,XPhase,XR,XI]=ReadWAMIT89file(fname,nHead,Order)
%
% A function to read a WAMIT .8 or .9 file with mean drift force data 
% and sort it into arrays ordered by frequency, mode and heading angles.  
% The returned arrays always have at least 6 modes, so that modes 1-6 
% correspond to the rigid-body modes.  Depending on Order, the arrays are
% returned with period first or mode first.  Heading angle always comes
% last.  
%
fin=fopen(fname);
%
for i=1:nHead, buffer = fgetl(fin); end;  % skip the header line(s)
tmp=fscanf(fin,'%f'); nd=length(tmp); nc=8;
fclose(fin);
beta1=unique(tmp(2:nc:end),'stable'); nbeta1=length(beta1); 
beta2=unique(tmp(3:nc:end),'stable'); nbeta2=length(beta2); 
modes=unique(tmp(4:nc:end),'stable'); nmds=length(modes); 
display('Found modes: ');
modes
display(' in the file.'); 
modes=abs(modes);
stride=nbeta1*nbeta2*nmds*nc;
nf=nd/stride; 
%
T=tmp(1:stride:end);
%
nmodes=max(6,max(modes));
%
switch Order
    case 'TM'
        XMag=zeros(nf,max(6,nmodes),nbeta1,nbeta2); XPhase=XMag;
        XR=XMag; XI=XMag;
        %
        for ib1=1:nbeta1
            for ib2=1:nbeta2
                for im=1:nmds
                    i0=(ib1-1)*(ib2-1)*nmds*nc+(im-1)*nc+5;
                    XMag(:,modes(im),ib1,ib2)=tmp(i0:stride:end);
                    XPhase(:,modes(im),ib1,ib2)=tmp(i0+1:stride:end);
                    XR(:,modes(im),ib1,ib2)=tmp(i0+2:stride:end);
                    XI(:,modes(im),ib1,ib2)=tmp(i0+3:stride:end);
                end
            end
        end
    case 'MT'
        XMag=zeros(max(6,nmodes),nf,nbeta1,nbeta2); XPhase=XMag;
        XR=XMag; XI=XMag;
        %
        for ib1=1:nbeta1
            for ib2=1:nbeta2
                for im=1:nmds
                    i0=(ib1-1)*(ib2-1)*nmds*nc+(im-1)*nc+5;
                    XMag(modes(im),:,ib1,ib2)=tmp(i0:stride:end);
                    XPhase(modes(im),:,ib1,ib2)=tmp(i0+1:stride:end);
                    XR(modes(im),:,ib1,ib2)=tmp(i0+2:stride:end);
                    XI(modes(im),:,ib1,ib2)=tmp(i0+3:stride:end);
                end
            end
        end
    otherwise
        error('Choose between "TM" and "MT" for the ordering.');
end