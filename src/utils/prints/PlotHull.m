function [] = PlotHull(S)

S = S(vertcat(S.Nv)>0); % Remove the dummy stations

Nv = length(S(2).x);
Np=2*(Nv-1);
nst  = length(S); %L=S(nst).xst-S(1).xst;
for t =1:nst
    S(t).xfp = ones(Nv,1) * S(t).xst;
    S(t).xfc = ones(Np/2,1) * S(t).xst;
end
%
% Fix the bow and stern dummy stations if neccessary
%
% if length(S(1).x) ~= Nv
%     S(1).xv=S(2).x; S(1).x(:,1)=0;
% end
% if length(S(nst).x) ~= Nv
%     S(nst).x=S(nst-1).x; S(nst).x(:,1)=0;
% end
% if length(S(1).xc) ~=Np
%     S(1).n3D(Np/2+1:Np,:)=S(1).n3D(Np/2:-1:1,:);
%     S(1).n3D(Np/2+1:Np,2)=-S(1).n3D(Np/2+1:Np,2);
%     S(1).xc(Np/2+1:Np,:)=S(1).xc(Np/2:-1:1,:);
%     S(1).xfc(Np/2+1:Np,:)=S(1).xfc(Np/2:-1:1,:);
%     S(1).xc=S(1).xc/L;
%     S(1).xfc=S(1).xfc/L;
% end
% if length(S(nst).xc) ~=Np
%     S(nst).n3D(Np/2+1:Np,:)=S(nst).n3D(Np/2:-1:1,:);
%     S(nst).n3D(Np/2+1:Np,2)=-S(nst).n3D(Np/2+1:Np,2);
%     S(nst).xc(Np/2+1:Np,:)=S(nst).xc(Np/2:-1:1,:);
%     S(nst).xfc(Np/2+1:Np,:)=S(nst).xfc(Np/2:-1:1,:);
%     S(nst).xc=S(nst).xc/L;
%     S(nst).xfc=0*S(nst).xfc/L;
% end
%
% If this is a catamaran, move the dummy sections from the input hull to
% the half-beam and reflect.
%
% switch S(2).CatFlag
%     case 'yes'
%         for i=[1,nst]
%             if S(i).Nv==0
%                 S(i).xc(:,1)=S(i).xc(:,1)+S(i).Bcat/2;
%                 S(i).xc(Np+1:2*Np,1)=S(i).xc(1:Np,1)-S(i).Bcat;
%                 S(i).xc(Np+1:2*Np,2)=S(i).xc(1:Np,2);
%                 S(i).n3D(Np+1:2*Np,:)=S(i).n3D(1:Np,:);
%             end
%         end
%         Np=2*Np;
%         for i=1:nst
%             S(i).xfc = ones(Np,1) * S(i).xst;
%         end
% end

xfp  = vertcat(S.xfp);
yzfp = vertcat(S.x);
yfp  = yzfp(:,1);
zfp  = yzfp(:,2);


xc=vertcat(S.xfc);
yc=vertcat(S.xc);
n3D=vertcat(S.n3D);


figure(100); clf
subplot(2,3, [1,3])
plot3(xfp, yfp, -zfp, 'k.', 'MarkerSize', 4); axis equal;
hold on
plot3(xfp,-yfp, -zfp, 'k.', 'MarkerSize', 4); axis equal;
%
subplot(2,3, 4)
plot3(xfp, yfp, -zfp, 'k.', 'MarkerSize', 4); axis equal;
hold on
plot3(xfp,-yfp, -zfp, 'k.', 'MarkerSize', 4); axis equal;
view(90,0)
%
subplot(2,3, 5)
plot3(xfp, yfp, -zfp, 'k.', 'MarkerSize', 4); axis equal;
hold on
plot3(xfp,-yfp, -zfp, 'k.', 'MarkerSize', 4); axis equal;
view(0,0)
%
subplot(2,3, 6)
plot3(xfp, yfp, -zfp, 'k.', 'MarkerSize', 4); axis equal;
hold on
plot3(xfp,-yfp, -zfp, 'k.', 'MarkerSize', 4); axis equal;
view(0,90)

figure(101); clf
quiver3(xc, yc(:,1), yc(:,2), n3D(:,1), n3D(:,2),n3D(:,3), 'MarkerSize', 4); axis equal;
hold on
set ( gca, 'zdir', 'reverse' )

figure(102); clf; hold on;
for j=1:nst
    plot3(S(j).xfp,S(j).x(:,1),-S(j).x(:,2),'k'); axis equal
    plot3(S(j).xfp,-S(j).x(:,1),-S(j).x(:,2),'k'); axis equal
end
view(-26,20)
%


end

