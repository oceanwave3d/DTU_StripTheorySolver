# DTU_StripTheorySolver

 <div align="justify">

This project is a Matlab implementation of strip theory following: ***Salvesen, Tuck & Faltinsen (1970) "Ship Motions and Sea Loads" SNAME.*** The sectional 2D solutions are computed using a constant-strength panel method and exact integration of the free-surface Green function following the lecture note: ***Wave-Body Interaction Theory (Theory of Ship Waves)" by Masashi Kashiwagi, Osaka University, Japan, (2019).***
Two methods for computing the added resistance are implemented. The first is based on the Kochin function, and the second is according to an improved version of Salvesen's method. For further details see the Publications section at the bottom of this page. The solver has been written by ***Harry B. Bingham and Mostafa Amini-Afshar, Dept. of Civil and Mechanical Engineering, Technical University of Denmark.***

<br/>

If you find this package useful, please acknowledge this work by citing the package as:   

***H. B. Bingham*** and ***M. Amini-Afshar (2020)***  ***“DTU_StripTheorySolver”  https://gitlab.gbar.dtu.dk/oceanwave3d/DTU_StripTheorySolver***

If relevant, please also cite one or more of the publications shown below. 

See the file LICENCE for the license. A number of examples can be found in the directory "Examples".

</div>

# ![](./devt/demores.png)


## Publications

**Journal papers**

[2] Amini-Afshar, M. and H. B. Bingham. ***Added resistance using Salvesen–Tuck–Faltinsen strip theory and the Kochin function.*** *Applied Ocean Research, 106:102481, 2021. ISSN 0141-1187. https://doi.org/10.1016/j.apor.2020.102481*

[1] Amini-Afshar, M. ***Salvesen’s method for added resistance revisited***. *ASME. J. Offshore Mech. Arct. Eng. October 2021; 143(5): 051902. https://doi.org/10.1115/1.4050213*

**Conference papers**

[3] Amini-Afshar, M., Bingham, H.B.,  and  Kashiwagi, M. ***On a New Formulation for Wave Added Resistance.***
38th International Workshop on Water Waves and Floating Bodies, University of Michigan, Ann Arbor, Michigan, USA, May 7-10, 2023.

[2] Amini-Afshar, M. and H. B. Bingham. ***The Challenges of Computing Wave Added Resistance Using Maruo’s Formulation and The Kochin Function***. *In 37th International Workshop on Water Waves and Floating Bodies, Giardini Naxos, Italy (Sicily), 2022*.

[1] Amini-Afshar, M. and H. B. Bingham. ***Improvements in the Computation of Added Wave Resistance Using Classical Strip Theory***. *In World Maritime Technology Conference (WMTC), Denmark, Copenhagen, 2022*

**Theses**

[1] Gohn, Thomas. ***Enhanced Unified Theory for improved estimation of Added Wave Resistance***. Master's thesis, Technical University of Denmark, Department of Civil and Mechanical Engineering, 2024.