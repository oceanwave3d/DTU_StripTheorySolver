function [s,e,scheck,echeck]=findNonShadow(theta,origin,beta,nSegments,btolBow,btolStern)

scheck=false;
echeck=false;
s=0;e=0;

if beta>abs(theta(origin-1)) % last tangent at bow is less than the heading
    s=origin;
    scheck=true;
end
if beta>abs(pi-theta(0.5*nSegments+origin-1)) % last tangent at stern is less than the heading
    e=0.5*nSegments+origin-1;
    echeck=true;
end

for i=1:nSegments
    if (theta(i)>=-beta-btolBow && theta(i)<=-beta+btolBow && scheck==false)
        s=i;
        scheck=true;
    end
    if(scheck==true && i~=s && theta(i)>=pi-beta-btolStern && theta(i)<=pi-beta+btolStern && i>origin && echeck==false)
        e=i;
        echeck=true;
        break
    end
    if scheck==true && i==0.5*nSegments+origin-2
        echeck=true;
        e=i+1;
    end
end

end