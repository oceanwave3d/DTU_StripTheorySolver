function [x,y,Length,Breadth]=getShipGeometry(inputFile)

fileI=fopen(inputFile);
Ci=textscan(fileI,'%s %s %f',6);

Length=Ci{3}(1);
Breadth=Ci{3}(2);
designDraft=Ci{3}(3);
lbp=Ci{3}(4);
cog=Ci{3}(5);
ico=Ci{3}(6);

C=textscan(fileI,'%s %s %s');
sectionFile=C{3};
fclose(fileI);

fileS=fopen(sectionFile{:});
CS=textscan(fileS,'%f %f %f');


wtlt=0.05; % tolerance to capture waterline
j=0;
for i=1:length(CS{1})
    if (CS{3}(i)>=designDraft-wtlt && CS{3}(i)<=designDraft+wtlt)
        j=j+1;
        x(j)=-(CS{1}(i)-(lbp/2+cog+ico)); %#ok<*AGROW,*SAGROW>
        y(j)=CS{2}(i);
        %z(j)=CS{3}(i);
    end
end

% local interpolation at stern and bow
nx=length(x);
tol_inter=0.001;

for i=1:nx-1
    dx=x(i+1)-x(i);
    dy=y(i+1)-y(i);
    if(atan(dy/dx)<=tol_inter && atan(dy/dx)>=-tol_inter)
        bow=i;
        break
    end
end

for i=nx:-1:2
    dx=x(i-1)-x(i);
    dy=y(i-1)-y(i);
    if(atan(dy/dx)<=tol_inter && atan(dy/dx)>=-tol_inter)
        stern=i;
        break
    end
end

resolution=5000;
pb=polyfit(x(1:bow),y(1:bow),3);
r=roots(pb);
ps=polyfit(x(stern:end-1),y(stern:end-1),3);

ixb=linspace(r(1),x(bow),resolution);
ixs=linspace(x(stern),x(end-1),resolution);
iyb=polyval(pb,ixb);
iys=polyval(ps,ixs);

subplot(4,1,1)
plot(x,y,'-o','LineWidth',1);
hold on
plot(ixb,iyb,'r','LineWidth',2);
plot(ixs,iys,'m','LineWidth',2);
grid on
axis equal;
h1=legend('The imported data points','3rd order interpolation at the bow','3rd order interpolation at the stern','Location','South');
lg1 = findobj(h1,'type','text');
set(lg1,'FontSize',12)
% 
x=[ixb,x(bow+1:stern-1),ixs,x(end)];
y=[iyb,y(bow+1:stern-1),iys,y(end)];
limx=0.1*(max(x)-min(x));
xlim([min(x)-limx max(x)+limx]);
end