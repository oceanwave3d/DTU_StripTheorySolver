function [x,y,r,l]=getCircleRectangleGeometry()

r=input('enter radius: ');
n=input('enter multiple: ');
npc=input('enter resolution for circle part (should be even): ');

l=n*r; % half length
% npr=ceil(2*l*npc/(0.5*pi*r)); % resolution for the half length
% 
alphal=pi/2*linspace(1,2,npc);
alphar=pi/2*linspace(0,1,npc);

% un-comment for parabolic geometry
% tl=linspace(-1,0,npc);
% xcl=tl;
% ycl=-(tl/1).^2+r;

xcl=r*cos(alphal);
ycl=r*sin(alphal);
xcl=xcl(end:-1:1);    % starting from negative
ycl=ycl(end:-1:1);    % starting from negative
xcl=xcl-l;            % left circle

xr=linspace(-l,l,npc); % recangle
yr=r*ones(1,npc);

% un-comment for parabolic geometry
% tr=linspace(0,1,npc);
% xcr=tr;
% ycr=-(tr/1).^2+r;

xcr=r*cos(alphar);
ycr=r*sin(alphar);
xcr=xcr(end:-1:1);    % starting from negative
ycr=ycr(end:-1:1);    % starting from negative
xcr=xcr+l;            % right circle

x=[xcl,xr(2:end),xcr(2:end)];
y=[ycl,yr(2:end),ycr(2:end)];
end