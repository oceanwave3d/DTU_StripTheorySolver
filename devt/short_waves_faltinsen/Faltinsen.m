%%
%This script calculates the added resistance for
% the high frequency wave ranges using Faltinsen's method.
% Faltinsen, et.al (1981), Prediction of resistance and propulsion of a ship in a seaway.
% The required geometry is the upper half of the waterline at the design
% draft
%%
clc;
clearvars;
close all;
set(0, 'defaulttextinterpreter','latex')
set(0, 'defaultAxesTickLabelInterpreter','latex');
set(0, 'defaultLegendInterpreter','latex');
%% Input data
job=input('job type (ship, circle, circleRectangle): ');
gamma=input('heading (0: head wave, 90: beam wave): ');
u=input('forward speed knot (zero for circle and circleRectangle) :');
beta=gamma*pi/180;
U=u*0.515;
g=9.81;
rho=1025; % kg/m^3
%% ship geometry
switch job
    case 'ship'
        inputFile=input('input file : ');
        [x,y,shipLength,shipBreadth]=getShipGeometry(inputFile);
end
%% circle geometry
switch job
    case 'circle'
        [x,y,r]=getCircleGeometry();
        
        subplot(3,1,1)
        plot(x,y,'-o','LineWidth',2);
        h1=legend('The imported data points');
        lg1 = findobj(h1,'type','text');
        set(lg1,'FontSize',12)
        grid on
        axis equal;
end
%% circle rectangle geometry
switch job
    case 'circleRectangle'
        [x,y,r,l]=getCircleRectangleGeometry();
        subplot(3,1,1)
        plot(x,y,'.-','LineWidth',1);
        h1=legend('The imported data points');
        lg1 = findobj(h1,'type','text');
        set(lg1,'FontSize',12)
        grid on
        axis equal;
end
%% re-arrange the final vectors
np=length(x)-1;
dt=0.00001;
% find origin
for i=1:np-1
    dx=x(i+1)-x(i);
    dy=y(i+1)-y(i);
    if(atan(dy/dx)<=dt && atan(dy/dx)>=-dt)
        origin=i+1;
        break
    end
end
xs1=x(1:origin);xs1=xs1(end:-1:1);
ys1=y(1:origin);ys1=ys1(end:-1:1);
xs2=x(2:end);
ys2=-y(2:end);
px=x(end:-1:1);py=y(end:-1:1);
xs3=px(2:length(x)+1-origin);
ys3=py(2:length(x)+1-origin);
X=[xs1,xs2,xs3];
Y=[ys1,ys2,ys3];
%% plot the final vectors
subplot(4,1,2) 
plot(X,Y,'.-k');
hold on
% show origin
plot(X(1),Y(1),'om','MarkerSize',4,'LineWidth',4);
axis equal;
grid on;
h1=legend('The arranged coordinates');
lg1 = findobj(h1,'type','text');
set(lg1,'FontSize',12)
limx=0.1*(max(X)-min(X));
limy=0.1*(max(Y)-min(Y));
ylim([min(Y)-limy max(Y)+limy]);
axis equal;
%% find theta (local tangent)
nSegments=length(X)-1;
for i=1:nSegments
    dX(i)=X(i+1)-X(i);
    dY(i)=Y(i+1)-Y(i);
    theta(i)=atan(dY(i)/dX(i)); %#ok<*SAGROW>
    if (dX(i)<0)
        theta(i)=-theta(i);
    end
    if (dX(i)>0)
        theta(i)=pi-theta(i);
    end
end
dL=sqrt(dX.^2+dY.^2);
%% find the shadow region
scheck=false;echeck=false;
while scheck==false || echeck==false
    btolBow=input('Bow Non-shadow region tolerance 0.0001-0.01: ');
    btolStern=input('Stern Non-shadow region tolerance 0.0001-0.01: ');
    [s,e,scheck,echeck]=findNonShadow(theta,origin,beta,nSegments,btolBow,btolStern);
end
fprintf('startTheta  = %f\n',theta(s)*180/pi);
fprintf('finishTheta = %f\n',180-theta(e)*180/pi);
% plot shadow region
subplot(4,1,3)
plot(X(s:e+1),Y(s:e+1),'m','LineWidth',5)
hold on
plot(X,Y,'-k','LineWidth',2);
plot([X(end) X(1)],[Y(end) Y(1)],'-k','LineWidth',2)
axis equal;
grid on
h5=legend(strcat('Non-shadow region $\beta$=',num2str(gamma),'$^\circ$'));
lg5 = findobj(h5,'type','text');
set(lg5,'FontSize',20)
ylim([min(Y)-limy max(Y)+limy]);
axis equal;
%% calculate the added resistance

w0=linspace(0.6,2,150); % angular frequency vector

for v=1:length(U)
    for i=1:length(w0)
        integrand=(sin(theta(s:e)+beta)).^2+2*w0(i)*U(v)/g*(1-cos(theta(s:e)).*cos(theta(s:e)+beta));
        Rw1(i)=-0.5*rho*g*sum((integrand.*(sin(theta(s:e)))).*dL(s:e));
        Rw2(i)=-0.5*rho*g*sum((integrand.*(cos(theta(s:e)))).*dL(s:e));
        
        if strcmp(job,'circle')
            anRw1=2/3*rho*g*r*cos(beta);
            anRw2=2/3*rho*g*r*sin(beta);
            fprintf('Analytical Rw1 =  %f\n',anRw1);
            fprintf('Numerical  Rw1 =  %f\n',Rw1);
            fprintf('Analytical Rw2 =  %f\n',anRw2);
            fprintf('Numerical  Rw2 =  %f\n',Rw2);
            break
        end
        if strcmp(job,'circleRectangle')
            anRw1=2/3*rho*g*r*cos(beta);
            anRw2=rho*g*(2/3*r*sin(beta)+l*sin(beta)*abs(sin(beta)));
            fprintf('Analytical Rw1 =  %f\n',anRw1);
            fprintf('Numerical  Rw1 =  %f\n',Rw1);
            fprintf('Analytical Rw2 =  %f\n',anRw2);
            fprintf('Numerical  Rw2 =  %f\n',Rw2);
            break
        end
    end
    if strcmp(job,'ship')
        k=w0.^2/g; % wave number in deep water
        lambda=2*pi*g./w0.^2; % wave length in deep water
        subplot(4,1,4)
        rw = Rw1*shipLength/(rho*g*shipBreadth^2);
        plot(lambda/shipLength,rw,'-.b', 'LineWidth',2)
        xlabel('$\lambda/L$')
        ylabel('$R_w L/\rho g B^2 A^2$')
        grid on
    end
end