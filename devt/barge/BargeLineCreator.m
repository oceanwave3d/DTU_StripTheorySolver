function [] = BargeLineCreator(L, B, D, N, S)

% This script creates the lines for a prismatic barge

Tl  = B/2 + D;
Nv  = ceil(D/Tl * N);
Nh  = ceil(B/(2*Tl) * N);
x   = linspace(-L/2, L/2, S);
ph  = linspace(0, B/2, Nh);
pv  = linspace(0, D  , Nv);
VH  = [ph; 0*ph];
VV  = [pv(2:end)./pv(2:end)*B/2; pv(2:end)];
V   = [VH VV];

%% Print to file

fid = fopen('./BargePoints.txt', 'w');

for i=1:S
    xv = repmat(x(i),1, length(V));
    outVec = [xv;V];
    fprintf (fid, '%15.8e %15.8e %15.8e \n', outVec);
    fprintf (fid, '\n');
end

end