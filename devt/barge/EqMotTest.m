%%
% Non-dimensionalize the mass and stiffness matrices
%
Mjk = Mjk/(rho*L^3); 
Mjk(1:3,4:6) = Mjk(1:3,4:6)/L; 
Mjk(4:6,1:3) = Mjk(4:6,1:3)/L;

Cjk=Cjk/(rho*g*L^2);
Cjk(1:3,4:6)=Cjk(1:3,4:6)/L; 
Cjk(4:6,1:3)=Cjk(4:6,1:3)/L;
for i=4:6
    Mjk(i,i)=Mjk(i,i)/L^2;
    Cjk(i,i)=Cjk(i,i)/L^2;
end
%%
% Solve the 2 DOF heave-pitch problem
%
xi=zeros(nf,6);
ifix=[1,2,4,6];
%
% Interpolate the excitation forces from absolute to encounter
% frequency and solve for the response.
%
omET=omT - cosB*Fn*nuT;  % The encounter frequencies for the diffraction problem
%
% Interpolate the TiMIT values to the STF encounter frequency-space
%
XjDi=zeros(nf,6); FjkT=zeros(nf,6,6);
for i=[3,5]
    XjDi(:,i)=interp1(omET,XjDT(:,i),om,'linear','extrap');
    for j=[3,5]
        FjkT(:,i,j)=interp1(omT,AjkT(:,i,j)-1i*BjkT(:,i,j),om,'linear','extrap');
    end
end
%
% Solve using TiMIT Fjk but STF XjD
%
for i=1:nf
    mat=(-om(i)^2*(Mjk+squeeze(FjkT(i,:,:)))+Cjk);
    mat(ifix,:)=[]; mat(:,ifix)=[];
    rhs=XjD(i,[3,5]).';
    sol=mat\rhs;
    xi2(i,[3,5])=sol.';
end
%
figure(9);clf;  
plot(lam0,abs(xi2(:,3)),lamT,abs(xiT(:,3)),lam0,abs(xi2(:,5))./(nu0),...
    lamT,abs(xiT(:,5))./(nuT))
xlabel('$\lambda/L$'); 
legend('$|\xi_{3}|/A$, STF','$|\xi_{3}|/A$, TiMIT',...
    '$|\xi_{5}|/(kA)$, STF','$|\xi_{5}|/(kA)$, TiMIT');
grid on; xlim([0,3]); 
