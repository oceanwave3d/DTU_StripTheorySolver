%
% THis script looks at the behavior of a Kochin function integral over 
% a wedge shaped section running from z=0, y=1 to z=1, y=0, then back again 
% to z=0, y=-1 to form the complete section. The potential 
% along the section is either taken to phi_B=1, or phi_B=1-z. In this case, 
% the heave sectional Kochin function can be easily integrated to give: 
%
% H(m) = sqrt(2) int_0^1 [n_2 - phi_B (i kappa n_1-K n_2)] 
%                                  exp[-(K+ikappa)z + ikappa] dz 
%      = C+/(K+ikappa) (1-exp[-(K+ikappa)])                                   for phi_B=1,
%        +C-/(K-ikappa) (1-exp[-(K-ikappa)])
%        sqrt(2) exp[ikappa] * { n2/(K+ikappa) (1-exp[-(K+ikappa)]) 
%        - (i kappa n_1-K n_2)/(K+ikappa)^2 (exp[-(K+ikappa)] -1 + K+ikappa) for phi_B=1-z,
%        +sqrt(2) exp[-ikappa] * { n2/(K-ikappa) (1-exp[-(K-ikappa)]) 
%        - (-i kappa n_1-K n_2)/(K-ikappa)^2 (exp[-(K-ikappa)] -1 + K-ikappa) 
%
% where kappa=sqrt(K^2-m^2) and 
% C+-=sqrt(2)exp[+-ikappa][n_2 - (+-i kappa n_1-K n_2)] and
% n_1=n_2=1/sqrt(2). 
%
% From this we can see that the limit of |H| as K-> infinity is actually a
% constant value, i.e. the Kochin function on the section does not go to 
% zero, but when the integral over x is performed with the factor exp(imx),
% the result is 
%
% H_3D(m) = 2/m sin(L/2 m) H(m)
%
% where L=ship length. So the 3D Kochin function does decay like 1/m. 
%
% Submerging the panels by zs=0.01 (1% of the section draft) makes H(m)
% decay. Also, scaling the potential to be phi-phi(z=0) removes
% the constant limit. That limit can be again computed analytically and
% then added back in to the rest of the solution. Neither of these 'tricks'
% makes much difference in the 3D result though. 
%
% Written by H. B. Bingham, DTU
%
% This version: August 19, 2020
%

clear all; ifig=0;
addpath ../Core/

%
% Set up the wedge geometry
%
np=10; dz=2/(np); z=[0:np/2]'*dz; z(np/2+2:np+1,1)=z(np/2:-1:1,1);
y=1-z; y(np/2+2:np+1,1)=-y(np/2+2:np+1,1); 
n1=1/sqrt(2); n2=1/sqrt(2);
xj(:,1)=y; xj(:,2)=z; 
% Set this to zero so as not to invoke a vertical shift within KochinFunction_m
draft=0;                  
%
% Choose the potential distribution, 'linear' or 'constant'. 
%
% Phi='constant';
Phi='linear';
phin=n2*ones(np,1);       % The heave BC
switch Phi
    case 'linear'
        phi=1-1/2*(z(2:np+1)+z(1:np));
    case 'constant'
        phi=ones(np,1); 
end
%
% The vertically shifted coordinates
%
zs=10^-2;
xjs(:,1)=y; xjs(:,2)=z+zs;
%
% Set the non-dimensional frequency and velocity. The length-scale here is
% the sectional 1/2 beam which is equal to the draft. Choosing a 
% length-to-draft ratio here, we can consider the behavior of the Kochin 
% function for a chosen wavelength and Froude number. 
%
LtoT=19;                     % A typical bulker/tanker value
lamToL=1.1; lam=lamToL*LtoT; % A lambda/L value
Fn=0.2; FnT=sqrt(LtoT)*Fn;   % A Froude number
K=2*pi/lam; om=sqrt(K); tau=FnT*om; k0=1/FnT^2;
kNyq=pi/dz;  mmax=kNyq;      % Set the m-limit to solve for
%
% Shift the integration bounds slightly to avoid the singular points
%
shift=mmax/1000;           
k1=-k0/2*(1+2*tau+sqrt(1+4*tau))-shift;
k2=-k0/2*(1+2*tau-sqrt(1+4*tau))+shift;
if tau<1/4
    k3=k0/2*(1-2*tau-sqrt(1-4*tau))-shift;
    k4=k0/2*(1-2*tau+sqrt(1-4*tau))+shift;
else
    k3=k0*tau; k4=k3;
end
%
% The three ranges of m and \bar kappa
%
nm0=500;
i1=[1:nm0]; i2=[nm0+1:2*nm0]; i3=[2*nm0+1:3*nm0];
mvec=[linspace(-mmax,k1,nm0)';linspace(k2,k3,nm0)';linspace(k4,mmax,nm0)'];
nm=length(mvec);
kvec=(om+FnT*mvec).^2;     % This is bar kappa in the paper
kbarV=sqrt(kvec.^2-mvec.^2); 

nvec=[zeros(np,1),[n1*ones(np/2,1);-n1*ones(np/2,1)],n2*ones(np,1)];

for i=1:nm
    kappabar=kvec(i); m=mvec(i); kbar=kbarV(i);
    
    [H(i,1),Hm]=KochinFunction_m_3D(kappabar, m, xj, phi, phin, nvec);
    [Hs(i,1),Hm]=KochinFunction_m(kappabar, m, xjs, phi, phin);
    [H0(i,1),Hm]=KochinFunction_m(kappabar, m, xjs, phi-phi(1), phin);

    switch Phi
        case 'constant'
            He(i,1)=sqrt(2)*(n2-(1i*kbar*n1-kappabar*n2)).*exp(1i*kbar)./(kappabar+1i*kbar).*(1-exp(-(kappabar+1i*kbar))) ...
                +sqrt(2)*(n2-(-1i*kbar*n1-kappabar*n2)).*exp(-1i*kbar)./(kappabar-1i*kbar).*(1-exp(-(kappabar-1i*kbar)));
        case 'linear'
            He(i,1)=sqrt(2).*exp(1i*kbar).*(n2./(kappabar+1i*kbar).*(1-exp(-(kappabar+1i*kbar))) ...
                -(1i*kbar*n1-kappabar*n2).*(exp(-(kappabar+1i*kbar))-1 + kappabar+1i*kbar)./(kappabar+1i*kbar).^2)...
                +sqrt(2).*exp(-1i*kbar).*(n2./(kappabar-1i*kbar).*(1-exp(-(kappabar-1i*kbar))) ...
                -(-1i*kbar*n1-kappabar*n2).*(exp(-(kappabar-1i*kbar))-1 + kappabar-1i*kbar)./(kappabar-1i*kbar).^2) ;
    end

end
%
% The error relative to the max. value.
%
err=(real((H-He))+1i*imag((H-He)))./max(abs(He));
%
% The contribution from the free-surface potential integrated exactly
%
H00=phi(1)*( (-kvec-1i*kbarV)./(kvec-1i*kbarV).*(exp(-kvec)-exp(-1i*kbarV))...
    +(kvec-1i*kbarV)./(kvec+1i*kbarV).*(-exp(-kvec)+exp(1i*kbarV)) );
H0=H0+H00;

ifig=ifig+1; figure(ifig);clf
plot(mvec(i1),abs(H(i1)),'b',mvec(i1),abs(Hs(i1)),'r',mvec(i1),abs(H0(i1)),'g',...
    mvec(i2),abs(H(i2)),'b',mvec(i2),abs(Hs(i2)),'r',mvec(i2),abs(H0(i2)),'g',...
    mvec(i3),abs(H(i3)),'b',mvec(i3),abs(Hs(i3)),'r',mvec(i3),abs(H0(i3)),'g')
xlabel('m'); ylabel('|H|');grid on
legend('No shift',['zs=',num2str(zs)]','\phi->\phi-\phi(z-0)','Location','NorthEast')
title(['Sectional Kochin function on a wedge, Fn=',num2str(Fn),' \lambda/L=',num2str(lamToL)])
%
% The integration over x factor
%
Ixfac=2./mvec.*sin(LtoT/2*mvec);
H=H.*Ixfac;
Hs=Hs.*Ixfac;
H0=H0.*Ixfac;
%
% Plot the complete Kochin function
%
ifig=ifig+1; figure(ifig);clf
plot(mvec(i1),abs(H(i1)),'b',mvec(i1),abs(Hs(i1)),'r',mvec(i1),abs(H0(i1)),'g',...
    mvec(i2),abs(H(i2)),'b',mvec(i2),abs(Hs(i2)),'r',mvec(i2),abs(H0(i2)),'g',...
    mvec(i3),abs(H(i3)),'b',mvec(i3),abs(Hs(i3)),'r',mvec(i3),abs(H0(i3)),'g')
xlabel('m'); ylabel('|H|');grid on
legend('No shift',['zs=',num2str(zs)]','\phi->\phi-\phi(z-0)','Location','NorthEast')
title(['Kochin function on a prismatic barge, Fn=',num2str(Fn),' \lambda/L=',num2str(lamToL)])
%
% The factor in the integrand for R_w
%
Rfac=kvec.*(mvec+K)./kbarV;
%
% Plot the integrand
%
ifig=ifig+1; figure(ifig);clf
plot(mvec(i1),-Rfac(i1).*abs(H(i1)).^2,'b',mvec(i1),-Rfac(i1).*abs(Hs(i1)).^2,'r',mvec(i1),-Rfac(i1).*abs(H0(i1).^2),'g',...
    mvec(i2),Rfac(i2).*abs(H(i2)).^2,'b',mvec(i2),Rfac(i2).*abs(Hs(i2)).^2,'r',mvec(i2),Rfac(i2).*abs(H0(i2)).^2,'g',...
    mvec(i3),Rfac(i3).*abs(H(i3)).^2,'b',mvec(i3),Rfac(i3).*abs(Hs(i3)).^2,'r',mvec(i3),Rfac(i3).*abs(H0(i3)).^2,'g')
xlabel('m'); ylabel('R_w integrand');grid on
legend('No shift',['zs=',num2str(zs)]','\phi->\phi-\phi(z-0)','Location','NorthWest')
title(['Added resistance integrand, prismatic barge, Fn=',num2str(Fn),' \lambda/L=',num2str(lamToL)])
%
% Plot the errors in the sectional solution 
%
im=1;
ifig=ifig+1; figure(ifig);clf
plot(mvec,real(err))
xlabel('m'); ylabel('real(H-H_e)/real(H_e)');grid on
title(['Errors in H(m) with \phi=',Phi])

ifig=ifig+1; figure(ifig);clf
plot(mvec,imag(err))
xlabel('m'); ylabel('imag(H-H_e)/imag(H_e)');grid on
title(['Errors in H(m) with \phi=',Phi])

%
% Kashiwagi's factor on the numerical integral
%
ifig=ifig+1; figure(ifig);clf
Kfac1=-(1-sqrt(1-mvec(i1).^2./kvec(i1).^2))./sqrt(1-mvec(i1).^2./kvec(i1).^2)...
    .*(mvec(i1)+K);
Kfac3=(1-sqrt(1-mvec(i3).^2./kvec(i3).^2))./sqrt(1-mvec(i3).^2./kvec(i3).^2)...
    .*(mvec(i3)+K);
plot(mvec(i1),Kfac1.*abs(H(i1)).^2,'b',mvec(i1),Kfac1.*abs(Hs(i1)).^2,'r',...
    mvec(i3),Kfac3.*abs(H(i3)).^2,'b',mvec(i3),Kfac3.*abs(Hs(i3)).^2,'r')
xlabel('m'); grid on; legend('No shift',['zs=',num2str(zs)]');
title("Kashiwagi's 'numerical' integrand")
