clc
clear variables
close all

L = 320;
lamlMin = 0.1;
lamlMax = 4;
g = 9.81;
beta = 0*pi/180;
Fr = linspace(0,0.3, 7);
%Fr = 0;
om0Min = 2*pi*g/(lamlMax*L);
om0Max = 2*pi*g/(lamlMin*L);
om0 = linspace(om0Min, om0Max, 60);
cols = [".-r",".-b", ".-y", ".-g", ".-k", ".-m", ".-c"];

for i=1:length(Fr)
    U = Fr(i) * sqrt(g*L);
    ome = om0 - om0.^2/g*U*cos(pi-beta);
    subplot(1,2,1)
    plot(om0, ome, cols(i));
    hold on
end
grid on

for i=1:length(Fr)
    U = Fr(i) * sqrt(g*L);
    ome = om0 - om0.^2/g*U*cos(beta);
    subplot(1,2,2)
    plot(om0, (ome), cols(i));
    hold on
end
grid on

% om01 = g/(2*U)*(1-sqrt(1-ome*4*U/g));
% om02 = g/(2*U)*(1+sqrt(1-ome*4*U/g));
