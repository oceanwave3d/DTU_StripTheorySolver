clc
clearvars
close all
addpath('../../src/');
%%
load('S.mat');
%%

Sd  = S(vertcat(S.Nv)==0); % The dummy stations
S   = S(vertcat(S.Nv)>0);  % Remove the dummy stations
npans = length(S(2).xc);
%
range1 = 1:npans/2;
range2 = npans/2+1:npans;

S = SetS(S, range1);
S = SetS(S, range2);
%
S = ComputeSurfaceDerivatives(S);
%
nst = length(S);
Nc = npans;
N=nst*Nc;

phixc=zeros(N,3);  % Initialize the coordinates
for i=1:nst
    phixc((i-1)*Nc+1:i*Nc,1)=S(i).dphi_scatr_dx(:,1);
    phixe((i-1)*Nc+1:i*Nc,1)=S(i).exactx(:,1);
    x((i-1)*Nc+1:i*Nc,1)=S(i).xst;
    x((i-1)*Nc+1:i*Nc,2)=S(i).xc(:,1);
    x((i-1)*Nc+1:i*Nc,3)=S(i).xc(:,2);
end
norm = max(phixe);
plot(x(:,1), phixe/norm, 'g-', 'LineWidth',2);
hold on
plot(x(:,1), phixc(:,1)/norm, 'k-','LineWidth',1);

%
function [S] = SetS(S, range)
L = 1;
k=2*pi/L;
alpha=2;  % The half-width of the FD stencils.
nst = length(S);
Nc=length(S(2).xc(range));  % The number of panel centroids.
%
% The u,v coordinates
%
u=1/(nst-1)*[0:nst-1]';
v=1/Nc*(1/2+[0:Nc-1]');
%
% The derivative matrices ordered to follow v-first then u.
%
Du=BuildDx2DOneSided(nst,Nc,u,alpha,1);
Dv=BuildDy2DOneSided(nst,Nc,v,alpha,1);
%
% Extract the coordinates following v first then u.
%
N=nst*Nc;
x=zeros(N,3);  % Initialize the coordinates
for i=1:nst
    x((i-1)*Nc+1:i*Nc,1)=S(i).xst;
    x((i-1)*Nc+1:i*Nc,2)=S(i).xc(range,1);
    x((i-1)*Nc+1:i*Nc,3)=S(i).xc(range,2);
end
%
% Take all of the coordinate derivatives and build the operators in Eqs.
% (8) & (9) of Bingham & Maniar (1996).
%
xu(:,1)=Du*x(:,1); xu(:,2)=Du*x(:,2); xu(:,3)=Du*x(:,3);
xv(:,1)=Dv*x(:,1); xv(:,2)=Dv*x(:,2); xv(:,3)=Dv*x(:,3);
for i=1:N
    E(i,1)=xu(i,:)*xu(i,:)';
    F(i,1)=xu(i,:)*xv(i,:)';
    G(i,1)=xv(i,:)*xv(i,:)';
    n(i,1)=xv(i,2)*xu(i,3)-xv(i,3)*xu(i,2);
    n(i,2)=xv(i,3)*xu(i,1)-xv(i,1)*xu(i,3);
    n(i,3)=xv(i,1)*xu(i,2)-xv(i,2)*xu(i,1);
end

H=sqrt(E.*G-F.^2);
for i=1:3
    n(:,i)=n(:,i)./H;
end

nx = reshape(n(:,1), Nc, nst);
ny = reshape(n(:,2), Nc, nst);
nz = reshape(n(:,3), Nc, nst);

for i=1:nst
    x = S(i).xst;
    y = S(i).xc(range,1);
    z = S(i).xc(range,2);
    S(i).ScatPhiMapped(range,1) = cos(k*x).*cos(k*y).*exp(-k*z);
    phix = -k*sin(k*x).*cos(k*y).*exp(-k*z);
    phiy = -k*cos(k*x).*sin(k*y).*exp(-k*z);
    phiz = -k*cos(k*x).*cos(k*y).*exp(-k*z);
    S(i).ScatBCMapped(range,1) = nx(:,i).*phix + ny(:,i).*phiy + nz(:,i).*phiz;
    S(i).exactx(range,1) = phix;
    S(i).exacty(range,1) = phiy;
    S(i).exactz(range,1) = phiz;
end

end
%
