%%
% This script tests an evaluation of the x-derivative of a function defined
% on a set of structured points along the Wigley hull geometry. Following
% Bingham & Maniar IWWWFB (1996) Eqs. (8) & (9), we apply arbitrary order
% finite differences to evaluate all of the required surface derivatives,
% which are then combined with the normal derivative to find the
% x-derivative. 
%
% Written by H.B. Bingham, November 2020.
%
%%
clear all;
% close all
clc
ifig=0;
set(0,'defaulttextinterpreter','latex')
set(0, 'defaultAxesTickLabelInterpreter','latex');
set(0, 'defaultLegendInterpreter','latex');

addpath('../../Src/');

%%
%  Read in and re-mesh the hull geometry
%
L     = 2.5;
B     = 0.5;
Draft = 0.175;
tmp   = load('points.txt');
xst   = unique(tmp(:,1),'stable'); nst=length(xst);  % The station coordinates
Nv=25; % The number of vertices along each station. 
%
% Loop over the stations and extract the section points. Flip the z-axis so
% that it runs from the mean free surface (the waterline) to the keel and
% remove the extra point along the centerline, then re-distribute the
% points so that they are nearly uniformly spaced  in arc-lenth along the
% station.
%
for i=1:nst
    S(i).x=flipud(tmp(tmp(:,1)==xst(i,1),2:3));
    S(i).x(:,2)=-(S(i).x(:,2)-Draft);
    S(i).x = GenerateMesh(S(i).x, Nv, 1e-6, 0);
    fprintf('%s %d %s %d \n', 'station re-meshed:' ,i, 'of', nst);
    S(i).Nv=length(S(i).x); % Number of vertices for this station
end
%
%
%% Move the coordinate system to the center of floatation and add bow & stern dummy stations
%
Xg = L/2;
xst = xst-Xg;
for i=1:length(xst)
    S(i).xst = xst(i);
end
%
% Add in the bow and stern stations as dummy stations where the local
% solutions will just be zero for the Wigley hull. If a station has Nv=0,
% then it is considered a dummy station in the strip theory solver.
%
% Note that the stations should run from stern to bow.
%
xst=[-L/2;xst;L/2]; nst = nst+2;
%
S1(1)=S(1); S1(1).Nv=0;
S3(1)=S1(1); S3(1).x(:,1)=0;
S3(2:nst-1)=S(1:nst-2);
S3(nst)=S1; S3(nst).x(:,1)=0;
S=S3;
xst = num2cell(xst);
[S.xst] = xst{:};
%
% Get the normal derivatives and add in the centroid coordinates. 
%
S=Compute3DNormals(S);
%
%%
% Set up a test function and take derivatives on the body surface.
% 
k=2*2*pi/L;
%
% The parameter u runs from 0 to 1 from stern to bow, and the parameter v
% runs from 0 to 1 from free surface to the keel along the body surface. 
% Thus the d/du and d/dv operators are approximated by FD matrices on a 
% uniform grid.
%
Nc=Nv-1;  % The number of panel centroids.
alpha=2;  % The half-width of the FD stencils.
%
% The u,v coordinates
%
u=1/(nst-1)*[0:nst-1]'; 
v=1/Nc*(1/2+[0:Nc-1]');
%
% The derivative matrices ordered to follow v-first then u. 
%
Du=BuildDx2DOneSided(nst,Nc,u,alpha,1);
Dv=BuildDy2DOneSided(nst,Nc,v,alpha,1);
%
% Extract the coordinates following v first then u. 
%
N=nst*Nc;
x=zeros(N,3);  % Initialize the coordinates
for i=1:nst
    x((i-1)*Nc+1:i*Nc,1)=S(i).xst;
    x((i-1)*Nc+1:i*Nc,2)=S(i).xc(:,1);
    x((i-1)*Nc+1:i*Nc,3)=S(i).xc(:,2);
end
%
% Take all of the coordinate derivatives and build the operators in Eqs.
% (8) & (9) of Bingham & Maniar (1996). 
%
xu(:,1)=Du*x(:,1); xu(:,2)=Du*x(:,2); xu(:,3)=Du*x(:,3);  
xv(:,1)=Dv*x(:,1); xv(:,2)=Dv*x(:,2); xv(:,3)=Dv*x(:,3);  
for i=1:N
    E(i,1)=xu(i,:)*xu(i,:)';
    F(i,1)=xu(i,:)*xv(i,:)';
    G(i,1)=xv(i,:)*xv(i,:)';
    n(i,1)=xv(i,2)*xu(i,3)-xv(i,3)*xu(i,2);
    n(i,2)=xv(i,3)*xu(i,1)-xv(i,1)*xu(i,3);
    n(i,3)=xv(i,1)*xu(i,2)-xv(i,2)*xu(i,1);
end
H=sqrt(E.*G-F.^2);
for i=1:3
    n(:,i)=n(:,i)./H;
end
%
% Plot up the normal vectors.
%
figure(1); clf
quiver3(x(:,1), x(:,2), x(:,3), n(:,1), n(:,2),n(:,3), 'MarkerSize', 4); axis equal;
hold on
set ( gca, 'zdir', 'reverse' )
%
% Set up the test function and its derivatives.
%
phi=cos(k*x(:,1)).*cos(k*x(:,2)).*exp(-k*x(:,3));  % the exact test function
phixE=-k*sin(k*x(:,1)).*cos(k*x(:,2)).*exp(-k*x(:,3)); % and phix
phiyE=-k*cos(k*x(:,1)).*sin(k*x(:,2)).*exp(-k*x(:,3)); % and phiy
phizE=-k*cos(k*x(:,1)).*cos(k*x(:,2)).*exp(-k*x(:,3)); % and phiz
for i=1:N
    phin(i,1)=n(i,:)*[phixE(i,1);phiyE(i,1);phizE(i,1)];
end
%
% Take the surface derivatives of phi.
%
phiu=Du*phi; phiv=Dv*phi;
%
% The x-component of Eq. (8) gives the x-derivative.
%
phix=1./H.^2.*(xu(:,1).*(G.*phiu-F.*phiv) + xv(:,1).*(E.*phiv-F.*phiu)) ...
    + phin.*n(:,1);
%
% Plot the derivative and the errors.
%
figure(2);clf
plot(x(:,1),phix,x(:,1),phixE); grid on;
xlabel('$x$'); ylabel('$\phi_x$'); legend('Computed','Exact');

figure(3);clf
plot(x(:,1),(phix-phixE)/k); grid on;
xlabel('$x$'); ylabel('$(\phi_x-\phi^E_x)/k$');
title('Relative errors in the derivative')

%%
% For our case where the x-coordinates do not change with v, dx/dv=0 so
% there is no contribution from the second term in the surface gradient of
% Eq. (8). Also, G dphi/du >> F dphi/dv, so the surface gradient term is
% approximately just the factor dx/du G/H^2 times dphi/du. This factor is
% the stretching factor 1/L times the component of the u-vector in the
% x-direction: 
%
figure(4);
plot(x(:,1),xu(:,1).*G./H.^2,'o',x(:,1),1/L*ones(N,1))
xlabel('$x$'); legend('$x_u G/H^2$','$1/L$'); grid on
%%
%
set(0,'defaulttextinterpreter','tex')
set(0, 'defaultAxesTickLabelInterpreter','tex');
set(0, 'defaultLegendInterpreter','tex');

