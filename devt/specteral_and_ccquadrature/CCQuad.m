function [res] = CCQuad(x, Fx)

a = x(1);
b = x(end);

n = length(x)-1;
l = (0:n)';
k = (2:n)';

w = cos(l*l'*pi/n)\[2;0;(1+(-1).^k)./(1-k.^2)];

res = 0.5*(b-a)*sum(w.*Fx);

end

