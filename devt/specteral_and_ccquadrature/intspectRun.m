clc
close all
clear variables

N = 12;
x = linspace(-.5, .5, N);
Fx = sin(10*pi*x);
res = intspec(x.', Fx.', 15);
