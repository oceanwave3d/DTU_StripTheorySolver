function [res] = CCQuad_01(a,b, n, fun)

l = (0:n)';
k = (2:n)';

CbNodes = cos(l*pi/n);
w = cos(l*l'*pi/n)\[2;0;(1+(-1).^k)./(1-k.^2)];
z = a+0.5*(b-a)*(CbNodes+1);

f = feval(fun,z);

res = 0.5*(b-a)*sum(w.'*f);

end

