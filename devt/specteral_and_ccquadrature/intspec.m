function [res] = intspec(x, Fx, m)

x  = x(2:end-1);
Fx = Fx(2:end-1);
nx = length(x);
k  = (1:nx)'*pi;

BASE  = zeros(nx,nx);
for i=1:nx
    BASE(i,:) = sin(x(i)*k + k/2);
end
C = BASE\Fx;

res = 0; 
tol = 10^-14;

for j=1:nx
    K = k(j);
    c = C(j);
    if abs(m-K)<=tol
        res = res - c*exp(-1i*m/2)*(exp(2*1i*m)-2*1i*m-1)/(4*m);
    else
        res = res + c*exp(-1i*m/2)/(K^2-m^2)*(K-exp(1i*m)*(K*cos(K)-1i*m*sin(K)));        
    end
end

end

