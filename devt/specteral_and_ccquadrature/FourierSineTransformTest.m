%%
% This script tests the evaluation of the Kochin function integral along
% the length of the ship by application of a Fourier Sine Transform to
% approximate the function. 
%
% Written by Harry Bingham and Mostafa Amini-Afshar
%
% This version: September 17, 2020.
%%
%
% The Strip Theory version of the Kochin function has the form: 
%
% $$
% H(m) = \int_{-\frac{1}{2}}^{\frac{1}{2}} f_m(x)\exp{({\rm i} mx)}\; dx \qquad (1)
% $$
%
% where $x$ and $m$ are non-dimensionalized by the ship length $L$. 
% Since the function is assumed to be zero at both ends of the ship, we 
% approximate $f_m(x)$ as a sum of Fourier sine modes
%
% $$
% f_m(x)=\sum_{j=1}^{N_x} c_j \sin{(k_j (x+1/2))} \qquad (2)
% $$
% 
% where $k_j=j\Delta k$, for $j=1,2,\ldots,N_x$, with $\Delta k=\pi$. 
%
% Inserting this into the Kochin function integral and integrating
% term-by-term gives:
%
% $$
% H(m)= \sum_{j=1}^{N_x} \frac{c_j\exp{(\frac{{-\rm i}m}{2})}
% \left[k_j - \exp{({\rm i}m)}(k_j\cos{k_j}-{\rm
% i}m\sin{k_j})\right]}{k_j^2-m^2} \qquad (3)
% $$
%
% For the special case where $k_j=m$, the contribution to the sum should be 
% replaced by 
%
% $$
% -\frac{c_j\exp{(-\frac{{\rm i}m}{2})}(\exp{(2{\rm i}m)}-2{\rm i}m-1)}{4m}.
% $$
%
% For a uniformly-spaced set of points in $x$, the coefficients can be 
% found from the Sine transform of $f_m(x)$ 
%
% $$
% c_j= \int_{-\frac{1}{2}}^{\frac{1}{2}} f_m(x) \sin{(k_j (x+1/2))}\;dx = 
%              2\Delta x\sum_{i=1}^{N_x} f_m(x_i)\sin{(k_j(x_i+1/2))}.
% $$
%
% More generally, Eq. (2) can be thought of as a matrix equation of the
% form 
%
% $$
% f_i = A_{ij} \;c_j, \qquad (4) 
% $$
%
% $$ A_{ij}=\sin{(k_j(x_i+1/2))}, \qquad f_i=f_m(x_i). $$
% 
% Building the matrix and solving the system gives the coefficients $c_j$.
% This approach supports a non-uniform spacing in either $x$ or $k$ or
% both. If the number of wave numbers $k_j$ is chosen to be less than the
% number of function values $f_i$, then this gives a least-squares solution
% by mulitplying (4) by the transpose of $A_{ij}$ and solving
%
% $$
% (A_{ki}\,A_{kj}) \;c_j = A_{ji}\, f_i.
% $$
%
% 
% Some examples are shown below. $f_m(x)$ is in general a complex function, 
% but is taken to be real for these examples. 
%
clear all
ifig=0;
%
% Working in a local coordinate x=x'/L running from -1/2<= x <=1/2, 
% we have nx samples from x=-1/2+dx to x=1/2-dx, with the first and last 
% stations equal to zero by definition. For now, we assume that the grid 
% spacing is uniform. 
%
nx=28; dx=1/(nx+1); x=[1:nx]'*dx-1/2; 
%
% There are nx wavenumbers in the Fourier Sine Transform space with the 
% zero and Nyquist wavenumbers again zero by definition. 
%
kNyq=pi/dx; dk=pi;  k=[1:nx]'*dk;
%
% Generate the function f(x) as a sum of sine-components: 
%
d=zeros(nx,1); 
%
% This example is meant to look like a relatively small m-value solution.
% m=pi; d(1)=.035; d(3)=.007; 
% This example is meant to look like a relatively large m-value solution.
m=kNyq/2; d(1)=.035; d(3)=.007; d(11)=.005; d(15)=.003; d(21)=.002;
%
for j=1:nx
    fe(j,1)=d'*sin(k*(x(j)+1/2));
end
ifig=ifig+1; figure(ifig);clf
plot(x,fe)
xlabel('x'); ylabel('f(x)');
%
% Recover the coefficients by direct evaluation of the Sine Transform and
% by inversion of the system matrix. 
%
c=zeros(nx,1); A=zeros(nx);
for i=1:nx
    A(i,:)=sin(k(i)*(x+1/2));
    c(i)=A(i,:)*fe; 
end
c=2*c*dx;  % Include the dx factor in the FST
c2=A\fe;   % Solve directly
%
% Compare to the original coefficients
%
ifig=ifig+1; figure(ifig);clf
plot(k,c-d,k,c2-d)
xlabel('k'); ylabel('Error in c(k)'); legend('FST','Direct');
%
% The errors are at machine precision.
%
% Recover the function by summation of the coefficients. 
%
for j=1:nx
    f(j,1)=c'*sin(k*(x(j)+1/2));
end
%
% and compare to the original
%
ifig=ifig+1; figure(ifig);clf
plot(x,f-fe)
xlabel('x'); ylabel('Error in f(x)');
%
% The errors are again at machine precision.
%
% Compute the integral of f(x) exp(i mx)
%
H=0; tol=10^-14;
for j=1:nx
    if abs(m-k(j))<=tol
        H=H- c(j)*exp(-1i*m/2)*(exp(2*1i*m)-2*1i*m-1)/(4*m);
    else
        H=H+(c(j)*exp(-1i*m/2)*(k(j)-exp(1i*m)*(k(j)*cos(k(j))-1i*m*sin(k(j))))/(k(j)^2-m^2));
    end
end
H
%%
% Test a Gaussian function on a non-uniform grid. Taking the function to be
%
% $$
% f_m(x) = \exp{(-.5(x/\sigma)^2)}
% $$
%
% the integral is given by
%
% $$
% \int_{-1/2}^{1/2} f_m(x) \exp{({\rm i}mx})\, dx = {\rm i}\exp{(-1/2
% m^2\sigma^2)} \sqrt{\pi/2}\left[{\rm Erfi}\left(\frac{-{\rm
% i}/2+m\sigma^2}{\sqrt{2}\sigma}\right) - {\rm Erfi}\left(\frac{{\rm
% i}/2+m\sigma^2}{\sqrt{2}\sigma}\right) \right]
% $$
%
% where Erfi is the complex error function. 
%
% The example below tells me that while the function can be approximated
% well on a non-uniform grid by finding the coefficients through a direct
% solution, these coefficients are not the same ones that we find on a 
% uniform grid and hence do not give the correct integral. It may be that
% by reducing the number of coefficients and solving a least-squares
% problem that we can do better, but I haven't tried this yet. 
%
%%
% Here is a stretched grid function which maps the uniform grid 0<s<1 to a
% non-uniform grid -1/2<x<1/2 based on a stretching factor Cs and an
% inflection point xc. If Cs=0 the grid is uniform, for Cs>0 the grid is
% stretched towards xc, and if Cs<0 then it is stretched away from xc. 
%
Cs=eps; L=1; xc=1/2;  % Choosing a uniform grid spacing gives good accuracy
% Cs=.1; L=1; xc=1/2;  % The more the stretching, the larger the error. 
%
ds=1/(nx+1); s=[1:nx]'*ds; 
x=(-L/2+L*s+Cs*(xc-L*s).*(1-s).*s)/L;
%
% Use a Gaussian function as the test function. 
%
sigma=.15; fe=exp(-.5*(x/sigma).^2);
ifig=ifig+1; figure(ifig);clf
plot(x,fe,'-o')
%
% Solve for the coefficients.
%
c=zeros(nx,1); A=zeros(nx);
for i=1:nx
    A(i,:)=sin(k(i)*(x+1/2));
end
c=A\fe;   % Solve directly
%
% Plot their magnitudes on a log-scale.
%
ifig=ifig+1; figure(ifig);clf
semilogy(k,abs(c),'-o')
xlabel('k'); ylabel('c');
%
% Recover the function 
%
f=A*c;
%
% and check the error
%
ifig=ifig+1; figure(ifig);clf
plot(x,fe-f)
xlabel('x'); ylabel('f_e-f');
%
% Compute the integral of f(x) exp(i mx)
%
m=pi;
H=0; tol=10^-14;
for j=1:nx
    if abs(m-k(j))<=tol
        H=H- c(j)*exp(-1i*m/2)*(exp(2*1i*m)-2*1i*m-1)/(4*m);
    else
        H=H+(c(j)*exp(-1i*m/2)*(k(j)-exp(1i*m)*(k(j)*cos(k(j))-1i*m*sin(k(j))))/(k(j)^2-m^2));
    end
end
%
% Compare with the exact result for the integral with exp(i m x)
%
He=1i*exp(-1/2*m^2*sigma^2)*sqrt(pi/2)*sigma*(erfi((-1i/2+m*sigma^2)/(sqrt(2)*sigma)) ...
    -erfi((1i/2+m*sigma^2)/(sqrt(2)*sigma)));

display(['Error in the integral of a Gaussian: ',num2str(H-He)])
