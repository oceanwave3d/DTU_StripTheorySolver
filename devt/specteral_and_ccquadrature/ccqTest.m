clc
clear variables
close all

n = 120;
l = (0:n)';
a = -1/2;
b = 1/2;
CbNodes = cos(l*pi/n);
z = a+0.5*(b-a)*(CbNodes+1);

fx = z.*sin(2*exp(2*sin(2*exp(2*z))));
res= CCQuad(z,fx)

% f = @(x) x.*sin(2*exp(2*sin(2*exp(2*x))));
% res = CCQuad_01(-1/2,1/2,n, f)