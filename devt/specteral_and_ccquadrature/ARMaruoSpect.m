%
% This function implements Maruo's method for calculation of mean second-order wave
% drift force (added resistance) based on the Kochin Functions. For further details
% on Maruo's method see:
% H. Maruo, “Wave resistance of a ship in regular head seas,” Bulletin of the Faculty
% of Engineering, Yokohama National University, vol. vol. 9, 1960.
%
% Written by: Mostafa Amini-Afshar
%

function [Rw, Kochin1, Kochin2, Kochin3] = ARMaruoSpect(U, rao, omegao, omegae, S, findex, beta, MaruoCoeff, KochinType)

C      = MaruoCoeff.C;
Cleft  = MaruoCoeff.Cleft;
Cright = MaruoCoeff.Cright;

xst   = vertcat(S.xCoor);
g     = 1;
rho   = 1;
K     = omegao^2/g;
N1    = 150;
N2    = 60;
N3    = 300;
nst   = length(xst);

Kochin1 = struct('Heave',{},'Pitch',{},'Scatr',{},'mvec', {});
Kochin2 = struct('Heave',{},'Pitch',{},'Scatr',{},'mvec', {});
Kochin3 = struct('Heave',{},'Pitch',{},'Scatr',{},'mvec', {});

if U~=0 % Added resistance (Forward-Speed Cases)
    kNyq      = 180;%GetNyquistWaveNumber(xst);
    tau       = U * omegae/g;
    kappaZero = g/U^2;
    kBar1     = -kappaZero/2 * (1 + 2 * tau + sqrt(1 + 4 * tau));
    kBar2     = -kappaZero/2 * (1 + 2 * tau - sqrt(1 + 4 * tau));
    
    if tau < 0.25
        kBar3 =  kappaZero/2 * (1 - 2 * tau - sqrt(1 - 4 * tau));
        kBar4 =  kappaZero/2 * (1 - 2 * tau + sqrt(1 - 4 * tau));
    else
        kBar3 = kappaZero * tau;
        kBar4 = kappaZero * tau;
    end
else % Wave Drift Froce (Zero-Speed Cases)
    kBar1 =  0;
    kBar2 = -K;
    kBar3 =  K;
    kBar4 =  0;
    kNyq  =  0;
end

Rw1  =  0;
Rw2  =  0;
Rw3  =  0;

finteg = 1; % 1 If The 1st Integral Should Be Included
sinteg = 1; % 1 If The 2nd Integral Should Be Included
tinteg = 1; % 1 If The 3rd Integral Should Be Included

if kBar1 >-kNyq && finteg && U~=0
    DL = (kBar1+kNyq)/(N1-1);
    [integrand, Kochin1] = GetIntegrand(-kNyq, kBar1, N1);
    Rw1 = TaylorIntegration(N1, integrand, DL, C, Cleft, Cright);
    
end

if kBar3 > kBar2 && sinteg
    DL = (kBar3-kBar2)/(N2-1);
    [integrand, Kochin2] = GetIntegrand(kBar2, kBar3, N2);
    Rw2 = TaylorIntegration(N2, integrand, DL, C, Cleft, Cright);
end

if kNyq > kBar4 && tinteg && U~=0
    DL = (kNyq-kBar4)/(N3-1);
    [integrand, Kochin3] = GetIntegrand(kBar4, kNyq, N3);
    Rw3 = TaylorIntegration(N3, integrand, DL, C, Cleft, Cright);
end

Rw = rho/(4*pi) * (-Rw1 + Rw2 + Rw3);

% ---

    function [integrand, Kochin] = GetIntegrand(kLow, kUp, N)
        
        integrand  = zeros(N,1);
        StripHeave = zeros(nst,1);
        StripPitch = zeros(nst,1);
        StripScatr = zeros(nst,1);
        HT         = zeros(N,1);
        mvec       = linspace(kLow, kUp, N);
        
        for p=1:N
            m = mvec(p);
            kappaBar = 1/g*(omegae + m*U)^2;
            
            for t=1:nst
                if S(t).Nv == 0
                    StripHeave(t) = 0;
                    StripScatr(t) = 0;
                else
                    switch KochinType
                        case '2D'
                            [StripHeave(t), ~] = KochinFunction_m(kappaBar, m, S(t).xv, S(t).phi(:,findex,2), S(t).nv(:,2));
                            [StripScatr(t), ~] = KochinFunction_m(kappaBar, m, S(t).xv, S(t).ScatPhiMapped(:,findex), S(t).ScatBCMapped(:,findex));
                        case '3D'
                            [StripHeave(t), ~] = KochinFunction_m_3D(kappaBar, m, S(t).xv, S(t).phi(:,findex,2), S(t).n3D(:,3),S(t).n3D);
                            [StripScatr(t), ~] = KochinFunction_m_3D(kappaBar, m, S(t).xv, S(t).ScatPhiMapped(:,findex), S(t).ScatBCMapped(:,findex),S(t).n3D);
                    end
                end
                
                StripPitch(t) = StripHeave(t)*(-xst(t)+U/(1i*omegae));
            end
            
            fmx   = 1i*omegae*rao(3)*StripHeave + 1i*omegae*rao(5)*StripPitch + 1/(1i*omegao)*StripScatr;
            HT(p) = intspec(xst, fmx, m);
            %HT(p) = CCQuad(xst, fmx.*exp(1i*xst*m));
            
            integrand(p) = kappaBar * (m - K*cos(beta))/sqrt(kappaBar^2 - m^2) * (abs(HT(p)))^2;
        end
        
        Kochin.Heave = 0;
        Kochin.Pitch = 0;
        Kochin.Scatr = HT;
        Kochin.mvec  = mvec';
    end
end
