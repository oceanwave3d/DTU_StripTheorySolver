%
% THis script looks at the behavior of a Kochin function like integral for
% a very simple case where the section is just a vertical wall from z=0 to
% z=1, and the potential phi=1-z. In this case, the sway Kochin function is
% given by:
%
% H(K) = int_0^1 [1 - i (K (1-z))] exp[-Kz] = -1/K*(exp(-K)-1)-1i*(exp(-K)-1+K)/K
%
% From this we can see that the limit as K-> infinity is actually 1, i.e.
% the Kochin function does not go to zero at all. 
%
% Submerging the panels by zs=0.01 (1% of the section draft) makes the
% function decay. Also, scaling the potential to be phi-phi(z=0) removes
% the constant limit. Can we use this to remove this contribution from the
% numerical integration and add it back in analytically? 
%

clear all; ifig=0;

np=40; dz=1/(np); z=[0:np]'*dz; 
phin=ones(np,1); phi=1-[0:np-1]'/(np-1); 
zs=10^-2;

kmax=pi/dz; 

nk=100; dk=kmax/nk;
kvec=[1:nk]'*dk;

for j=1:nk
    K=kvec(j);
    
    x1=z(2:np+1,1); x0=z(1:np,1);
    
    F=-1/K*(exp(-K*x1) - exp(-K*x0));
    Fs=-1/K*(exp(-K*(x1+zs)) - exp(-K*(x0+zs)));
    
    G=1i*K*F;
    Gs=1i*K*Fs;
    
    H(j,1)=phin'*F-phi'*G;
    Hs(j,1)=phin'*Fs-phi'*Gs;
    H0(j,1)=phin'*F-(phi'-1)*G;
    H00(j,1)=phin'*F-ones(1,np)*G;
    
    He=-1/K*(exp(-K)-1)-1i*(exp(-K)-1+K)/K;
    
    err(j,1)=real((H(j)-He))/real(He)+1i*imag((H(j)-He))/imag(He);
end

ifig=ifig+1; figure(ifig);clf
plot(kvec,abs(H),kvec,abs(Hs),kvec,abs(H0))
xlabel('k'); ylabel('|H|');grid on
legend('No shift',['zs=',num2str(zs)]','\phi -> \phi-\phi_0')

ifig=ifig+1; figure(ifig);clf
loglog(kvec,abs(H),kvec,abs(Hs),kvec,abs(H0))
xlabel('k'); ylabel('|H|');grid on
legend('No shift',['zs=',num2str(zs)]','\phi -> \phi-\phi_0','Location','SouthWest')

ifig=ifig+1; figure(ifig);clf
plot(kvec,abs(H0),kvec,abs(H00))
%
% Plot the errors in the function
%
ifig=ifig+1; figure(ifig);clf
plot(kvec,real(err))
xlabel('k'); ylabel('real(H-H_e)/real(H_e)');grid on
ifig=ifig+1; figure(ifig);clf
plot(kvec,imag(err))
xlabel('k'); ylabel('imag(H-H_e)/imag(H_e)');grid on

