clc
clearvars
close all
%
% The script for numerical solution of the integral equation:
%
% q(x) + A(x)\int{-1}^1 [q(\xi)*W(x-\xi)] = f(x)
%

addpath(genpath('../../src'))

set(0,'defaulttextinterpreter','latex')
set(0, 'defaultAxesTickLabelInterpreter','latex');
set(0, 'defaultLegendInterpreter','latex');

W = @(k,x,m) cos(k*(x-m))/x; % The kernel
A = @(x) cos(x);
L = 1;
npoints = 40;
order = 4;
n = 1;
K = 2*pi/n;
Width = order+1;
nseg = npoints-1;
x = 2*linspace(-L/2,L/2, npoints)'/L;
Mat = diag(ones(npoints,1),0);

% Get fd coefficients
Cmid  = FDCoefficients(order/2, order/2);
Cleft = zeros(order+1, order+1, order+1);
Cright= zeros(order+1, order+1, order+1);
for index=1:order+1
    Cleft (:,:,index) = FDCoefficients(index-1, order-index+1);
    Cright(:,:,index) = FDCoefficients(order-index+1, index-1);
end
FdCoeff.C      = Cmid;
FdCoeff.Cleft  = Cleft;
FdCoeff.Cright = Cright;

% Set the matrix
for row=1:npoints
    for s=1:nseg
        ds = x(s+1)-x(s);
        for i=1:Width % THIS DEPENDS ON THE DERIVATIVES OP TO ORDER "Order"
            if s <= (order/2) %  POINTS AT THE LEFT BOUNDARY
                counter = 1;
                for j=1:order+1
                    u = K*(x(row)-x(j));
                    Mat(row,j) = Mat(row,j) + ds*A(K*x(row))*W(K,x(row), x(j))*Cleft(i, counter, s) / factorial(i);
                    counter = counter+1;
                end
            elseif  (npoints-s) < order/2 % POINTS AT THE RIGHT BOUNDARY
                rem     = npoints-s;
                counter = 1;
                for j=s-(order-rem):npoints
                    u = K*(x(row)-x(j));
                    Mat(row,j) = Mat(row,j) + ds*A(K*x(row))*W(K,x(row), x(j))*Cright(i, counter, rem + 1) / factorial(i);
                    counter = counter + 1;
                end
            else % INTERNAL POINTS (Expansion around 0)
                counter = 1;
                for j=(s-order/2):(s+order/2)
                    u = K*(x(row)-x(j));
                    Mat(row,j) = Mat(row,j) + ds*A(K*x(row))*W(K,x(row), x(j))*Cmid(i, counter) / factorial(i);
                    counter = counter + 1;
                end
            end
        end
    end
end
% Set the right hand side
fex = sin(K*x);
f =  fex + (K-cos(K)*sin(K))*sin(K*x).*cos(K*x)./(K*x);
% Solve the system
res = Mat\f;
% Plot
plot(x, fex,'-ob', 'LineWidth',1)
hold on
plot(x, res,'k-*', 'LineWidth',1)
erro = max(abs(fex-res))/max(abs(fex));
ax = gca;
ax.FontSize = 15;
xlabel('$x$')
ylabel('$q(x)$')
% The erro
e = abs(max(res-fex))/max(fex);
fprintf('%s %e\n', 'Error =', e);

