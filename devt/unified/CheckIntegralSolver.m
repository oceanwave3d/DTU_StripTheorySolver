clc
clearvars
close all
%
% The script for numerical solution of the integral equation:
%
% q(x) + A(x)\int{-1}^1 [q(\xi)*W(x-\xi)] = f(x)
%

addpath(genpath('../../src'))

set(0,'defaulttextinterpreter','latex')
set(0, 'defaultAxesTickLabelInterpreter','latex');
set(0, 'defaultLegendInterpreter','latex');

L = 1;
npoints = 40;
order = 4;
% Get fd coefficients
Cmid  = FDCoefficients(order/2, order/2);
Cleft = zeros(order+1, order+1, order+1);
Cright= zeros(order+1, order+1, order+1);
for index=1:order+1
    Cleft (:,:,index) = FDCoefficients(index-1, order-index+1);
    Cright(:,:,index) = FDCoefficients(order-index+1, index-1);
end
c.m = Cmid;
c.l = Cleft;
c.r = Cright;

n = 1;
K = 2*pi/n;
x = 2*linspace(-L/2,L/2, npoints)'/L;

w.f = @GetW;
w.parm.K = K;

a.f = @GetA;
a.parm.K = K;

s.f = @GetRHS;
s.parm.K = K;

res = IntegraEquationlSolver(x,w,a,s,c);
%%
% Set the right hand side
fex = exp(-K*x).*sin(K*x);
%fex = x.^3.*sin(K*x);
%fex = sin(K*x);
% Plot
plot(x, fex,'bo-', 'LineWidth',1)
hold on
plot(x, res,'k-*', 'LineWidth',1)
erro = max(abs(fex-res))/max(abs(fex));
ax = gca;
ax.FontSize = 15;
xlabel('$x$')
ylabel('$q(x)$')
% The erro
e = max(abs(res-fex))/max(abs(fex));
fprintf('%s %e\n', 'Error =', e);

