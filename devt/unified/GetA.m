function [A] = GetA(x, parm)
K = parm.K;
A = cos(K*x);
end