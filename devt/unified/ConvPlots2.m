%%
close all
clc
clearvars
set(0,'defaulttextinterpreter','latex')
set(0, 'defaultAxesTickLabelInterpreter','latex');
set(0, 'defaultLegendInterpreter','latex');
h = [10 20 40 80 160 320];
e4 = [5.838402e-01 1.107677e-01 5.507501e-04 2.518781e-05 8.417219e-06 8.164748e-07];
e6 = [5.975031e-01 7.077673e-02 2.346408e-03 1.767276e-05 7.360449e-08 3.727534e-11];
subplot(2,1,1)
loglog(h,e4, '-kd', 'LineWidth', 2);
hold on
loglog( [10 320], [400  10^(log10(400) - 4*(log10(320)-log10(10)))], 'b', 'LineWidth', 2)
grid on
ax = gca;
ax.FontSize = 15;
xticks([10 20 40 80 160 320])
yticks([10^-8 10^-4 10^-2])
xlabel('$N$')
ylabel('$E$')
subplot(2,1,2)
loglog(h,e6, '-kd', 'LineWidth', 2);
hold on
loglog( [10 320], [400  10^(log10(400) - 6*(log10(320)-log10(10)))], 'b', 'LineWidth', 2)
grid on
ax = gca;
ax.FontSize = 15;
xlabel('$N$')
ylabel('$E$')
xticks([10 20 40 80 160 320])
yticks([10^-8 10^-4 10^-2])