function [W] = GetW(x, parm)
K = parm.K;
zeta = parm.zeta;
W = cos(K*(x-zeta))/x;
end