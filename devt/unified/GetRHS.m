function [rhs] = GetRHS(x, parm)
K = parm.K;
rhs = exp(-K*x)*sin(K*x) + cos(K*x)*(1/(10*K*x))*exp(-K)*(-2*cos(K*(-2 + x))+ sin(K*(-2 + x)) - 5*sin(K*x) + ...
    exp(2*K)*(2*cos(K*(2 + x)) + 5*sin(K*x) - sin(K*(2 + x))));
%rhs = x^3*sin(K*x)-cos(K*x)^2*(2*K*(-3+2*K^2)*cos(2*K)+3*(1-2*K^2)*sin(2*K))/(8*K^4*x);
%rhs = sin(K*x) + (K-cos(K)*sin(K))*sin(K*x)*cos(K*x)/(K*x);
end