% This script is written in order to compare the computed
% Kernesl with the results by Sclavounos P. D. (1985) in
% "The unified slender-body theory: ship motions in waves"
% 16th Symposium on Naval Hydrodynamics, University of Michigan, Ann Arbour.
clc
clear variables
close all
%% Input parameters
L = 1;
Fr = 0.2;
kappa0 = 1/Fr^2;
xres = 300;
xvec = 2*linspace(-L/2,L/2, xres)'/L;
negin = find(xvec<0);
msgid ='MATLAB:integral:MaxIntervalCountReached';
warning('off', msgid)
reltol = 1e-3;
abstol = 1e-3;
%% tau = 0.2
tau = 0.2;
k1 = -kappa0/2*(1+2*tau+sqrt(1+4*tau));
k2 = -kappa0/2*(1+2*tau-sqrt(1+4*tau));
k3 = kappa0/2*(1-2*tau-sqrt(1-4*tau));
k4 = kappa0/2*(1-2*tau+sqrt(1-4*tau));
W234 = zeros(xres,1);
W11 = zeros(xres,1);
W24 = zeros(xres,1);
W23 = zeros(xres,1);
W12 = zeros(xres,1);
W2 = zeros(xres,1);
Wt = zeros(xres, 1);

for i=1:xres
    x = xvec(i);

    if x <0
        fun = @(m, x, tau, Fr) exp(-1i*m*x).*(1./sqrt(1-m.^2./((tau/Fr)^2+2*m*tau + m.^2*Fr^2).^2)-1);
        W11(i) = 1i*integral(@(m) fun(m,x, tau, Fr),-Inf,k1, 'AbsTol', abstol,'RelTol', reltol) - (exp(-1i*k1*x)-1)/x;
        fun = @(m, x, tau, Fr) exp(-1i*m*x).*(1./sqrt(1-m.^2./((tau/Fr)^2+2*m*tau + m.^2*Fr^2).^2));
        W12(i) = -1i*integral(@(m) fun(m,x, tau, Fr),k2,0, 'AbsTol', abstol,'RelTol', reltol);
    end

    fun = @(m, x, tau, Fr) exp(-1i*m*x).*(1./sqrt(1-m.^2./((tau/Fr)^2+2*m*tau + m.^2*Fr^2).^2));
    W23(i) = -1i/2*integral(@(m) fun(m,x, tau, Fr),0,k3, 'AbsTol', abstol,'RelTol', reltol);
    fun = @(m, x, tau, Fr) exp(-1i*m*x).*(1./sqrt(m.^2./((tau/Fr)^2+2*m*tau + m.^2*Fr^2).^2-1));
    W234(i) = 1/2*integral(@(m) fun(m,x, tau, Fr),k3,k4, 'AbsTol', abstol,'RelTol', reltol);
    fun = @(m, x, tau, Fr) exp(-1i*m*x).*(1./sqrt(1-m.^2./((tau/Fr)^2+2*m*tau + m.^2*Fr^2).^2)-1);
    W24(i) = -1i/2*integral(@(m) fun(m,x, tau, Fr),k4,Inf, 'AbsTol', abstol,'RelTol', reltol) - (exp(-1i*k4*x)-1)/(2*x);

    if x<0
        Wt(i) = W11(i)+W12(i) + W24(i)+W234(i)+W23(i);
    else
        Wt(i) = W24(i)+W234(i)+W23(i);
    end
end

fid1 = fopen('kernel1.dat');
fid2 = fopen('kernel2.dat');
fid3 = fopen('kernel3.dat');
fid4 = fopen('kernel4.dat');
fid5 = fopen('kernel5.dat');
fid6 = fopen('kernel6.dat');
fid7 = fopen('kernel7.dat');
fid8 = fopen('kernel8.dat');
Pdat1 = textscan(fid1, '%f %f');
Pdat2 = textscan(fid2, '%f %f');
Pdat3 = textscan(fid3, '%f %f');
Pdat4 = textscan(fid4, '%f %f');
Pdat5 = textscan(fid5, '%f %f');
Pdat6 = textscan(fid6, '%f %f');
Pdat7 = textscan(fid7, '%f %f');
Pdat8 = textscan(fid8, '%f %f');

set(0,'defaulttextinterpreter','latex')
set(0, 'defaultAxesTickLabelInterpreter','latex');
set(0, 'defaultLegendInterpreter','latex');

subplot(6,2,1)
plot(xvec(negin),imag(W11(negin)), 'r-', 'LineWidth',1.5)
ylabel('$\Im (W_1^1 ) L$');
ax = gca;
ax.FontSize = 11;
hold on
xlim([-1 1])
plot(Pdat1{1},Pdat1{2}, 'ko', 'MarkerSize',4)

subplot(6,2,3)
plot(xvec(negin),imag(W12(negin)), 'r-', 'LineWidth',1.5)
hold on
plot(Pdat4{1},Pdat4{2}, 'ko', 'MarkerSize',4)
xlim([-1 1])
ylim([-1.2,0])
ylabel('$\Im (W_1^2 ) L$');
ax = gca;
ax.FontSize = 11;

subplot(6,2,5)
plot(xvec,imag(W23), 'r-', 'LineWidth',1.5)
hold on
plot(Pdat5{1},Pdat5{2}, 'ko', 'MarkerSize',4)
ylabel('$\Im (W_2^3 ) L$');
ylim([-2.5 1])
ax = gca;
ax.FontSize = 11;

subplot(6,2,7)
plot(xvec,imag(W234), 'r-', 'LineWidth',1.5)
hold on
plot(Pdat2{1},Pdat2{2}, 'ko', 'MarkerSize',4)
xlim([-1 1])
ylabel('$\Im (W_2^{34} ) L$');
ylim([-15 15])
ax = gca;
ax.FontSize = 11;

subplot(6,2,9)
plot(xvec,imag(W24), 'r-', 'LineWidth',1.5)
hold on
plot(Pdat3{1},Pdat3{2}, 'ko', 'MarkerSize',4)
xlim([-1 1])
ylabel('$\Im (W_2^{4} ) L$');
ylim([-8 10])
ax = gca;
ax.FontSize = 11;

subplot(6,2,11)
plot(xvec,imag(Wt), 'r-', 'LineWidth',1.5)
xlim([-1 1])
ylabel('$\Im (W _1 + W_2 ) L$');
ylim([-30 40])
ax = gca;
ax.FontSize = 11;
%% tau = 0.8
tau = 0.8;
kappa0 = 1/Fr^2;
k1 = -kappa0/2*(1+2*tau+sqrt(1+4*tau));
k2 = -kappa0/2*(1+2*tau-sqrt(1+4*tau));

for i=1:xres
    x = xvec(i);

    if x <0
        fun = @(m, x, tau, Fr) exp(-1i*m*x).*(1./sqrt(1-m.^2./((tau/Fr)^2+2*m*tau + m.^2*Fr^2).^2)-1);
        W11(i) = 1i*integral(@(m) fun(m,x, tau, Fr),-Inf,k1, 'AbsTol', abstol,'RelTol', reltol) - (exp(-1i*k1*x)-1)/x;
        fun = @(m, x, tau, Fr) exp(-1i*m*x).*(1./sqrt(1-m.^2./((tau/Fr)^2+2*m*tau + m.^2*Fr^2).^2));
        W12(i) = -1i*integral(@(m) fun(m,x, tau, Fr),k2,0, 'AbsTol', abstol,'RelTol', reltol);
    end

    fun = @(m, x, tau, Fr) exp(-1i*m*x).*(1./sqrt(1-m.^2./((tau/Fr)^2+2*m*tau + m.^2*Fr^2).^2)-1);
    W2(i) = -1i/2*integral(@(m) fun(m,x, tau, Fr),0,Inf, 'AbsTol', abstol,'RelTol', reltol);

    if x<0
        Wt(i) = W11(i)+W12(i)+W2(i);
    else
        Wt(i) = W2(i);
    end

end

subplot(6,2,2)
plot(xvec(negin),imag(W11(negin)), 'r-', 'LineWidth',1.5)
xlim([-1 1])
hold on
plot(Pdat6{1},Pdat6{2}, 'ko', 'MarkerSize',4)
ylim([-50 30])
ylabel('$\Im (W_1^{1} ) L$');
ax = gca;
ax.FontSize = 11;

subplot(6,2,4)
plot(xvec(negin),imag(W12(negin)), 'r-', 'LineWidth',1.5)
hold on
plot(Pdat7{1},Pdat7{2}, 'ko', 'MarkerSize',4)
ylabel('$\Im (W_1^{2} ) L$');
ax = gca;
ax.FontSize = 11;
xlim([-1 1])
ylim([-10 4])

subplot(6,2,8)
plot(xvec,imag(W2), 'r-', 'LineWidth',1.5)
hold on
plot(Pdat8{1},Pdat8{2}, 'ko', 'MarkerSize',4)
xlim([-1 1])
ylim([-3 0.5])
ylabel('$\Im (W_2 ) L$');
ax = gca;
ax.FontSize = 11;

subplot(6,2,12)
plot(xvec,imag(Wt), 'r-', 'LineWidth',1.5)
xlim([-1 1])
ylim([-60 20])
ylabel('$\Im (W _1 + W_2 ) L$');
ax = gca;
ax.FontSize = 11;
