
clearvars;
close all
clc
ifig=0;
set(0,'defaulttextinterpreter','latex')
set(0, 'defaultAxesTickLabelInterpreter','latex');
set(0, 'defaultLegendInterpreter','latex');

addpath(genpath('../../src'))

%%
%
%
L     = 2.5;
B     = 0.5;
Draft = 0.175;
tmp   = load('points.txt');
xst   = unique(tmp(:,1),'stable'); nst=length(xst);  % The station coordinates
%
% Loop over the stations and extract the section points. Flip the z-axis so
% that it runs from the mean free surface (the waterline) to the keel and
% remove the extra point along the centerline, then re-distribute the
% points so that they are nearly uniformly spaced  in arc-lenth along the
% station.
%
S = repmat(struct('x',{}), 1, nst);
pres = 25;
for i=1:nst
    S(i).x=flipud(tmp(tmp(:,1)==xst(i,1),2:3));
    S(i).x(:,2)=-(S(i).x(:,2)-Draft);
    S(i).x = GenerateMesh(S(i).x, pres, 1e-6, 0);
    fprintf('%s %d %s %d \n', 'station re-meshed:' ,i, 'of', nst);
    S(i).Nv=length(S(i).x); % Number of vertices for this station
end
%
%
%% Move the coordinate system to the center of floatation and add bow & stern dummy stations
%
Xg = L/2;
xst = xst-Xg;
for i=1:length(xst)
    S(i).xst = xst(i);
end
%
% Add in the bow and stern stations as dummy stations where the local
% solutions will just be zero for the Wigley hull. If a station has Nv=0,
% then it is considered a dummy station in the strip theory solver.
%
% Note that the stations should run from stern to bow.
%
xst=[-L/2;xst;L/2]; nst = nst+2;
%
S1(1)=S(1); S1(1).Nv=0;
S3(1)=S1(1); S3(1).x(:,1)=0;
S3(2:nst-1)=S(1:nst-2);
S3(nst)=S1; S3(nst).x(:,1)=0;
S=S3;
xst = num2cell(xst);
[S.xst] = xst{:};

%
%% Set up the speeds and encounter frequencies to solve for
%
% The density, gravity and length-scale for non-dimensionalization.
%
ulen=L; g=9.81; rho=1024;
%
% The speed and encounter frequency vectors to solve for.
%
% The STF function is set up to accept a vector of speeds, for any given
% heading angle, but this script only checks one at a time. TiMIT heading
% angles which can be compared to are beta=180, 160, 140.
%

Fn=[0 0.1]; beta=0*pi/180;
%Fn=0.2; beta=180*pi/180;
% Fn=0.3; beta=180*pi/180;
KochinType='2D';  % Choose the Kochin function type: 2D or 3D

cosB=cos(beta);
U=sqrt(g*ulen)*Fn;

nf=100;
lamlMin = 0.25;
lamlMax = 5;
om0Min = sqrt(2*pi*g/(lamlMax*L));
om0Max = sqrt(2*pi*g/(lamlMin*L));
om0 = (linspace(om0Min, om0Max, nf))';

%
% The inertia and restoring force coefficients
%
Zg   = -0.031;
rx   = 0.4*B;      % m
ry   = 0.236*L;   % m
RGyr=zeros(3);
RGyr(1,1)=rx; RGyr(2,2)=ry; RGyr(3,3)=ry;

%
% Call the strip theory solver
%
[Sol,S]=DTU_StripTheorySolver(S,U,beta,om0,g,ulen,Zg,RGyr,KochinType, 'all');
%%
clc
close all

set(0,'defaulttextinterpreter','latex')
set(0, 'defaultAxesTickLabelInterpreter','latex');
set(0, 'defaultLegendInterpreter','latex');

ind = 2;
ome = om0 - om0.^2/g*U(ind)*cosB;
nu = ome.^2/g;
nu0 = om0.^2/g;
lam0 = 2*pi./(nu0*ulen);
Fjk=Sol(1).Fjk; XjD=Sol(1).XjD; xi=Sol(1).xi;
PlotHull(S);

ifig=ifig+1; figure(ifig);clf;
plot(lam0,Sol(ind).RwMaruo(:,1),lam0,Sol(ind).RwSalvesen(:,1));

xlabel('$\lambda/L$');
ylabel('$R_w/(\rho g A L^2)$');
legend('STF(Maruo)','STF(Salvesen)');
grid on; xlim([0,3]);
title(['Wigley hull I, $F_n=$',num2str(Fn),', $\beta=$',num2str(180*beta/pi)]);
PlotHull(S);

