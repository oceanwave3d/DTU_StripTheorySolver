
clearvars
close all
clc
ifig=0;
set(0, 'defaulttextinterpreter','latex')
set(0, 'defaultAxesTickLabelInterpreter','latex');
set(0, 'defaultLegendInterpreter','latex');

addpath(genpath('../../src'))

%%
L     = 320;
B     = 58;
Draft = 20.8;
tmp   = load('points.txt');
xst   = uniquetol(tmp(:,1),1e-4); nst=length(xst);  % The station coordinates
S = repmat(struct('x',{}), 1, nst);
pres = 30;
for i=1:nst
    S(i).x = tmp(abs(tmp(:,1)-xst(i,1))<1e-3,2:3);
    [z,ind] = sort(S(i).x(:,2),'descend');
    y = S(i).x(ind,1);
    S(i).x(:,1) = y;
    S(i).x(:,2) = -z + Draft;
    S(i).x = GenerateMesh(S(i).x, pres, 1e-4, 0);
    fprintf('%s %d %s %d \n', 'station re-meshed:' ,i, 'of', nst);
    S(i).Nv=length(S(i).x); % Number of vertices for this station
end
%
%
%% Move the coordinate system to the center of floatation and add bow & stern dummy stations
%
Xg = 171.1;
xst = xst-Xg;
%
% Add in the bow station as a dummy station where the local
% solutions will just be zero. If a station has Nv=0,
% then it is considered a dummy station in the strip theory solver.
%
% Note that the stations should run from stern to bow.
%
xst=[xst;xst(end)+0.5*(xst(end)-xst(end-1))]; nst = nst+1;
S1(1)=S(end); S1(1).Nv=0; S1(1).x(:,1)=0;
S3(1:nst-1)=S(1:nst-1); S3(nst)=S1;
S=S3;

%
%
%S(nst)=S(nst-1); S(nst).Nv=0;
xst = num2cell(xst);
[S.xst] = xst{:};
%PlotHull(S);
%
%% Set up the speeds and encounter frequencies to solve for
%
% The density, gravity and length-scale for non-dimensionalization.
%
ulen=L; g=9.81; rho=1024;
%
% The speed and encounter frequency vectors to solve for.
%
% The STF function is set up to accept a vector of speeds, for any given
% heading angle, but this script only checks one at a time. WAMIT heading
% angles which can be compared to are beta=180.
%

Fn=0; beta=180*pi/180;

KochinType='2D';  % Choose the Kochin function type: 2D or 3D

cosB=cos(beta);
U=sqrt(g*ulen)*Fn;

nf=100;
lamlMin = 0.25;
lamlMax = 4;
om0Min = sqrt(2*pi*g/(lamlMax*L));
om0Max = sqrt(2*pi*g/(lamlMin*L));
om0 = (linspace(om0Min, om0Max, nf))';

Zg   = -1.039;
rx   = 23.2;      % m
ry   = 80;        % m
RGyr=zeros(3); RGyr(1,1)=rx; RGyr(2,2)=ry; RGyr(3,3)=ry;

%
% Call the strip theory solver
%
[Sol,S]=DTU_StripTheorySolver(S,U,beta,om0,g,ulen,Zg,RGyr,KochinType, 'all');
%%
ind = 1;
clc
close all

set(0,'defaulttextinterpreter','latex')
set(0, 'defaultAxesTickLabelInterpreter','latex');
set(0, 'defaultLegendInterpreter','latex');

om0nd=om0*sqrt(L/g);
om = om0 - om0.^2/g*U(ind)*cosB;
omnd = om*sqrt(L/g);
nu0 = om0.^2/g;
lam0 = 2*pi./(nu0*ulen);

Fjk=Sol(ind).Fjk; XjD=Sol(ind).XjD; xi=Sol(ind).xi;

if Fn(ind)==0
    nH=1; Order='TM';
    fname='Fn0/kvlcc2.1';
    [T,AW,BW]=ReadWAMIT1file(fname,nH,Order);
    omW=2*pi./T*sqrt(ulen/g);
    %
    % Remove omega=infty
    %
    omW(1)=0; omW(2)=[];
    AW(2,:,:)=[];
    BW(2,:,:)=[];
    
    ifig=ifig+1; figure(ifig);clf;
    plot(omnd,real(Fjk(:,3,3)),omW,AW(:,3,3),omnd,-imag(Fjk(:,3,3)),omW,BW(:,3,3))
    xlabel('$\omega\sqrt{L/g}$');
    legend('$A_{33}/(\rho L^3)$, STF','$A_{33}/(\rho L^3)$, WAMIT',...
        '$B_{33}/(\rho L^3\omega)$, STF','$B_{33}/(\rho L^3\omega)$, WAMIT');
    title(['KVLCC2 at $F_n=$',num2str(Fn)]);
    grid on
    
    ifig=ifig+1; figure(ifig);clf;
    plot(omnd,real(Fjk(:,5,5)),omW,AW(:,5,5),omnd,-imag(Fjk(:,5,5)),omW,BW(:,5,5))
    xlabel('$\omega\sqrt{L/g}$');
    legend('$A_{55}/(\rho L^3)$, STF','$A_{55}/(\rho L^3)$, WAMIT',...
        '$B_{55}/(\rho L^3\omega)$, STF','$B_{55}/(\rho L^3\omega)$, WAMIT');
    title(['KVLCC2 at $F_n=$',num2str(Fn)]);
    grid on
    %
    % Diffraction forces
    %
    fname='Fn0/kvlcc2.3';
    [~,~,XMagW,XPhaseW,XRW,XIW]=ReadWAMIT234file(fname,nH,Order);
    
    ifig=ifig+1; figure(ifig);clf;
    plot(om0nd,real(XjD(:,3)),omW(2:end),XRW(:,3,1),om0nd,imag(XjD(:,3)),omW(2:end),XIW(:,3,1))
    xlabel('$\omega_0\sqrt{L/g}$');
    legend('$\Re\{X_{3}\}/(\rho g L^2 A)$, STF','$\Re\{X_{3}\}/(\rho g L^2 A)$, WAMIT',...
        '$\Im\{X_{3}\}/(\rho g L^2 A)$, STF','$\Im\{X_{3}\}/(\rho g L^2 A)$, WAMIT');
    grid on; xlim([0,7])
    title(['KVLCC2 at $F_n=$',num2str(Fn),', $\beta=$',num2str(180*beta/pi)]);
    
    ifig=ifig+1; figure(ifig);clf;
    plot(om0nd,real(XjD(:,5)),omW(2:end),XRW(:,5,1),om0nd,imag(XjD(:,5)),omW(2:end),XIW(:,5,1))
    xlabel('$\omega_0\sqrt{L/g}$');
    legend('$\Re\{X_{5}\}/(\rho g L^2 A)$, STF','$\Re\{X_{5}\}/(\rho g L^2 A)$, WAMIT',...
        '$\Im\{X_{5}\}/(\rho g L^2 A)$, STF','$\Im\{X_{5}\}/(\rho g L^2 A)$, WAMIT');
    grid on; xlim([0,7])
    title(['KVLCC2 at $F_n=$',num2str(Fn),', $\beta=$',num2str(180*beta/pi)]);
    %
    % RAOs
    %
    fname='Fn0/kvlcc2.4';
    [betaW,~,RAOMagW,RAOPhaseW,~,~]=ReadWAMIT234file(fname,nH,Order);
    
    ifig=ifig+1; figure(ifig);clf;
    plot(omnd,abs(xi(:,3)),omW(2:end),RAOMagW(:,3),omnd,abs(xi(:,5)),...
        omW(2:end),RAOMagW(:,5))
    xlabel('$\omega\sqrt{L/g}$');
    legend('$|\xi_{3}|/A$, STF','$|\xi_{3}|/A$, WAMIT',...
        '$|\xi_{5}|/(A/L)$, STF','$|\xi_{5}|/(A/L)$, WAMIT');
    grid on; xlim([0,7]);
    title(['KVLCC2 at $F_n=$',num2str(Fn),', $\beta=$',num2str(180*beta/pi)]);
    
    %
    fname='Fn0/kvlcc2.8';
    [beta1,beta2,T,DriftMagW,DriftPhaseW,XR,XI]=ReadWAMIT89file(fname,nH,Order);
    
    ifig=ifig+1; figure(ifig);clf;
    plot(lam0,Sol(ind).RwMaruo(:,1),lam0,Sol(ind).RwSalvesen(:,1),2*pi./(omW(2:end).^2),DriftMagW(:,1,1,1));
    
    xlabel('$\lambda/L$');
    ylabel('$R_w/(\rho g A L)$');
    legend('STF(Maruo)','STF(Salvesen)','WAMIT');
    grid on; xlim([0,3]);
    title(['KVLCC2, $F_n=$',num2str(Fn),', $\beta=$',num2str(180*beta/pi)])
end