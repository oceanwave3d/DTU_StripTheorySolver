%
% RIOS bulker hull.
%
clearvars
close all
clc

addpath(genpath('../../src'))

%%
L     = 2;
B     = 1/3;
Draft = 0.1067;
tmp   = load('points.txt');
xst   = uniquetol(tmp(:,1),1e-3); nst=length(xst);  % The station coordinates

S = repmat(struct('x',{}), 1, nst);

pres = 20;
for i=1:nst
    S(i).x = tmp(abs(tmp(:,1)-xst(i,1))<1e-3,2:3);
    [z,ind] = sort(S(i).x(:,2),'descend');
    y = S(i).x(ind,1);
    y(end) = 0;
    S(i).x(:,1) = y;
    S(i).x(:,2) = -z+Draft;
    S(i).x = GenerateMesh(S(i).x, pres, 1e-5, 0);
    fprintf('%s %d %s %d \n', 'station re-meshed:' ,i, 'of', nst);
    S(i).Nv=length(S(i).x); % Number of vertices for this station
end
%
%
%% Move the coordinate system to the center of floatation and add bow & stern dummy stations
%
Xg = 0.42533e-1;
xst = xst-Xg;

%
% Add in the bow station as a dummy station where the local
% solutions will just be zero. If a station has Nv=0,
% then it is considered a dummy station in the strip theory solver.
%
% Note that the stations should run from stern to bow.
%hbb I've put in a dummy station at the bow here.
%
xst=[xst;xst(end)+1/2*(xst(end)-xst(end-1))]; nst = nst+1;
S(nst)=S(nst-1); S(nst).Nv=0; S(nst).x(:,1)=0;

xst = num2cell(xst);
[S.xst] = xst{:};

%
%% Set up the speeds and encounter frequencies to solve for
%
% The density, gravity and length-scale for non-dimensionalization.
%
ulen=L; g=9.81; rho=1024;
%
% The speed and encounter frequency vectors to solve for.
%
% The STF function is set up to accept a vector of speeds, for any given
% heading angle, but this script only checks one at a time. TiMIT heading
% angles which can be compared to are beta=180, 160, 140.
%

Fn=[0 0.18]; beta=180*pi/180;
KochinType='2D';  % Choose the Kochin function type: 2D or 3D

cosB=cos(beta);
U=sqrt(g*ulen)*Fn;
nf=80;
lamlMin = 0.25;
lamlMax = 4;
om0Min = sqrt(2*pi*g/(lamlMax*L));
om0Max = sqrt(2*pi*g/(lamlMin*L));
om0 = (linspace(om0Min, om0Max, nf))';
%
% The vcg and radii of gyration
%
Zg   = -0.16667e-1;
rx   = 0.35*B;           % m
ry   = 0.25*ulen;        % m
RGyr=zeros(3);
RGyr(1,1)=rx; RGyr(2,2)=ry; RGyr(3,3)=ry;

[Sol,S]=DTU_StripTheorySolver(S,U,beta,om0,g,ulen,Zg,RGyr,KochinType,'all');
%%
clc
close all;
ind = 2;
Fjk = Sol(ind).Fjk;
XjD = Sol(ind).XjD;
xi  = Sol(ind).xi;
VOL = 0.56887e-01;
ome = om0 - om0.^2/g*U(ind)*cosB;
nu      = ome.^2/g;
nu0     = om0.^2/g;
lam0 = 2*pi./(nu0*ulen);

ifig=0;
set(0, 'defaulttextinterpreter','latex')
set(0, 'defaultAxesTickLabelInterpreter','latex');
set(0, 'defaultLegendInterpreter','latex');

A33 =  real(Fjk(:,3,3))*ulen^3/VOL;
B33 = -imag(Fjk(:,3,3))*ulen^3/VOL;
A55 =  real(Fjk(:,5,5))*ulen^3/VOL;
B55 = -imag(Fjk(:,5,5))*ulen^3/VOL;
A53 =  real(Fjk(:,5,3))*ulen^3/VOL;
B53 = -imag(Fjk(:,5,3))*ulen^3/VOL;
A35 =  real(Fjk(:,3,5))*ulen^3/VOL;
B35 = -imag(Fjk(:,3,5))*ulen^3/VOL;
X3A =  abs((XjD(:,3))) *ulen/B;
X3P =  atan2(imag(XjD(:,3)), real(XjD(:,3)))*180/pi;
X5A =  abs((XjD(:,5)))*ulen/B;
X5P =  atan2(imag(XjD(:,5)), real(XjD(:,5)))*180/pi;
R3A =  abs(xi(:,3));
R3P =  atan2(imag(xi(:,3)), real(xi(:,3)))*180/pi;
R5A =  abs(xi(:,5))./(2*pi./lam0);
R5P =  atan2(imag(xi(:,5)), real(xi(:,5)))*180/pi;

eA33 = load('./Exp/Hydro/a33.dat');
eB33 = load('./Exp/Hydro/b33.dat');
eA55 = load('./Exp/Hydro/a55.dat');
eB55 = load('./Exp/Hydro/b55.dat');
eA53 = load('./Exp/Hydro/a53.dat');
eB53 = load('./Exp/Hydro/b53.dat');
eA35 = load('./Exp/Hydro/a35.dat');
eB35 = load('./Exp/Hydro/b35.dat');
eXP3 = load('./Exp/Force_Motion/exp_force_heave_phase.txt');
eXA3 = load('./Exp/Force_Motion/exp_force_heave_amp.txt');
eXP5 = load('./Exp/Force_Motion/exp_force_pitch_phase.txt');
eXA5 = load('./Exp/Force_Motion/exp_force_pitch_amp.txt');
eRP3 = load('./Exp/Force_Motion/exp_rao_heave_phase.txt');
eRA3 = load('./Exp/Force_Motion/exp_rao_heave_amp.txt');
eRP5 = load('./Exp/Force_Motion/exp_rao_pitch_phase.txt');
eRA5 = load('./Exp/Force_Motion/exp_rao_pitch_amp.txt');
AR_NST = load('./Exp/Drift/NST.txt');
AR_EUT = load('./Exp/Drift/EUT.txt');
AR_PIN = load('./Exp/Drift/kashiwagi/pressure_integration.txt');

ifig=ifig+1; figure(ifig);clf;
subplot(2,1,1)
plot(nu*ulen, A33, 'k-', 'LineWidth', 2);
hold on
plot(eA33(:,1), eA33(:,2), 'ro','MarkerSize', 5, 'LineWidth', 2);
xlabel('$\nu L_{pp}$');
ylabel('$A_{33}$');
xlim([0 60]);
ylim([-0.5 3])
set(gca, 'FontSize', 15);
subplot(2,1,2)
plot(nu*ulen, B33, 'k-', 'LineWidth', 2);
hold on
plot(eB33(:,1), eB33(:,2), 'ro','MarkerSize', 5, 'LineWidth', 2);
xlabel('$\nu L_{pp}$');
xlim([0 60]);
ylim([-0.5 3])
set(gca, 'FontSize', 15);
ylabel('$B_{33}$');
% ---------------------
ifig=ifig+1; figure(ifig);clf;
subplot(2,1,1)
plot(nu*ulen, A55, 'k-', 'LineWidth', 2);
hold on
plot(eA55(:,1), eA55(:,2), 'ro','MarkerSize', 5, 'LineWidth', 2);
xlabel('$\nu L_{pp}$');
ylabel('$A_{55}$');
xlim([0 60]);
ylim([-0.05 .3])
set(gca, 'FontSize', 15);
subplot(2,1,2)
plot(nu*ulen, B55, 'k-', 'LineWidth', 2);
hold on
plot(eB55(:,1), eB55(:,2), 'ro','MarkerSize', 5, 'LineWidth', 2);
xlabel('$\nu L_{pp}$');
ylabel('$B_{55}$');
xlim([0 60]);
ylim([-0.05 .3])
set(gca, 'FontSize', 15);
% ---------------------
ifig=ifig+1; figure(ifig);clf;
subplot(2,1,1)
plot(nu*ulen, A53, 'k-', 'LineWidth', 2);
hold on
plot(eA53(:,1), eA53(:,2), 'ro','MarkerSize', 5, 'LineWidth', 2);
xlabel('$\nu L_{pp}$');
ylabel('$A_{53}$');
xlim([0 60]);
ylim([-0.2 .4])
set(gca, 'FontSize', 15);
subplot(2,1,2)
plot(nu*ulen, B53, 'k-', 'LineWidth', 2);
hold on
plot(eB53(:,1), eB53(:,2), 'ro','MarkerSize', 5, 'LineWidth', 2);
xlabel('$\nu L_{pp}$');
ylabel('$B_{53}$');
xlim([0 60]);
ylim([-0.4 .4])
set(gca, 'FontSize', 15);
% ---------------------
ifig=ifig+1; figure(ifig);clf;
subplot(2,1,1)
plot(nu*ulen, A35, 'k-', 'LineWidth', 2);
hold on
plot(eA35(:,1), eA35(:,2), 'ro','MarkerSize', 5, 'LineWidth', 2);
xlabel('$\nu L_{pp}$');
ylabel('$A_{35}$');
xlim([0 60]);
ylim([-0.6 .6])
set(gca, 'FontSize', 15);
subplot(2,1,2)
plot(nu*ulen, B35, 'k-', 'LineWidth', 2);
hold on
plot(eB35(:,1), eB35(:,2), 'ro','MarkerSize', 5, 'LineWidth', 2);
xlabel('$\nu L_{pp}$');
ylabel('$B_{35}$');
xlim([0 60]);
ylim([-0.6 .6])
set(gca, 'FontSize', 15);
% ---------------------
ifig=ifig+1; figure(ifig);clf;
subplot(2,1,1)
plot(lam0, X3A, 'k-', 'LineWidth', 2);
hold on
plot(eXA3(:,1), eXA3(:,2), 'ro','MarkerSize', 5, 'LineWidth', 2);
xlabel('$\lambda/L_{pp}$');
ylabel('$X_3$');
xlim([0 5]);
ylim([0 1])
set(gca, 'FontSize', 15);
subplot(2,1,2)
plot(lam0, X3P, 'k-', 'LineWidth', 2);
hold on
plot(eXP3(:,1), eXP3(:,2), 'ro','MarkerSize', 5, 'LineWidth', 2);
xlabel('$\lambda/L_{pp}$');
ylabel('$X_3$ Phase');
xlim([0 5]);
ylim([-180 180])
set(gca, 'FontSize', 15);
% ---------------------
ifig=ifig+1; figure(ifig);clf;
subplot(2,1,1)
plot(lam0, X5A, 'k-', 'LineWidth', 2);
hold on
plot(eXA5(:,1), eXA5(:,2), 'ro','MarkerSize', 5, 'LineWidth', 2);
xlabel('$\lambda/L_{pp}$');
ylabel('$X_5$');
xlim([0 5]);
ylim([0 .2])
set(gca, 'FontSize', 15);
subplot(2,1,2)
plot(lam0, X5P, 'k-', 'LineWidth', 2);
hold on
plot(eXP5(:,1), eXP5(:,2), 'ro','MarkerSize', 5, 'LineWidth', 2);
xlabel('$\lambda/L_{pp}$');
ylabel('$X_5$ Phase');
xlim([0 5]);
ylim([-180 180])
set(gca, 'FontSize', 15);
% ---------------------
ifig=ifig+1; figure(ifig);clf;
subplot(2,1,1)
plot(lam0, R3A, 'k-', 'LineWidth', 2);
hold on
plot(eRA3(:,1), eRA3(:,2), 'ro','MarkerSize', 5, 'LineWidth', 2);
xlabel('$\lambda/L_{pp}$');
ylabel('$\xi_3$');
xlim([0 5]);
ylim([0 2])
set(gca, 'FontSize', 15);
subplot(2,1,2)
plot(lam0, R3P, 'k-', 'LineWidth', 2);
hold on
plot(eRP3(:,1), eRP3(:,2), 'ro','MarkerSize', 5, 'LineWidth', 2);
xlabel('$\lambda/L_{pp}$');
ylabel('$\xi_3$ Phase');
xlim([0 5]);
ylim([-180 180])
set(gca, 'FontSize', 15);
% ---------------------
ifig=ifig+1; figure(ifig);clf;
subplot(2,1,1)
plot(lam0, R5A, 'k-', 'LineWidth', 2);
hold on
plot(eRA5(:,1), eRA5(:,2), 'ro','MarkerSize', 5, 'LineWidth', 2);
xlabel('$\lambda/L_{pp}$');
ylabel('$\xi_5$');
xlim([0 5]);
ylim([0 2])
set(gca, 'FontSize', 15);
subplot(2,1,2)
plot(lam0, R5P, 'k-', 'LineWidth', 2);
hold on
plot(eRP5(:,1), eRP5(:,2), 'ro','MarkerSize', 5, 'LineWidth', 2);
xlabel('$\lambda/L_{pp}$');
ylabel('$\xi_5$ Phase');
xlim([0 5]);
ylim([-180 180])
set(gca, 'FontSize', 15);
% ---------------------
ifig=ifig+1; figure(ifig);clf;
plot(lam0,Sol(ind).RwMaruo(:,1)*(ulen/B)^2,'k-', 'LineWidth', 2);
hold on
plot(lam0,Sol(ind).RwSalvesen(:,1)*(ulen/B)^2,'m-');
plot(AR_NST(:,1), AR_NST(:,2), 'b-.','MarkerSize', 5, 'LineWidth', 2);
plot(AR_EUT(:,1), AR_EUT(:,2), 'k-.','MarkerSize', 5, 'LineWidth', 2);
plot(AR_PIN(:,1), AR_PIN(:,2), 'ro','MarkerSize', 5, 'LineWidth', 2);
xlim([0 3])
xlabel('$\lambda/L_{pp}$');
ylabel('$R_w L_{pp}/ \rho g B^2 A^2$');
legend('STF(Maruo)','STF(Salvesen)', ...
    'NST(Kashiwagi)',  ...
    'EUT(Kashiwagi)', 'Exp.(Kashiwagi IWWWFB2019)',...
    'Location','NorthOutside');
set(gca, 'FontSize', 15);
% Plot the hull lines
PlotHull(S);