%
% RIOS bulker hull.
%
clearvars
close all
clc

addpath(genpath('../../src'))

%%
L     = 2;
B     = 1/3;
Draft = 0.1067;
tmp   = load('points.txt');
xst   = uniquetol(tmp(:,1),1e-3); nst=length(xst);  % The station coordinates
S = repmat(struct('x',{}), 1, nst);
pres = 20;
for i=1:nst
    S(i).x = tmp(abs(tmp(:,1)-xst(i,1))<1e-3,2:3);
    [z,ind] = sort(S(i).x(:,2),'descend');
    y = S(i).x(ind,1);
    y(end) = 0;
    S(i).x(:,1) = y;
    S(i).x(:,2) = -z+Draft;
    S(i).x = GenerateMesh(S(i).x, pres, 1e-5, 0);
    fprintf('%s %d %s %d \n', 'station re-meshed:' ,i, 'of', nst);
    S(i).Nv=length(S(i).x); % Number of vertices for this station
end
%
%
%% Move the coordinate system to the center of floatation and add bow & stern dummy stations
%
Xg = 0.42533e-1;
xst = xst-Xg;

%
% Add in the bow station as a dummy station where the local
% solutions will just be zero. If a station has Nv=0,
% then it is considered a dummy station in the strip theory solver.
%
% Note that the stations should run from stern to bow.
%hbb I've put in a dummy station at the bow here.
%
xst=[xst;xst(end)+1/2*(xst(end)-xst(end-1))]; nst = nst+1;
S(nst)=S(nst-1); S(nst).Nv=0; S(nst).x(:,1)=0;

xst = num2cell(xst);
[S.xst] = xst{:};

%
%% Set up the speeds and encounter frequencies to solve for
%
% The density, gravity and length-scale for non-dimensionalization.
%
ulen=L; g=9.81; rho=1024;
%
% The speed and encounter frequency vectors to solve for.
%
% The STF function is set up to accept a vector of speeds, for any given
% heading angle, but this script only checks one at a time. TiMIT heading
% angles which can be compared to are beta=180, 160, 140.
%

Fn=0.18; beta=0*pi/180;
KochinType='2D';  % Choose the Kochin function type: 2D or 3D

cosB=cos(beta);
U=sqrt(g*ulen)*Fn;

nf=80;
lamlMin = 0.35;
lamlMax = 5;
om0Min = sqrt(2*pi*g/(lamlMax*L));
om0Max = sqrt(2*pi*g/(lamlMin*L));
om0 = (linspace(om0Min, om0Max, nf))';

%
% The vcg and radii of gyration
%
Zg = -0.16667e-1;
rx = 0.35*B;           % m
ry = 0.25*ulen;        % m
RGyr=zeros(3);
RGyr(1,1)=rx; RGyr(2,2)=ry; RGyr(3,3)=ry;

[Sol,S]=DTU_StripTheorySolver(S,U,beta,om0,g,ulen,Zg,RGyr,KochinType,'all');
%%
clc
close all
ind = 1;
ome = om0 - om0.^2/g*U(ind)*cosB;
nu  = ome.^2/g;
nu0 = om0.^2/g;
lam0 = 2*pi./(nu0*ulen);
Fjk = Sol(ind).Fjk;
XjD = Sol(ind).XjD;
xi  = Sol(ind).xi;

VOL = 0.56887e-01;
ifig=0;
set(0, 'defaulttextinterpreter','latex')
set(0, 'defaultAxesTickLabelInterpreter','latex');
set(0, 'defaultLegendInterpreter','latex');

X3A =  abs((XjD(:,3))) *ulen/B;
X3P =  atan2(imag(XjD(:,3)), real(XjD(:,3)))*180/pi;
X5A =  abs((XjD(:,5)))*ulen/B;
X5P =  atan2(imag(XjD(:,5)), real(XjD(:,5)))*180/pi;
R3A =  abs(xi(:,3));
R3P =  atan2(imag(xi(:,3)), real(xi(:,3)))*180/pi; 
R5A =  abs(xi(:,5))./(2*pi./lam0);
R5P =  atan2(imag(xi(:,5)), real(xi(:,5)))*180/pi;

IWP3 = load('./Exp/Force_Motion/iwashita_force_heave_phase.txt');
IWA3 = load('./Exp/Force_Motion/iwashita_force_heave_amp.txt');
IWP5 = load('./Exp/Force_Motion/iwashita_force_pitch_phase.txt');
IWA5 = load('./Exp/Force_Motion/iwashita_force_pitch_amp.txt');

TiFrcP = load('./Exp/Force_Motion/TiMIT_force_phase_fol.dat');
TiFrcA = load('./Exp/Force_Motion/TiMIT_force_mag_fol.dat');
TiRaoP = load('./Exp/Force_Motion/TiMIT_rao_phase_fol.dat');
TiRaoA = load('./Exp/Force_Motion/TiMIT_rao_mag_fol.dat');

eRP3 = load('./Exp/Force_Motion/exp_fol_rao_heave_phase.txt');
eRA3 = load('./Exp/Force_Motion/exp_fol_rao_heave_amp.txt');
eRP5 = load('./Exp/Force_Motion/exp_fol_rao_pitch_phase.txt');
eRA5 = load('./Exp/Force_Motion/exp_fol_rao_pitch_amp.txt');

% ---------------------
ifig=ifig+1; figure(ifig);clf;
subplot(2,1,1)
plot(lam0, X3A, 'k-', 'LineWidth', 1.5);
hold on
plot(TiFrcA(:,1), TiFrcA(:,3), 'g-', 'LineWidth', 1.5);
plot(IWA3(:,1), IWA3(:,2), 'b-','MarkerSize', 5, 'LineWidth', 1.5);
xlabel('$\lambda/L_{pp}$');
ylabel('$X_3$');
legend('STF','TiMIT','Exp')
xlim([0 5]);
ylim([0 1])
set(gca, 'FontSize', 15);
subplot(2,1,2)
plot(lam0, X3P, 'k-', 'LineWidth', 1.5);
hold on
plot(TiFrcP(:,1), TiFrcP(:,3), 'g-', 'LineWidth', 1.5);
hold on 
plot(IWP3(:,1), IWP3(:,2), 'b-','MarkerSize', 5, 'LineWidth', 1.5);
xlabel('$\lambda/L_{pp}$');
ylabel('$X_3$ Phase');
xlim([0 5]);
ylim([-180 180])
set(gca, 'FontSize', 15);
% ---------------------
ifig=ifig+1; figure(ifig);clf;
subplot(2,1,1)
plot(lam0, X5A, 'k-', 'LineWidth', 1.5);
hold on
plot(TiFrcA(:,1), TiFrcA(:,4), 'g-', 'LineWidth', 1.5);
hold on 
plot(IWA5(:,1), IWA5(:,2), 'b-','MarkerSize', 5, 'LineWidth', 1.5);
xlabel('$\lambda/L_{pp}$');
ylabel('$X_5$');
legend('STF','TiMIT','Exp')
xlim([0 5]);
ylim([0 .2])
set(gca, 'FontSize', 15);
subplot(2,1,2)
plot(lam0, X5P, 'k-', 'LineWidth', 1.5);
hold on
plot(TiFrcP(:,1), TiFrcP(:,4), 'g-', 'LineWidth', 1.5);
hold on 
plot(IWP5(:,1), IWP5(:,2), 'b-','MarkerSize', 5, 'LineWidth', 1.5);
xlabel('$\lambda/L_{pp}$');
ylabel('$X_5$ Phase');
xlim([0 5]);
ylim([-180 180])
set(gca, 'FontSize', 15);
% ---------------------
ifig=ifig+1; figure(ifig);clf;
subplot(2,1,1)
plot(lam0, R3A, 'k-', 'LineWidth', 1.5);
hold on
plot(TiRaoA(:,1), TiRaoA(:,3), 'g-', 'LineWidth', 1.5);
hold on 
plot(eRA3(:,1), eRA3(:,2), 'bo','MarkerSize', 5, 'LineWidth', 1.5);
xlabel('$\lambda/L_{pp}$');
ylabel('$\xi_3$');
legend('STF','TiMIT','Exp')
xlim([0 5]);
ylim([0 2])
set(gca, 'FontSize', 15);
subplot(2,1,2)
plot(lam0, R3P, 'k-', 'LineWidth', 1.5);
hold on
plot(TiRaoP(:,1), TiRaoP(:,3), 'g-', 'LineWidth', 1.5);
hold on 
plot(eRP3(:,1), eRP3(:,2), 'bo','MarkerSize', 5, 'LineWidth', 1.5);
xlabel('$\lambda/L_{pp}$');
ylabel('$\xi_3$ Phase');
xlim([0 5]);
ylim([-180 180])
set(gca, 'FontSize', 15);
% ---------------------
ifig=ifig+1; figure(ifig);clf;
subplot(2,1,1)
plot(lam0, R5A, 'k-', 'LineWidth', 1.5);
hold on
plot(TiRaoA(:,1), TiRaoA(:,4), 'g-', 'LineWidth', 1.5);
hold on 
plot(eRA5(:,1), eRA5(:,2), 'bo','MarkerSize', 5, 'LineWidth', 1.5);
xlabel('$\lambda/L_{pp}$');
ylabel('$\xi_5$');
legend('STF','TiMIT','Exp')
xlim([0 5]);
ylim([0 2])
set(gca, 'FontSize', 15);
subplot(2,1,2)
plot(lam0, R5P, 'k-', 'LineWidth', 1.5);
hold on
plot(TiRaoP(:,1), TiRaoP(:,4), 'g-', 'LineWidth', 1.5);
hold on 
plot(eRP5(:,1), eRP5(:,2), 'bo','MarkerSize', 5, 'LineWidth', 1.5);
xlabel('$\lambda/L_{pp}$');
ylabel('$\xi_5$ Phase');
xlim([0 5]);
ylim([-180 180])
set(gca, 'FontSize', 15);
% ---------------------
ifig=ifig+1; figure(ifig);clf;
plot(lam0,Sol(1).RwMaruo(:,1)*(ulen/B)^2,'k-', 'LineWidth', 2);
hold on
plot(lam0,Sol(1).RwSalvesen(:,1)*(ulen/B)^2,'m-', 'LineWidth', 1.5);

xlim([0 3])
xlabel('$\lambda/L_{pp}$');
ylabel('$R_w L_{pp}/ \rho g B^2 A^2$');
legend('STF(Maruo)','STF(Salvesen)');
set(gca, 'FontSize', 15);

% Plot the hull lines
PlotHull(S);

