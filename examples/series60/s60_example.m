
clearvars
close all
clc
ifig=0;
set(0, 'defaulttextinterpreter','latex')
set(0, 'defaultAxesTickLabelInterpreter','latex');
set(0, 'defaultLegendInterpreter','latex');

addpath(genpath('../../src'))

%%
L     = 1.0;
B     = 0.14;
Draft = 0.057;
tmp   = load('s60_points.txt');
xst   = uniquetol(tmp(:,1),1e-2); nst=length(xst);  % The station coordinates
S = repmat(struct('x',{}), 1, nst);
pres = 60;
for i=1:nst
    S(i).x = tmp(abs(tmp(:,1)-xst(i,1))<1e-3,2:3);
    [z,ind] = sort(S(i).x(:,2),'descend');
    y = S(i).x(ind,1);
    y(end) = 0;
    S(i).x(:,1) = y;
    S(i).x(:,2) = -z+Draft;
    S(i).x = GenerateMesh(S(i).x, pres, 1e-6, 0);
    fprintf('%s %d %s %d \n', 'station re-meshed:' ,i, 'of', nst);
    S(i).Nv=length(S(i).x); % Number of vertices for this station
end
%
%
%% Move the coordinate system to the center of floatation and add a bow dummy station
%
Xg = 0;
xst = xst-Xg;
%
% Add 1 dummy stations
%
xst=[xst;0.5]; nst = nst+1;
S1(1)=S(end); S1(1).Nv=0; S1(1).x(:,1)=0;
S3(1:nst-1)=S(1:nst-1); S3(nst)=S1;
S=S3;
xst = num2cell(xst);
[S.xst] = xst{:};
%%
ulen=L; g=9.81; rho=1024;

Fn=0.2;
beta=180*pi/180;
KochinType='2D';  % Choose the Kochin function type: 2D or 3D

cosB=cos(beta);
U=sqrt(g*ulen)*Fn;

nf=60;
lamlMin = 0.5;
lamlMax = 10;
om0Min = sqrt(2*pi*g/(lamlMax*L));
om0Max = sqrt(2*pi*g/(lamlMin*L));
om0 = (linspace(om0Min, om0Max, nf))';

%
% The vcg and radii of gyration
%
Zg   = 0;
rx   = 0.4*B;       % m
ry   = 0.25*ulen;   % m
RGyr=zeros(3);
RGyr(1,1)=rx; RGyr(2,2)=ry; RGyr(3,3)=ry;

[Sol,S]=DTU_StripTheorySolver(S,U,beta,om0,g,ulen,Zg,RGyr,KochinType, 'non');
%%
close all
clc
set(0,'defaulttextinterpreter','latex')
set(0, 'defaultAxesTickLabelInterpreter','latex');
set(0, 'defaultLegendInterpreter','latex');

ind = 1;
ome = om0 - om0.^2/g*U(ind)*cosB;
nu = ome.^2/g;
nu0 = om0.^2/g;
lam0 = 2*pi./(nu0*ulen);
Fjk = Sol(ind).Fjk;
VOL = 0.005761256800610;
A33 =  real(Fjk(:,3,3))*ulen^3/VOL;
B33 = -imag(Fjk(:,3,3))*ulen^3.*ome/(VOL*sqrt(g/ulen));
A53 =  real(Fjk(:,5,3))*ulen^3/VOL;
B53 = -imag(Fjk(:,5,3))*ulen^4.*ome/(VOL*sqrt(g*ulen));
%
A55 =  real(Fjk(:,5,5))*ulen^3/VOL;
B55 = -imag(Fjk(:,5,5))*ulen^3.*ome/(VOL*sqrt(g*ulen));
A35 =  real(Fjk(:,3,5))*ulen^3/VOL;
B35 = -imag(Fjk(:,3,5))*ulen^4.*ome/(VOL*sqrt(g*ulen));
%
figure(1)
subplot(2,2,1)
plot(ome*sqrt(ulen/g), A33, 'b-', 'LineWidth', 2);
xlabel('$\omega \sqrt{L/g}$');
ylabel('$A_{33}/\rho \nabla$');
set(gca, 'FontSize', 12);
subplot(2,2,2)
plot(ome*sqrt(ulen/g), B33, 'b-', 'LineWidth', 2);
xlabel('$\omega \sqrt{L/g}$');
set(gca, 'FontSize', 12);
ylabel('$B_{33}/\rho \nabla \sqrt{g/L}$');
subplot(2,2,3)
plot(ome*sqrt(ulen/g), A53, 'b-', 'LineWidth', 2);
xlabel('$\omega \sqrt{L/g}$');
ylabel('$A_{53}/\rho \nabla L$');
set(gca, 'FontSize', 12);
subplot(2,2,4)
plot(ome*sqrt(ulen/g), B53, 'b-', 'LineWidth', 2);
xlabel('$\omega \sqrt{L/g}$');
set(gca, 'FontSize', 12);
ylabel('$B_{53}/\rho \nabla \sqrt{g L}$');
%
figure(2)
subplot(2,2,1)
plot(ome*sqrt(ulen/g), A55, 'b-', 'LineWidth', 2);
xlabel('$\omega \sqrt{L/g}$');
ylabel('$A_{55}/\rho \nabla L^2$');
set(gca, 'FontSize', 12);
subplot(2,2,2)
plot(ome*sqrt(ulen/g), B55, 'b-', 'LineWidth', 2);
xlabel('$\omega \sqrt{L/g}$');
ylabel('$B_{55}/\rho \nabla L\sqrt{g L}$');
set(gca, 'FontSize', 12);
subplot(2,2,3)
plot(ome*sqrt(ulen/g), A35, 'b-', 'LineWidth', 2);
xlabel('$\omega \sqrt{L/g}$');
ylabel('$A_{35}/\rho \nabla L$');
set(gca, 'FontSize', 12);
subplot(2,2,4)
plot(ome*sqrt(ulen/g), B35, 'b-', 'LineWidth', 2);
xlabel('$\omega \sqrt{L/g}$');
ylabel('$B_{35}/\rho \nabla \sqrt{g L}$');
set(gca, 'FontSize', 12);
%
PlotHull(S);
