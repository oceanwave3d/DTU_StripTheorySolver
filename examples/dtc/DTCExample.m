
clearvars
close all
clc
ifig=0;
set(0, 'defaulttextinterpreter','latex')
set(0, 'defaultAxesTickLabelInterpreter','latex');
set(0, 'defaultLegendInterpreter','latex');

addpath(genpath('../../src'))

%%
L     = 355;
B     = 51;
Draft = 14.5;
tmp   = load('points.txt');
xst   = uniquetol(tmp(:,1),1e-3); nst=length(xst);  % The station coordinates
S = repmat(struct('x',{}), 1, nst);
pres = 30;
for i=1:nst
    S(i).x = tmp(abs(tmp(:,1)-xst(i,1))<1e-3,2:3);
    [z,ind] = sort(S(i).x(:,2),'descend');
    y = S(i).x(ind,1);
    y(end) = 0;
    S(i).x(:,1) = y;
    S(i).x(:,2) = -z+Draft;
    S(i).x = GenerateMesh(S(i).x, pres, 1e-6, 0);
    fprintf('%s %d %s %d \n', 'station re-meshed:' ,i, 'of', nst);
    S(i).Nv=length(S(i).x); % Number of vertices for this station
end
%
%
%% Move the coordinate system to the center of floatation and add a bow dummy station
%
Xg = L/2+3.44;
xst = xst-Xg;
%
% Add a bow dummy station
%
xst=[xst;xst(end)+1/2*(xst(end)-xst(end-1))]; nst = nst+1;
S1(1)=S(end); S1(1).Nv=0; S1(1).x(:,1)=0;
S3(1:nst-1)=S(1:nst-1); S3(nst)=S1;
S=S3;

xst = num2cell(xst);
[S.xst] = xst{:};
%%
ulen=L; g=9.81; rho=1024;

Fn=0.20;
beta=180*pi/180;
KochinType='2D';  % Choose the Kochin function type: 2D or 3D

cosB=cos(beta);
U=sqrt(g*ulen)*Fn;

nf=60;
lamlMin = 0.3;
lamlMax = 3;
om0Min = sqrt(2*pi*g/(lamlMax*L));
om0Max = sqrt(2*pi*g/(lamlMin*L));
om0 = (linspace(om0Min, om0Max, nf))';

%
% The vcg and radii of gyration
%
Zg   = 19.851-14.5;
rx   = 20.30;           % m
ry   = 87.3;        % m
RGyr=zeros(3);
RGyr(1,1)=rx; RGyr(2,2)=ry; RGyr(3,3)=ry;

[Sol,S]=DTU_StripTheorySolver(S,U,beta,om0,g,ulen,Zg,RGyr,KochinType, 'all');
%%
close all
clc
set(0,'defaulttextinterpreter','latex')
set(0, 'defaultAxesTickLabelInterpreter','latex');
set(0, 'defaultLegendInterpreter','latex');

ind = 1;
ome = om0 - om0.^2/g*U(ind)*cosB;
nu = ome.^2/g;
nu0 = om0.^2/g;
lam0 = 2*pi./(nu0*ulen);

ifig=ifig+1; figure(ifig);clf;
plot(lam0,Sol(ind).RwMaruo(:,1),lam0,Sol(ind).RwSalvesen(:,1));

xlabel('$\lambda/L$');
ylabel('$R_w/\rho g A L$');
legend('STF(Maruo)','STF(Salvesen.)');
grid on; xlim([0,3]);
title(['DTC, $F_n=$',num2str(Fn),', $\beta=$',num2str(180*beta/pi)]);
PlotHull(S);
