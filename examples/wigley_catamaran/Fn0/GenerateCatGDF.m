%
% Read in a gdf file representing the x>0 half of a Wigley hull, move it to
% the half beam of the catamaran and re-write the gdf with both x- and
% y-symmetry. 
%
% wamit2matlab plus sub-directories must be in the path
%

W.pot.GDF="wig480.gdf";
W.cfg.ILOWHI=0;
W.pot.NBODY=1;
B=1/3;

W=read_wamit_gdf("./",W);

%
fid=fopen('new.gdf','w');
fprintf(fid,'%s \n',['A Wigley catamaran with x- and y-symmetry. ']);
fprintf(fid,'%e %e \n',[W.gdf{1}.ULEN,W.gdf{1}.GRAV]);
fprintf(fid,'%d %d \n',[1,1]);
fprintf(fid,'%d \n',[W.gdf{1}.NPAN]);

for i=1:W.gdf{1}.NPAN
    for j=1:4
        fprintf(fid,'%e %e %e \n', ...
            [W.gdf{1}.X(i,j) W.gdf{1}.Y(i,j)+B/2 W.gdf{1}.Z(i,j)]);
    end
end

        

