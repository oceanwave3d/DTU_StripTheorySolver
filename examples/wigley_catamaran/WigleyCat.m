%
% This is an example of using the DTU_StripTheorySolver function to
% solve for the heave and pitch response of a Wigley hull catamaran. 
% Comparison is made with TiMIT calculations at Fn=0,0.2 and 0.3.
%
%
% Written by H.B. Bingham and Mostafa Amini-Afshar
%
% This version: November, 2021
%
clearvars;
close all
clc
ifig=0;
set(0,'defaulttextinterpreter','latex')
set(0, 'defaultAxesTickLabelInterpreter','latex');
set(0, 'defaultLegendInterpreter','latex');

addpath(genpath('../../src'))

%% Generate the Wigley sections for one hull
%
%
L=1; B=L/10; Draft=L/16;  % Length, beam and draft of the Wigley hull.
Bcat=L/3;                 % The beam of the catamaran.
CatFlag='yes';            % Flag to indicate a catamaran, 'yes' or 'no'.
%
nst=30;  dx=L/(nst-1); xst=-L/2+L*(0:nst-1)'*dx; % The station x-coordinates
%
% Loop over the stations and define the vertices of each station running 
% from the mean free surface (the waterline) to the keel. Stations 1, and 
% nst are dummy stations at the ends. 
%
% Choose the number of vertices along each half-station for a single hull.
% Note here that, as for a monohull, we only need to define 1/2 of each 
% station in coordinates centered at the cg of the single hull. These lines
% will be reflected and moved to the y-position of +-Bcat/2 by the program
% when CatFlag=='yes'. 
%
Nv=20; 
S = repmat(struct('x',{}), 1, nst);
dth=pi/2/(Nv-1); th=dth*(0:Nv-1)'; 
for i=2:nst-1
    % A Wigley hull model 1.
    xi=2*xst(i)/L;
    zeta=sin(th);
    S(i).x(1:Nv,1)=B/2*((1-zeta.^2).*(1-xi.^2).*(1+0.2*xi.^2)...
        +zeta.^2.*(1-zeta.^8).*(1-xi.^2).^4);
    S(i).x(1:Nv,2)=Draft*zeta;
    S(i).Nv=Nv; 
    S(i).CatFlag=CatFlag;  % Each station should hold CatFlag and Bcat.
    S(i).Bcat=Bcat;
end
%
% Add in the dummy stations at the ends
%
S(1).Nv=0; S(1).x=zeros(Nv,2);
S(1).x(:,2)=S(2).x(:,2); 
S(1).CatFlag=CatFlag; S(1).Bcat=Bcat; 
S(nst).Nv=0; S(nst).x=zeros(Nv,2);
S(nst).x(:,2)=S(nst-1).x(:,2); 
S(nst).CatFlag=CatFlag; S(nst).Bcat=Bcat;
%
% Set the xcg, add xst to the structure and compute the normal vectors. 
% 
xcg=0;
xst = num2cell(xst);
[S.xst] = xst{:};
% S=Compute3DNormals(S);
% %
% % Plot the normals to check
% %
% figure(101); clf; hold on
% for i=1:nst
%     quiver3(S(i).xst*ones(Nv-1,1),S(i).xc(:,1),S(i).xc(:,2),S(i).n3D(:,1),...
%         S(i).n3D(:,2),S(i).n3D(:,3),'MarkerSize', 4);
% end
% axis equal;
% view(-34,28)
% set ( gca, 'zdir', 'reverse' )
%
%% Set up the speeds and encounter frequencies to solve for
%
% The density, gravity and length-scale for non-dimensionalization.
%
ulen=L; g=9.81; rho=1024;
%
% The speed and encounter frequency vectors to solve for.
%
% The STF function is set up to accept a vector of speeds, for any given
% heading angle, but this script only checks one at a time. TiMIT heading
% angles which can be compared to are beta=180, 160, 140, 0.
%
% Fn=0.; fext='Fn0'; beta=180*pi/180;
Fn=0.2; fext='Fn2'; beta=180*pi/180;
% Fn=0.3; fext='Fn3'; beta=180*pi/180;

KochinType='2D';  % Choose the Kochin function type: 2D or 3D

cosB=cos(beta);

U=sqrt(g*ulen)*Fn;

nf=50;
lamlMin = 0.2; % For following seas and Fn=0.3, increase this to 0.75. 
lamlMax = 5;
om0Min = sqrt(2*pi*g/(lamlMax*L));
om0Max = sqrt(2*pi*g/(lamlMin*L));
om0 = (linspace(om0Min, om0Max, nf))';
%
zcg=-.02*ulen; 
RGyr=zeros(3); RGyr(1,1)=0.25*ulen; RGyr(2,2)=0.25*ulen; RGyr(3,3)=0.25*ulen;
%
% Call the strip theory solver
%
%hbb Do not compute added resistance for a cat as this is still being
%worked on. 
%hbb
[Sol,S]=DTU_StripTheorySolver(S,U,beta,om0,g,ulen,zcg,RGyr,KochinType, 'non');
%
Fjk=Sol(1).Fjk; XjD=Sol(1).XjD; XjDfk=Sol(1).XjDfk; XjDs=Sol(1).XjDs; xi=Sol(1).xi;
%
% Save the results
%
save(['WigleyCatNx',num2str(nst),'Nv',num2str(Nv),...
    '_Fn',num2str(Fn),'_beta',num2str(180*beta/pi),'.mat']);
%%
% Plot and compare with TiMIT
%
ind = 1;
om0nd=om0*sqrt(L/g);
om = om0 - om0.^2/g*U(ind)*cosB;
omnd = om*sqrt(L/g);
nu0nd = om0nd.^2;
nu0   = om0.^2/g;
lam0 = 2*pi./(nu0*ulen);

% Load the TiMIT calcs for comparison. The results are non-dimensional.
%
% Added mass and damping are given in terms of encounter period while
% diffraction forces and RAOs use exactly the same range but in terms of
% absolute wave period!
%
fname=[fext,'/ifoptn.1'];
tmp=load(fname);
modesT=unique(tmp(:,2)); nmT=length(modesT);
fname=[fext,'/ifoptn.frq'];
Tpars=load(fname);
index=Tpars(:,5)==180*beta/pi;
omT=Tpars(index,1);  nfT=length(omT);
nuT=omT.^2; lamT=2*pi./nuT; omET=omT-Fn*cosB*nuT;
% 
% 
AjkT=zeros(nfT,6,6); BjkT=zeros(nfT,6,6); XjDT=zeros(nfT,6);
index=find(tmp(:,2)==3 & tmp(:,3)==3);
AjkT(:,3,3)=tmp(index,4); BjkT(:,3,3)=tmp(index,5);
index=find(tmp(:,2)==5 & tmp(:,3)==5);
AjkT(:,5,5)=tmp(index,4); BjkT(:,5,5)=tmp(index,5);
%
if Fn>0
    index=find(tmp(:,2)==3 & tmp(:,3)==5);
    AjkT(:,3,5)=tmp(index,4); BjkT(:,3,5)=tmp(index,5);
    index=find(tmp(:,2)==5 & tmp(:,3)==3);
    AjkT(:,5,3)=tmp(index,4); BjkT(:,5,3)=tmp(index,5);
end
%
% Exciting forces
%
fname=[fext,'/ifoptn.3'];
tmp=load(fname);

index=find(tmp(:,2)==beta*180/pi & tmp(:,3)==3);
XjDT(:,3)=complex(tmp(index,6),tmp(index,7));
index=find(tmp(:,2)==beta*180/pi & tmp(:,3)==5);
XjDT(:,5)=complex(tmp(index,6),tmp(index,7));
%
ifig=ifig+1; figure(ifig);clf;
plot(omnd,real(Fjk(:,3,3)),omT,AjkT(:,3,3),omnd,-imag(Fjk(:,3,3)),omT,BjkT(:,3,3))
xlabel('$\omega\sqrt{L/g}$');
legend('$A_{33}/(\rho L^3)$, STF','$A_{33}/(\rho L^3)$, TiMIT',...
    '$B_{33}/(\rho L^3\omega)$, STF','$B_{33}/(\rho L^3\omega)$, TiMIT');
title(['Wigley I hull at $F_n=$',num2str(Fn)]);
grid on
%
ifig=ifig+1; figure(ifig);clf;
plot(omnd,real(Fjk(:,3,5)),omT,AjkT(:,3,5),omnd,-imag(Fjk(:,3,5)),omT,BjkT(:,3,5))
xlabel('$\omega\sqrt{L/g}$');
legend('$A_{35}/(\rho L^3)$, STF','$A_{35}/(\rho L^5)$, TiMIT',...
    '$B_{35}/(\rho L^5\omega)$, STF','$B_{35}/(\rho L^5\omega)$, TiMIT');
title(['Wigley I hull at $F_n=$',num2str(Fn)]);
grid on
%
ifig=ifig+1; figure(ifig);clf;
plot(omnd,real(Fjk(:,5,3)),omT,AjkT(:,5,3),omnd,-imag(Fjk(:,5,3)),omT,BjkT(:,5,3))
xlabel('$\omega\sqrt{L/g}$');
legend('$A_{53}/(\rho L^3)$, STF','$A_{53}/(\rho L^5)$, TiMIT',...
    '$B_{53}/(\rho L^5\omega)$, STF','$B_{53}/(\rho L^5\omega)$, TiMIT');
title(['Wigley I hull at $F_n=$',num2str(Fn)]);
grid on
%
ifig=ifig+1; figure(ifig);clf;
plot(omnd,real(Fjk(:,5,5)),omT,AjkT(:,5,5),omnd,-imag(Fjk(:,5,5)),omT,BjkT(:,5,5))
xlabel('$\omega\sqrt{L/g}$');
legend('$A_{55}/(\rho L^3)$, STF','$A_{55}/(\rho L^5)$, TiMIT',...
    '$B_{55}/(\rho L^5\omega)$, STF','$B_{55}/(\rho L^5\omega)$, TiMIT');
title(['Wigley I hull at $F_n=$',num2str(Fn)]);
xlim([0,7]); %ylim([0 1.5*max(BjkT(:,5,5))])
grid on
%
% For diffraction, the solution is at absolute frequencies for TiMIT.
%
ifig=ifig+1; figure(ifig);clf;
plot(om0nd,real(XjD(:,3)),omT,real(XjDT(:,3)),om0nd,real(XjDfk(:,3)+XjDs(:,3)),...
    om0nd,imag(XjD(:,3)),omT,imag(XjDT(:,3)),om0nd,imag(XjDfk(:,3)+XjDs(:,3)))
xlabel('$\omega_0\sqrt{L/g}$');
legend('$\Re\{X_{3}\}/(\rho g L^2 A)$, STF','$\Re\{X_{3}\}/(\rho g L^2 A)$, TiMIT',...
    '$\Re\{X_{3}\}/(\rho g L^2 A)$, STF-FK+scatt',...
    '$\Im\{X_{3}\}/(\rho g L^2 A)$, STF','$\Im\{X_{3}\}/(\rho g L^2 A)$, TiMIT',...
    '$\Im\{X_{3}\}/(\rho g L^2 A)$, STF-FK+scatt');
grid on; xlim([0,7])
title(['Wigley I hull at $F_n=$',num2str(Fn),', $\beta=$',num2str(180*beta/pi)]);
%
ifig=ifig+1; figure(ifig);clf;
plot(om0nd,real(XjDfk(:,3)),om0nd,real(XjDs(:,3)),om0nd,real(XjD(:,3)-XjDfk(:,3)),...
    om0nd,imag(XjDfk(:,3)),om0nd,imag(XjDs(:,3)),om0nd,imag(XjD(:,3)-XjDfk(:,3)))
xlabel('$\omega_0\sqrt{L/g}$');
legend('$\Re\{X_{3}\}/(\rho g L^2 A)$, STF-FK','$\Re\{X_{3}\}/(\rho g L^2 A)$, STF-scatt','$\Re\{X_{3}\}/(\rho g L^2 A)$, Hask',...
    '$\Im\{X_{3}\}/(\rho g L^2 A)$, STF-FK','$\Im\{X_{3}\}/(\rho g L^2 A)$, STF-scatt','$\Im\{X_{3}\}/(\rho g L^2 A)$, Hask');
grid on; xlim([0,7])
title(['Wigley I hull at $F_n=$',num2str(Fn),', $\beta=$',num2str(180*beta/pi)]);
%
ifig=ifig+1; figure(ifig);clf;
plot(om0nd,real(XjD(:,5)),omT,real(XjDT(:,5)),om0nd,imag(XjD(:,5)),omT,imag(XjDT(:,5)))
xlabel('$\omega_0\sqrt{L/g}$');
legend('$\Re\{X_{5}\}/(\rho g L^3 A)$, STF','$\Re\{X_{5}\}/(\rho g L^3 A)$, TiMIT',...
    '$\Im\{X_{5}\}/(\rho g L^3 A)$, STF','$\Im\{X_{5}\}/(\rho g L^3 A)$, TiMIT');
grid on; xlim([0,7])
title(['Wigley I hull at $F_n=$',num2str(Fn),', $\beta=$',num2str(180*beta/pi)]);

%Compare the strip theory RAOs with TiMIT/WAMIT

fname=[fext,'/ifoptn.4'];
tmp=load(fname);
index=find(tmp(:,2)==180*beta/pi & tmp(:,3)==3);
xiT(:,3)=complex(tmp(index,6),tmp(index,7));
index=find(tmp(:,2)==180*beta/pi & tmp(:,3)==5);
xiT(:,5)=complex(tmp(index,6),tmp(index,7));


ifig=ifig+1; figure(ifig);clf;
plot(lam0,abs(xi(:,3)),lamT,abs(xiT(:,3)),lam0,abs(xi(:,5))./(nu0nd),...
    lamT,abs(xiT(:,5))./(nuT))
xlabel('$\lambda/L$');
legend('$|\xi_{3}|/A$, STF','$|\xi_{3}|/A$, TiMIT',...
    '$|\xi_{5}|/(kA)$, STF','$|\xi_{5}|/(kA)$, TiMIT','Location','SouthEast');
grid on; xlim([0,6]);

switch fext
    case 'Fn0'
        ylim([0,1.2])
    case 'Fn2'
        ylim([0,2.5])
    case 'Fn3'
        ylim([0,4])
end
title(['Wigley hull I, $F_n=$',num2str(Fn),', $\beta=$',num2str(180*beta/pi)])

ifig=ifig+1; figure(ifig);clf;
plot(omnd,abs(xi(:,3)),omET,abs(xiT(:,3)),omnd,abs(xi(:,5)),...
    omET,abs(xiT(:,5)))
xlabel('$\omega\sqrt{L/g}$');
legend('$|\xi_{3}|/A$, STF','$|\xi_{3}|/A$, TiMIT',...
    '$|\xi_{5}|/(A/L)$, STF','$|\xi_{5}|/(A/L)$, TiMIT');
grid on; xlim([0,7]) % ylim([0,1.5])
title(['Wigley I hull at $F_n=$',num2str(Fn),', $\beta=$',num2str(180*beta/pi)]);
%%
ARplot='no';
switch ARplot
    case 'yes'
%
% Compare the strip theory added resistance with TiMIT/WAMIT
%
fname=[fext,'/ifoptn.9'];
tmp=load(fname);
index=find(tmp(:,2)==180*beta/pi & tmp(:,3)==180*beta/pi & tmp(:,4)==1);
RwT(:,1)=tmp(index,5);

ifig=ifig+1; figure(ifig);clf;
plot(lam0,Sol(ind).RwMaruo(:,1),...
    lam0,Sol(ind).RwSalvesen(:,1), ...
    lamT,-RwT);

xlabel('$\lambda/L$');
ylabel('$R_w/(\rho g A L^2)$');
legend('STF(Maruo)', 'STF(Salvesen)', 'TiMIT');
grid on; xlim([0,3]); ylim([0 1.5*max(-RwT)]);
title(['Wigley hull I, $F_n=$',num2str(Fn),', $\beta=$',num2str(180*beta/pi)])
end


PlotHull(S);

set(0,'defaulttextinterpreter','tex')
set(0, 'defaultAxesTickLabelInterpreter','tex');
set(0, 'defaultLegendInterpreter','tex');

